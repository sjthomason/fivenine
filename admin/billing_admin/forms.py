from django import forms
from django.utils.timezone import utc
from billing.models import Invoice, Payment
from utils.forms import PaymentField

from datetime import datetime

class AdminInvoiceForm(forms.ModelForm):

    time_stamp = forms.DateTimeField(required=False,
                                     label="Created",
                                     help_text="Leave blank for today")
    send_email = forms.BooleanField(required=False)

    def clean_time_stamp(self):
        time_stamp = self.cleaned_data['time_stamp']
        if not time_stamp:
            return datetime.utcnow().replace(tzinfo=utc)
        return time_stamp

    class Meta:
        model = Invoice


class AdminPaymentForm(forms.ModelForm):

    amount = PaymentField()
    time_stamp = forms.DateTimeField(required=False,
                                     label="Created",
                                     help_text="Leave blank for today")
    send_email = forms.BooleanField(required=False)

    def clean_time_stamp(self):
        time_stamp = self.cleaned_data['time_stamp']
        if not time_stamp:
            return datetime.utcnow().replace(tzinfo=utc)
        return time_stamp

    class Meta:
        model = Payment
