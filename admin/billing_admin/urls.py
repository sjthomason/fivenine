from django.conf.urls.defaults import patterns, include, url

from billing.models import (DidPlan, CpanelPlan, OtherPlan,
                            PbxPlan, SipChannelPlan)
from views import *

urlpatterns = patterns('',
    url(r'^did-plan/$',
        AdminPlanListView.as_view(model=DidPlan)),
    url(r'^did-plan/add/$',
        AdminPlanCreateView.as_view(model=DidPlan, success_url="/admin/billing/did-plan/")),
    url(r'^did-plan/(?P<pk>\d+)/edit/$',
        AdminPlanUpdateView.as_view(model=DidPlan, success_url="/admin/billing/did-plan/")),
    url(r'^did-plan/(?P<pk>\d+)/delete/$',
        AdminPlanDeleteView.as_view(model=DidPlan, success_url="/admin/billing/did-plan/")),

    url(r'^hosting-plan/$', AdminPlanListView.as_view(model=CpanelPlan)),
    url(r'^hosting-plan/add/$',
        AdminPlanCreateView.as_view(model=CpanelPlan, success_url="/admin/billing/hosting-plan/")),
    url(r'^hosting-plan/(?P<pk>\d+)/edit/$',
        AdminPlanUpdateView.as_view(model=CpanelPlan, success_url="/admin/billing/hosting-plan/")),
    url(r'^hosting-plan/(?P<pk>\d+)/delete/$',
        AdminPlanDeleteView.as_view(model=CpanelPlan)),

    url(r'^other-plan/$', AdminPlanListView.as_view(model=OtherPlan)),
    url(r'^other-plan/add/$',
        AdminPlanCreateView.as_view(model=OtherPlan, success_url="/admin/billing/other-plan/")),
    url(r'^other-plan/(?P<pk>\d+)/edit/$',
        AdminPlanUpdateView.as_view(model=OtherPlan, success_url="/admin/billing/other-plan/")),
    url(r'^other-plan/(?P<pk>\d+)/delete/$',
        AdminPlanDeleteView.as_view(model=OtherPlan, success_url="/admin/billing/other-plan/")),

    url(r'^pbx-plan/$', AdminPlanListView.as_view(model=PbxPlan)),
    url(r'^pbx-plan/add/$',
        AdminPlanCreateView.as_view(model=PbxPlan, success_url="/admin/billing/pbx-plan/")),
    url(r'^pbx-plan/(?P<pk>\d+)/edit/$',
        AdminPlanUpdateView.as_view(model=PbxPlan, success_url="/admin/billing/pbx-plan/")),
    url(r'^pbx-plan/(?P<pk>\d+)/delete/$',
        AdminPlanDeleteView.as_view(model=PbxPlan, success_url="/admin/billing/pbx-plan/")),

    url(r'^sipchannel-plan/$',
        AdminPlanListView.as_view(model=SipChannelPlan)),
    url(r'^sipchannel-plan/add/$',
        AdminPlanCreateView.as_view(model=SipChannelPlan, success_url="/admin/billing/sipchannel-plan/")),
    url(r'^sipchannel-plan/(?P<pk>\d+)/edit/$',
        AdminPlanUpdateView.as_view(model=SipChannelPlan, success_url="/admin/billing/sipchannel-plan/")),
    url(r'^sipchannel-plan/(?P<pk>\d+)/delete/$',
        AdminPlanDeleteView.as_view(model=SipChannelPlan, success_url="/admin/billing/sipchannel-plan/")),

    url(r'^invoices/$', AdminInvoiceListView.as_view()),
    url(r'^invoices/add/$', AdminInvoiceCreateView.as_view()),
    url(r'^invoices/(?P<pk>\d+)/edit/$', AdminInvoiceUpdateView.as_view()),
    url(r'^invoices/(?P<pk>\d+)/delete/$', AdminInvoiceDeleteView.as_view()),
    url(r'^invoices/(?P<pk>\d+)/add-charge/$', AdminInvoiceItemCreateView.as_view()),
    url(r'^invoices/(?P<inv_pk>\d+)/remove-charge/(?P<pk>\d+)/$', AdminInvoiceItemDeleteView.as_view()),
    url(r'^payments/$', AdminPaymentListView.as_view()),
    url(r'^payments/add/$', AdminPaymentCreateView.as_view()),
    url(r'^payments/(?P<pk>\d+)/edit/$', AdminPaymentUpdateView.as_view()),
    url(r'^payments/(?P<pk>\d+)/delete/$', AdminPaymentDeleteView.as_view())
)
