from django.http import HttpResponseRedirect
from billing.models import Payment, Invoice, InvoiceItem
from base_views.staff import (StaffListView, StaffCreateView,
                              StaffUpdateView, StaffDeleteView)

from forms import AdminInvoiceForm, AdminPaymentForm


class AdminPlanListView(StaffListView):

    template_name = "admin/plan_list.html"


class AdminPlanCreateView(StaffCreateView):

    template_name = "admin/base_form.html"


class AdminPlanUpdateView(StaffUpdateView):

    template_name = "admin/base_form.html"


class AdminPlanDeleteView(StaffDeleteView):

    template_name = "admin/confirm_delete.html"


class AdminInvoiceListView(StaffListView):

    model = Invoice
    template_name = "admin/invoice_list.html"
    context_object_name = "invoice_list"
    paginate_by = 100


class AdminInvoiceCreateView(StaffCreateView):

    model = Invoice
    form_class = AdminInvoiceForm
    template_name = "admin/invoice_form.html"
    success_url= "/admin/billing/invoices/"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if form.cleaned_data['send_email']:
            self.object.email_to_user()
        return super(AdminInvoiceCreateView, self).form_valid(form)


class AdminInvoiceUpdateView(StaffUpdateView):

    form_class = AdminInvoiceForm
    model = Invoice
    template_name = "admin/invoice_form.html"
    success_url= "/admin/billing/invoices/"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if form.cleaned_data['send_email']:
            self.object.email_to_user()
        return super(AdminInvoiceUpdateView, self).form_valid(form)


class AdminInvoiceDeleteView(StaffDeleteView):

    model = Invoice
    template_name = "admin/confirm_delete.html"
    success_url= "/admin/billing/invoices/"


class AdminInvoiceItemCreateView(StaffCreateView):

    model = InvoiceItem
    template_name = "admin/invoice_form.html"
    pk_url_kwarg = 'pk'

    def form_valid(self, form):
        inv_pk = self.kwargs.get(self.pk_url_kwarg, None)
        self.object = form.save(commit=False)
        self.object.invoice = Invoice.objects.get(pk=inv_pk)
        self.success_url = "/admin/billing/invoices/%s/edit/" % inv_pk
        return super(AdminInvoiceItemCreateView, self).form_valid(form)

class AdminInvoiceItemDeleteView(StaffDeleteView):

    model = InvoiceItem
    template_name = "admin/confirm_delete.html"
    inv_pk_url_kwarg = 'inv_pk'

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        inv_pk = self.kwargs.get(self.inv_pk_url_kwarg, None)
        self.success_url = "/admin/billing/invoices/%s/edit/" % inv_pk
        self.object.delete()
        return HttpResponseRedirect(self.get_success_url())

class AdminPaymentListView(StaffListView):

    model = Payment
    template_name = "admin/payment_list.html"


class AdminPaymentCreateView(StaffCreateView):

    form_class = AdminPaymentForm
    model = Payment
    template_name = "admin/payment_form.html"
    success_url = "/admin/billing/payments/"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.amount = form.cleaned_data['amount']
        if form.cleaned_data['send_email']:
            self.object.save()
            self.object.email_to_user()
        return super(AdminPaymentCreateView, self).form_valid(form)


class AdminPaymentUpdateView(StaffUpdateView):

    form_class = AdminPaymentForm
    model = Payment
    template_name = "admin/payment_form.html"
    success_url = "/admin/billing/payments/"

    def get_initial(self, *args, **kwargs):
        initial = super(AdminPaymentUpdateView, self).get_initial(*args, **kwargs)
        initial['amount'] = self.object.amount
        return initial

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.amount = form.cleaned_data['amount']
        if form.cleaned_data['send_email']:
            self.object.email_to_user()
        return super(AdminPaymentUpdateView, self).form_valid(form)


class AdminPaymentDeleteView(StaffDeleteView):

    model = Payment
    template_name = "admin/confirm_delete.html"
    success_url = "/admin/billing/payments/"

