from django import forms
from django.contrib.auth.models import User
from django.contrib.localflavor.us.forms import USZipCodeField

from billing.models import Provider
from kb.models import KbArticle, KbCategory, KbImage
from portal.models import Did, Endpoint, ServiceAddress
from cdr.forms import CdrForm


class AdminCdrForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(AdminCdrForm, self).__init__(*args, **kwargs)
        self.fields['endpoint'] = forms.ChoiceField(
            choices=[('', 'All')] + [(ep.ep_id, str(ep)) for ep in
                                    Endpoint.objects.all()],
                                    required=False)


class AdminUserCreationForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ("username",)


class ServiceAddressForm(forms.ModelForm):
    zip = USZipCodeField()

    class Meta:
        model = ServiceAddress
        exclude = ("user",)


class EndpointForm(forms.ModelForm):
    class Meta:
        model = Endpoint
        exclude = ("ep_id", "billing_acct",)


class DidForm(forms.ModelForm):

    def __init__(self, user=None, *args, **kwargs):
        super(DidForm, self).__init__(*args, **kwargs)
        self.fields['endpoint'].queryset = Endpoint.objects.filter(user=user)
        self.fields['svc_addr'].queryset = ServiceAddress.objects.filter(user=user)

    class Meta:
        model = Did
        exclude = ("billing_plan",)


class DidExportForm(forms.Form):

    provider = forms.ModelChoiceField(queryset=Provider.objects.all())


class KbArticleForm(forms.ModelForm):
    body = forms.CharField(
                widget=forms.Textarea(attrs={'rows':'15',
                                             'cols':'80',
                                             'class':'tinymce'}))

    class Meta:
        model = KbArticle


class KbCategoryForm(forms.ModelForm):
    class Meta:
        model = KbCategory


class KbImageForm(forms.ModelForm):
    class Meta:
        model = KbImage

