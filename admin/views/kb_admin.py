from admin.forms import KbArticleForm, KbCategoryForm, KbImageForm
from base_views.staff import (StaffCreateView, StaffDeleteView,
                              StaffListView, StaffUpdateView)
from kb.models import KbArticle, KbCategory, KbImage


class KbArticleListView(StaffListView):

    model = KbArticle
    context_object_name = "article_list"
    template_name = "admin/kb_article_list.html"


class KbArticleCreateView(StaffCreateView):

    form_class = KbArticleForm
    model = KbArticle
    success_url = "/admin/kb/articles/"
    template_name = "admin/kb_article_form.html"


class KbArticleUpdateView(StaffUpdateView):

    form_class = KbArticleForm
    model = KbArticle
    success_url = "/admin/kb/articles/"
    template_name = "admin/kb_article_form.html"


class KbArticleDeleteView(StaffDeleteView):

    pass


class KbCategoryListView(StaffListView):

    model = KbCategory
    context_object_name = "category_list"
    template_name = "admin/kb_category_list.html"


class KbCategoryCreateView(StaffCreateView):

    form_class = KbCategoryForm
    model = KbCategory
    success_url = "/admin/kb/categories/"
    template_name = "admin/kb_category_form.html"


class KbCategoryUpdateView(StaffUpdateView):

    form_class = KbCategoryForm
    model = KbCategory
    success_url = "/admin/kb/categories/"
    template_name = "admin/kb_category_form.html"


class KbCategoryDeleteView(StaffDeleteView):

    pass

class KbImageListView(StaffListView):

    model = KbImage
    context_object_name = "image_list"
    template_name = "admin/kb_image_list.html"


class KbImageCreateView(StaffCreateView):

    form_class = KbImageForm
    model = KbImage
    success_url = "/admin/kb/"
    template_name = "admin/kb_image_form.html"


class KbImageUpdateView(StaffUpdateView):

    form_class = KbImageForm
    model = KbImage
    success_url = "/admin/kb/"
    template_name = "admin/kb_image_form.html"


class KbImageDeleteView(StaffDeleteView):

    pass
