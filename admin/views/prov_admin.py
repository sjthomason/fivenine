from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from mongoengine import ValidationError

from portal.models import Did, Endpoint
from cdr.models import Cdr

from admin.forms import AdminCdrForm, DidForm, DidExportForm

from base_views.staff import (StaffCreateView, StaffDetailView, StaffDeleteView,
                              StaffFormView, StaffListView, StaffUpdateView)


class AdminCdrListView(StaffListView):

    success_url = None

    def get_success_url(self):
        if self.success_url:
            url = self.success_url
        else:
            raise ImproperlyConfigured(
                "No URL to redirect to. Provide a success_url.")
        return url

    def get(self, request, *args, **kwargs):
        ep_id = request.session.get('ep_id')
        if ep_id is not None:
            self.object_list = Cdr.objects.filter(
                ep_id=ep_id).order_by('-start_time')
            form = AdminCdrForm(initial={'endpoint': ep_id})
        else:
            self.object_list = Cdr.objects.all().order_by('-start_time')
            form = AdminCdrForm()

        context = self.get_context_data(object_list=self.object_list)
        context['form'] = form
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        form = AdminCdrForm(request.POST)
        if form.is_valid():
            ep_id = form.cleaned_data['endpoint']
            if ep_id:
                request.session['ep_id'] = ep_id
                self.object_list = Cdr.objects.filter(
                    ep_id=ep_id).order_by('-start_time')
            else:
                self.object_list = Cdr.objects.all().order_by('-start_time')
                try:
                    del request.session['ep_id']
                except KeyError:
                    pass

            context = self.get_context_data(object_list=self.object_list)
            context['form'] = form
            return HttpResponseRedirect(self.get_success_url())


class AdminCdrDetailView(StaffDetailView):

    def get_object(self):
        pk = self.kwargs.get(self.pk_url_kwarg, None)
        if pk is not None:
            try:
                obj = self.model.objects.with_id(pk)
            except ValidationError:
                raise Http404

        if obj is None:
            raise Http404

        return obj

    def get_template_names(self):
        return [self.template_name]


class AdminDidListView(StaffListView):

    model = Did
    context_object_name = "did_list"
    template_name = "admin/did_list.html"
    paginate_by = 100


class AdminDidCreateView(StaffCreateView):

    form_class = DidForm
    model = Did
    success_url = "/admin/dids/"
    template_name = "admin/base_form.html"


class AdminDidUpdateView(StaffUpdateView):

    form_class = DidForm
    model = Did
    success_url = "/admin/dids/"
    template_name = "admin/base_form.html"

    def get_form(self, form_class):
        return form_class(self.object.user, **self.get_form_kwargs())

class AdminDidDeleteView(StaffDeleteView):

    model = Did
    success_url = "/admin/dids/"
    template_name = "admin/confirm_delete.html"

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.remove()
        return HttpResponseRedirect(self.get_success_url())


class AdminDidExportView(StaffFormView):

    form_class = DidExportForm
    template_name = "admin/didexport_form.html"

    def form_valid(self, form):
        from django.http import HttpResponse
        from django.template import loader, Context

        provider = form.cleaned_data['provider']
        did_list = Did.objects.filter(provider=provider)

        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename=did-export.csv'

        t = loader.get_template('admin/didexport_list.txt')
        c = Context({
            'did_list': did_list,
        })
        response.write(t.render(c))
        return response


class AdminDidSearchView(StaffListView):

    model = Did
    context_object_name = 'did_list'
    template_name = "admin/did_list.html"

    def get_queryset(self):
        from django.db.models import Q
        import re
        if self.query is not None:
            qset = (
                Q(number__icontains=re.sub('(-|\(|\)|\.)', '', self.query)) |
                Q(description__icontains=self.query) |
                Q(rate_center__icontains=self.query)
            )
        if self.model is not None:
            queryset = self.model._default_manager.filter(qset)
        return queryset

    def get(self, request, **kwargs):
        self.query = request.GET.get('q', '')
        return super(AdminDidSearchView, self).get(request, **kwargs)


cdr_list_view = AdminCdrListView.as_view(
                            context_object_name="cdr_list",
                            template_name="admin/cdr_list.html",
                            paginate_by=100,
                            success_url="/admin/cdrs/")
cdr_detail_view = AdminCdrDetailView.as_view(
                            model=Cdr,
                            template_name="admin/cdr_detail.html",
                            context_object_name="cdr")
