from base_views.staff import (StaffCreateView, StaffUpdateView,
                              StaffDeleteView, StaffListView,
                              StaffTemplateView)

from billing.models import Provider


class ActiveCallsView(StaffTemplateView):
    def get_context_data(self, *args, **kwargs):
        from utils.freeswitch import get_calls
        context = super(ActiveCallsView, self).get_context_data(*args, **kwargs)
        context['call_list'] = get_calls()
        return context

active_calls_view = StaffTemplateView.as_view(template_name="admin/active_calls.html")
active_calls_list_view = ActiveCallsView.as_view(
                                template_name="admin/active_calls_list.html")

admin_dashboard_view = StaffTemplateView.as_view(template_name="admin/status.html")
provider_list_view = StaffListView.as_view(
                                model=Provider,
                                template_name="admin/provider_list.html",
                                context_object_name="provider_list")
provider_create_view = StaffCreateView.as_view(
                                model=Provider,
                                success_url="/admin/providers/",
                                template_name="admin/provider_form.html")
provider_update_view = StaffUpdateView.as_view(
                                model=Provider,
                                success_url="/admin/providers/",
                                template_name="admin/provider_form.html")
provider_delete_view = StaffDeleteView.as_view(
                                model=Provider,
                                success_url="/admin/providers/",
                                template_name="admin/confirm_delete.html")
