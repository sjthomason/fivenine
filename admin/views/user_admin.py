from django.conf import settings
from django.contrib.auth.forms import AdminPasswordChangeForm
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404

from base_views.staff import (StaffCreateView, StaffDeleteView,
                                        StaffListView, StaffRedirectView,
                                        StaffUpdateView)
from admin.forms import AdminUserCreationForm
from portal.models import UserProfile


class AdminUserCreateView(StaffCreateView):

    form_class = AdminUserCreationForm
    model = User
    success_url = "/admin/users/"
    template_name = "admin/user_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.set_password(User.objects.make_random_password(length=12))
        return super(AdminUserCreateView, self).form_valid(form)


class AdminUserProfileView(StaffUpdateView):
    pass


class AdminUserPasswordView(StaffUpdateView):

    form_class = AdminPasswordChangeForm
    model = User
    success_url = "/admin/users/"
    template_name = "admin/user_form.html"

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instanciating the form.
        """
        kwargs = {'user': self.object}
        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs


class BecomeUserView(StaffRedirectView):

    permanent = False
    url = "/accounts/profile/"

    def get(self, request, pk, *args, **kwargs):
        from django.contrib.auth import SESSION_KEY, BACKEND_SESSION_KEY

        su_user = get_object_or_404(User, pk=pk)

        if SESSION_KEY in request.session:
            if request.session[SESSION_KEY] != su_user.id:
                # To avoid reusing another user's session, create a new, empty
                # session if the existing session corresponds to a different
                # authenticated user.
                request.session.flush()
        else:
            request.session.cycle_key()
        request.session[SESSION_KEY] = su_user.id
        request.session[BACKEND_SESSION_KEY] = settings.AUTHENTICATION_BACKENDS[0]
        if hasattr(request, 'user'):
            request.user = su_user

        try:
            profile = request.user.get_profile()
            request.session['timezone'] = profile.timezone
        except UserProfile.DoesNotExist:
            profile = None

        return super(BecomeUserView, self).get(request, *args, **kwargs)


user_list_view = StaffListView.as_view(
                            context_object_name="user_list",
                            template_name="admin/users.html",
                            queryset = User.objects.all().order_by('username'))
user_update_view = StaffUpdateView.as_view(
                            form_class=UserChangeForm,
                            model=User,
                            success_url="/admin/users/",
                            template_name ="admin/user_form.html")
user_delete_view = StaffDeleteView.as_view(
                            model=User,
                            success_url="/admin/users/",
                            template_name="admin/confirm_delete.html")
