from base_views.staff import (StaffCreateView, StaffUpdateView, StaffDeleteView,
                              StaffListView)

from portal.models import CpanelServer, Domain

class AdminCpanelServerListView(StaffListView):

    model = CpanelServer
    template_name = "admin/cpanelserver_list.html"
    context_object_name = 'cpanelserver_list'


class AdminCpanelServerCreateView(StaffCreateView):

    model = CpanelServer
    template_name = "admin/cpanelserver_form.html"
    success_url = "/admin/web-servers/"


class AdminCpanelServerUpdateView(StaffUpdateView):

    model = CpanelServer
    template_name = "admin/cpanelserver_form.html"
    success_url = "/admin/web-servers/"


class AdminCpanelServerDeleteView(StaffDeleteView):

    model = CpanelServer
    template_name = "admin/confirm_delete.html"
    success_url = "/admin/web-servers/"

domain_list_view = StaffListView.as_view(model=Domain,
                        template_name="admin/domain_list.html")
domain_create_view = StaffCreateView.as_view(model=Domain,
                        template_name="admin/domain_form.html",
                        success_url="/admin/domains/")
domain_update_view = StaffUpdateView.as_view(model=Domain,
                        template_name="admin/domain_form.html",
                        success_url="/admin/domains/")
domain_delete_view = StaffDeleteView.as_view(model=Domain,
                        template_name="admin/confirm_delete.html",
                        success_url="/admin/domains/")
