#!/usr/bin/env python
from django.core.management import setup_environ
import settings
setup_environ(settings)

from datetime import datetime, timedelta
from django.utils.timezone import utc

try:
    from pymongo import MongoClient
except ImportError:
    # deprecated
    from pymongo import Connection as MongoClient

from cdr.models import Cdr

ARCHIVE_COL = 'cdr_archive'
CDR_COL = Cdr.meta['collection']

# Connect to MongoDB
archive_db = MongoClient(host=settings.CDR_ARCHIVE_DB['HOST'],
                         tz_aware=True)[settings.CDR_ARCHIVE_DB['NAME']]
cdr_db = MongoClient(host=settings.CDR_DB['HOST'],
                         tz_aware=True)[settings.CDR_DB['NAME']]
archives = archive_db[ARCHIVE_COL]
cdrs = cdr_db[CDR_COL]

def archive_cdrs(days_old=settings.CDR_ARCHIVE_DAYS, remove=True):
    """
    Archives old call detail records
    Return a tuple containing the archive threshold date and the
    number of CDRs archived
    """

    NOW = datetime.utcnow().replace(tzinfo=utc)
    ARCHIVE_DATE = NOW - timedelta(days=days_old)

    num_archived = 0

    for old_cdr in cdrs.find({"start_time": {"$lt": ARCHIVE_DATE}}):
        archives.insert(old_cdr)
        num_archived += 1

        if remove:
            cdrs.remove(old_cdr)

    if num_archived:
        archive_db.command("repairDatabase", 1)
        cdr_db.command("repairDatabase", 1)

    return (ARCHIVE_DATE, num_archived)

if __name__ == "__main__":
    archived = archive_cdrs()
    print "Archive Date: %s" % archived[0]
    print "CDRs Archived: %s" % archived[1]
