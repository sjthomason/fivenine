#!/usr/bin/env python
from django.core.management import setup_environ
import settings
setup_environ(settings)

from django.utils.timezone import utc
from datetime import datetime
import subprocess
import os, sys

from system.models import DbBackup

NOW = datetime.utcnow().replace(tzinfo=utc)
POSTGRES = 'postgres'
MYSQL = 'mysql'
SQLITE = 'sqlite'



def backup_db(db_type=POSTGRES, host='127.0.0.1', port='', user='', password=''):

    env = None
    if db_type == POSTGRES:
        backup_cmd = '/usr/bin/pg_dump'
        args = ['--create', '--column-inserts']
        if host:
            args.append('-h %s' % (host))
        if port:
            args.append('-p %s' % (port))
        if user:
            args.append('-U %s' % (user))
        if password:
            env = {'PGPASSWORD':password}

    args = ' '.join(args)
    filename = "db_backup-" + NOW.strftime('%Y-%m-%d_%H_%M_%S') + ".sql"
    target = os.path.join(settings.DB_BACKUP_ROOT, filename)
    cmd = '%s %s > %s' % (backup_cmd, args, target)
    os.umask(0066)
    p = subprocess.Popen(cmd, shell=True, env=env)
    p.wait()
    if os.path.isfile(target):
        db_backup = DbBackup(time_stamp=NOW, file_path=target)
        db_backup.save()

    return p.returncode


def remove_old_backups(keep_count=settings.DB_BACKUP_COUNT):
    db_backup_count = DbBackup.objects.count()
    print db_backup_count
    if db_backup_count > keep_count:
        for db_backup in DbBackup.objects.all()[keep_count:]:
            db_backup.delete()


if __name__ == "__main__":
    db_engine = settings.DATABASES['default']['ENGINE']
    if 'postgresql_psycopg2' in db_engine:
        db_type = POSTGRES
    elif 'mysql' in db_engine:
        db_type = MYSQL
    elif 'sqlite3' in db_engine:
        db_type = SQLITE

    ret_code = backup_db(db_type,
                    host=settings.DATABASES['default']['HOST'],
                    port=settings.DATABASES['default']['PORT'],
                    user=settings.DATABASES['default']['USER'],
                    password=settings.DATABASES['default']['PASSWORD'])
    if ret_code == 0:
        print "Database backup successful"
        remove_old_backups()
    else:
        sys.exit(1)
