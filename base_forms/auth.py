from django import forms
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.models import User

class UserPasswordResetForm(PasswordResetForm):
    """
    Accepts either an e-mail address or username and sends a password reset
    e-mail to the registered e-mail address.
    """

    email = forms.RegexField(label="Enter your e-mail address or username",
                             max_length=75,
                             regex=r'^[\w.@+-]+$',
                             help_text="If you have forgotten your password, "\
                                       "we will send you an email to reset "\
                                       "your password.",
                             error_messages = {'invalid': "This value may contain only letters, numbers and @/./+/-/_ characters."})

    def clean_email(self):
        """
        Validates that an active user exists with the given e-mail address
        or username.
        """
        email = self.cleaned_data["email"]
        # First check the e-mail address
        self.users_cache = User.objects.filter(
                                email__iexact=email,
                                is_active=True
                            )
        # Next check for a valid username
        if len(self.users_cache) == 0:
            self.users_cache = User.objects.filter(
                                    username__iexact=email,
                                    is_active=True
                                )
            if len(self.users_cache) == 0:
                raise forms.ValidationError("No matching e-mail address or username was found on file. Are you sure you've registered?")

        return email

