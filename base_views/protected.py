from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import (TemplateView, RedirectView, DetailView,
                                  FormView, CreateView, UpdateView,
                                  DeleteView, ListView)


class ProtectedTemplateView(TemplateView):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProtectedTemplateView, self).dispatch(*args, **kwargs)


class ProtectedRedirectView(RedirectView):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProtectedRedirectView, self).dispatch(*args, **kwargs)


class ProtectedDetailView(DetailView):

    def get_queryset(self, *args, **kwargs):
        queryset = super(ProtectedDetailView, self).get_queryset(*args, **kwargs)
        queryset = queryset.filter(user=self.request.user)
        return queryset

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProtectedDetailView, self).dispatch(*args, **kwargs)


class ProtectedFormView(FormView):

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProtectedFormView, self).dispatch(*args, **kwargs)


class ProtectedCreateView(CreateView):

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        return super(ProtectedCreateView, self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProtectedCreateView, self).dispatch(*args, **kwargs)


class ProtectedUpdateView(UpdateView):

    def get_queryset(self, *args, **kwargs):
        queryset = super(ProtectedUpdateView, self).get_queryset(*args, **kwargs)
        queryset = queryset.filter(user=self.request.user)
        return queryset

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProtectedUpdateView, self).dispatch(*args, **kwargs)


class ProtectedDeleteView(DeleteView):

    def get_queryset(self, *args, **kwargs):
        queryset = super(ProtectedDeleteView, self).get_queryset(*args, **kwargs)
        queryset = queryset.filter(user=self.request.user)
        return queryset

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProtectedDeleteView, self).dispatch(*args, **kwargs)


class ProtectedListView(ListView):

    def get_queryset(self, *args, **kwargs):
        queryset = super(ProtectedListView, self).get_queryset(*args, **kwargs)
        queryset = queryset.filter(user=self.request.user)
        order_by = self.request.GET.get('order_by')
        # ensure order_by contains a valid field
        if order_by and order_by.replace('-', '') \
            in self.model._meta.get_all_field_names():
            return queryset.order_by(order_by)
        return queryset

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProtectedListView, self).dispatch(*args, **kwargs)
