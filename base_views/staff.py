from django.contrib.admin.views.decorators import staff_member_required
from django.utils.decorators import method_decorator
from django.views.generic import (TemplateView, RedirectView, DetailView,
                                  FormView, CreateView, UpdateView,
                                  DeleteView, ListView)


class StaffTemplateView(TemplateView):

    @method_decorator(staff_member_required)
    def dispatch(self, *args, **kwargs):
        return super(StaffTemplateView, self).dispatch(*args, **kwargs)


class StaffRedirectView(RedirectView):

    @method_decorator(staff_member_required)
    def dispatch(self, *args, **kwargs):
        return super(StaffRedirectView, self).dispatch(*args, **kwargs)


class StaffDetailView(DetailView):

    @method_decorator(staff_member_required)
    def dispatch(self, *args, **kwargs):
        return super(StaffDetailView, self).dispatch(*args, **kwargs)


class StaffFormView(FormView):

    @method_decorator(staff_member_required)
    def dispatch(self, *args, **kwargs):
        return super(StaffFormView, self).dispatch(*args, **kwargs)


class StaffCreateView(CreateView):

    @method_decorator(staff_member_required)
    def dispatch(self, *args, **kwargs):
        return super(StaffCreateView, self).dispatch(*args, **kwargs)


class StaffUpdateView(UpdateView):

    @method_decorator(staff_member_required)
    def dispatch(self, *args, **kwargs):
        return super(StaffUpdateView, self).dispatch(*args, **kwargs)


class StaffDeleteView(DeleteView):

    @method_decorator(staff_member_required)
    def dispatch(self, *args, **kwargs):
        return super(StaffDeleteView, self).dispatch(*args, **kwargs)


class StaffListView(ListView):

    @method_decorator(staff_member_required)
    def dispatch(self, *args, **kwargs):
        return super(StaffListView, self).dispatch(*args, **kwargs)
