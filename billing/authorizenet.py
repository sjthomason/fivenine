import hmac
import re
import urllib2

from decimal import Decimal
from django.utils.http import urlencode
from utils.encryption import decrypt

from models import Payment


AUTHNET_TEST_MODE = False
AUTHNET_DEBUG = False

AUTHNET_DELIM_CHAR = '|'
AUTHNET_LOGIN_ID = '6TX5fq849'
AUTHNET_POST_URL = 'https://secure.authorize.net/gateway/transact.dll'
AUTHNET_TEST_POST_URL = 'https://test.authorize.net/gateway/transact.dll'
AUTHNET_TRANSACTION_KEY = decrypt('xM7JGhV+5Iw9tUBNt11tNSosXywt0AYtL+612ZF8nZj37ppkHKbES/LrUAHcKRtmAYKD3E0X1c39+OiD')

AIM_DEFAULT_DICT = {
    'x_login': AUTHNET_LOGIN_ID,
    'x_tran_key': AUTHNET_TRANSACTION_KEY,
    'x_delim_data': "TRUE",
    'x_delim_char': AUTHNET_DELIM_CHAR,
    'x_relay_response': "FALSE",
    'x_type': "AUTH_CAPTURE",
    'x_method': "CC",
    'x_version': "3.1"
}

AIM_RESPONSE_LIST = [
    'response_code',
    'response_subcode',
    'response_reason_code',
    'response_reason_text',
    'auth_code',
    'avs_code',
    'trans_id',
    'invoice_num',
    'description',
    'amount',
    'method',
    'type',
    'cust_id',
    'first_name',
    'last_name',
    'company',
    'address',
    'city',
    'state',
    'zip',
    'country',
    'phone',
    'fax',
    'email',
    'ship_to_first_name',
    'ship_to_last_name',
    'ship_to_company',
    'ship_to_address',
    'ship_to_city',
    'ship_to_state',
    'ship_to_zip',
    'ship_to_country',
    'tax',
    'duty',
    'freight',
    'tax_exempt',
    'po_num',
    'MD5_Hash',
    'cvv2_resp_code',
    'cavv_response'
]


class CardDeclined(Exception):
    pass


class CardProcessingError(Exception):
    pass


def get_fingerprint(x_fp_sequence, x_fp_timestamp, x_amount):
    msg = '^'.join([AUTHNET_LOGIN_ID,
                    x_fp_sequence,
                    x_fp_timestamp,
                    x_amount
                    ]) + '^'

    return hmac.new(AUTHNET_TRANSACTION_KEY, msg).hexdigest()


def get_response(aim_dict):

    if AUTHNET_TEST_MODE:
        aim_dict['x_test_request'] = 'TRUE'

    if AUTHNET_DEBUG:
        post_url = AUTHNET_TEST_POST_URL
    else:
        post_url = AUTHNET_POST_URL

    c = aim_dict['x_delim_char']
    # Escape delimiter characters in request fields
    for k, v in aim_dict.items():
        if k != 'x_delim_char':
            aim_dict[k] = unicode(v).replace(c, "\\%s" % c)
    request_string = urlencode(aim_dict)
    response = urllib2.urlopen(post_url, request_string).read()
    # Split response by delimiter,
    # unescaping delimiter characters in fields
    response = re.split("(?<!\\\\)\%s" % c, response)
    response = map(lambda s: s.replace("\\%s" % c, c), response)
    response = dict(zip(AIM_RESPONSE_LIST, response))

    if response['response_code'] == '1':
        return response

    if response['response_code'] == '2':
        raise CardDeclined(response['response_reason_text'])

    if response['response_code'] == '3':
        raise CardProcessingError(response['response_reason_text'])

    else:
        raise CardProcessingError()


def process_payment(user, cc_object, amount,
                    cvv2=None, ip_addr=None, recurring=False):
    """
    Processes a credit card payment using Authorize.net AIM API
    """

    aim_data = AIM_DEFAULT_DICT

    # ensure amount is rounded to two decimal places
    amount = float(amount)
    amount = Decimal(amount).quantize(Decimal('1.00'))

    aim_data['x_amount'] = amount
    aim_data['x_card_num'] = cc_object.num
    aim_data['x_exp_date'] = '%02d-%d' % (int(cc_object.exp_month),
                                          int(cc_object.exp_year))
    aim_data['x_first_name'] = cc_object.firstname
    aim_data['x_last_name'] = cc_object.lastname
    aim_data['x_address'] = cc_object.address
    aim_data['x_city'] = cc_object.city
    aim_data['x_state'] = cc_object.state
    aim_data['x_zip'] = cc_object.postal
    aim_data['x_country'] = cc_object.country
    aim_data['x_phone'] = cc_object.phone

    if cvv2:
        aim_data['x_card_code'] = cvv2

    if recurring:
        aim_data['x_recurring_billing'] = 'TRUE'
        aim_data['x_description'] = 'Auto Payment on Account Balance'

    if ip_addr is not None:
        aim_data['x_customer_ip'] = ip_addr

    response = get_response(aim_data)

    payment = Payment()
    payment.user = user
    payment.method = cc_object.card_type
    payment.amount = amount
    payment.ref_id = response['trans_id']
    payment.logged_ip = ip_addr
    payment.save()
    return payment

def process_refund(user, cc_object, amount, trans_id):
    aim_data = AIM_DEFAULT_DICT

    # ensure amount is rounded to two decimal places
    amount = float(amount)
    amount = Decimal(amount).quantize(Decimal('1.00'))

    aim_data['x_type'] = "CREDIT"
    aim_data['x_trans_id'] = trans_id
    aim_data['x_amount'] = amount
    aim_data['x_card_num'] = cc_object.cc_num

    response = get_response(aim_data)

    return response
