from datetime import date
from dateutil.relativedelta import relativedelta


def get_next_billing_date(last_billed_date=None, day_of_month=1, term=1):

    """
    Return a billing date given the date last billed, day of the month to
    bill on and a number of months in the term
    """

    TODAY = date.today()

    if last_billed_date is None:
        last_billed_date = TODAY

    last_billed_day = int(last_billed_date.strftime('%d'))

    cycle_year = int(last_billed_date.strftime('%Y'))
    cycle_month = int(last_billed_date.strftime('%m'))
    cycle_start_date = date(cycle_year, cycle_month, day_of_month)

    if last_billed_day < day_of_month:
        cycle_start_date -= relativedelta(months=+1)

    billing_date = cycle_start_date + relativedelta(months=+term)
    cutoff_date = cycle_start_date + relativedelta(days=+20)

    if last_billed_date >= cutoff_date:
        billing_date += relativedelta(months=+1)

    if billing_date >= TODAY:
        return billing_date
    else:
        billing_date = date(int(TODAY.strftime('%Y')),
                            int(TODAY.strftime('%m')),
                            day_of_month)
        if billing_date < TODAY:
            billing_date += relativedelta(months=+1)
        return billing_date


def months_between(date2, date1):

    """
    Returns the number of months between two dates
    i.e. date2 - date1
    """

    date1_day = int(date1.strftime('%d'))
    date1_month = int(date1.strftime('%m'))
    date1_year = int(date1.strftime('%Y'))

    date2_day = int(date2.strftime('%d'))
    date2_month = int(date2.strftime('%m'))
    date2_year = int(date2.strftime('%Y'))

    year_delta = (date1_year - date2_year) * 12

    # If both are on the same day of the month, no need to calculate days
    if date1_day == date2_day:
        month_delta = date1_month - date2_month
        return year_delta + month_delta

    DAYS_PER_MONTH = 365.25 / 12
    date1_day_of_year = int(date1.strftime('%j'))
    date2_day_of_year = int(date2.strftime('%j'))
    month_delta = (date1_day_of_year - date2_day_of_year) / DAYS_PER_MONTH
    return year_delta + month_delta
