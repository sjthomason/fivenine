from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMultiAlternatives
from django.template import loader

from gen_pdf import gen_invoice, gen_payment_receipt


FROM_ADDR = 'billing@5ninesolutions.com'
FROM_NAME = '5Nine Solutions Billing'
FROM_EMAIL = '%s <%s>' % (FROM_NAME, FROM_ADDR)


def email_invoice(invoice,
                  subject_template_name='billing/invoice_subject.txt',
                  email_template_name_html='billing/invoice_email.html',
                  email_template_name_txt='billing/invoice_email.txt',
                  from_email=FROM_EMAIL):
    """
    Generates a invoice email and attaches a pdf invoice
    """
    user = invoice.user

    try:
        profile = user.get_profile()
    except ObjectDoesNotExist:
        profile = None

    if profile is not None:
        email = profile.billing_email or user.email
    else:
        email = user.email

    c = {
        'email': email,
        'user': user,
        'invoice': invoice
    }

    subject = loader.render_to_string(subject_template_name, c)
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    email_html = loader.render_to_string(email_template_name_html, c)
    email_txt = loader.render_to_string(email_template_name_txt, c)

    msg = EmailMultiAlternatives(subject, email_txt, from_email, [email])

    bcc = getattr(settings, 'BILLING_BCC_EMAIL', None)
    if bcc:
        msg.bcc = [bcc]

    msg.attach_alternative(email_html, "text/html")

    # Generate the pdf
    pdf = gen_invoice(invoice)
    filename = 'invoice-%s.pdf' % (invoice.pk)

    msg.attach(filename, pdf, 'application/pdf')
    msg.send()

    return


def email_payment_receipt(payment_obj,
                          subject_template_name='billing/payment_receipt_subject.txt',
                          email_template_name_html='billing/payment_receipt_email.html',
                          email_template_name_txt='billing/payment_receipt_email.txt',
                          from_email=FROM_EMAIL):
    """
    Generates a payment receipt email and attaches a pdf receipt
    """
    user = payment_obj.user

    try:
        profile = user.get_profile()
    except ObjectDoesNotExist:
        profile = None

    if profile is not None:
        email = profile.billing_email or user.email
    else:
        email = user.email

    c = {
        'email': email,
        'user': user,
        'payment': payment_obj
    }

    subject = loader.render_to_string(subject_template_name, c)
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    email_html = loader.render_to_string(email_template_name_html, c)
    email_txt = loader.render_to_string(email_template_name_txt, c)

    msg = EmailMultiAlternatives(subject, email_txt, from_email, [email])

    bcc = getattr(settings, 'BILLING_BCC_EMAIL', None)
    if bcc:
        msg.bcc = [bcc]

    msg.attach_alternative(email_html, "text/html")

    # Generate the pdf
    pdf = gen_payment_receipt(payment_obj)
    filename = 'payment-receipt-%s.pdf' % (payment_obj.pk)

    msg.attach(filename, pdf, 'application/pdf')
    msg.send()

    return


def email_card_declined(user,
                        subject_template_name='billing/card_declined_subject.txt',
                        email_template_name_html='billing/card_declined_email.html',
                        email_template_name_txt='billing/card_declined_email.txt',
                        from_email=FROM_EMAIL,
                        error_msg=None):
    """
    Send an email to a user notifying them that their
    credit card payment was declined
    """
    from datetime import date

    try:
        profile = user.get_profile()
    except ObjectDoesNotExist:
        profile = None

    if profile is not None:
        email = profile.billing_email or user.email
    else:
        email = user.email

    c = {
        'email': email,
        'user': user,
        'date': date.today(),
        'error_msg': error_msg
    }

    subject = loader.render_to_string(subject_template_name, c)
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    email_html = loader.render_to_string(email_template_name_html, c)
    email_txt = loader.render_to_string(email_template_name_txt, c)

    msg = EmailMultiAlternatives(subject, email_txt, from_email, [email])

    bcc = getattr(settings, 'BILLING_BCC_EMAIL')
    if bcc:
        msg.bcc = [bcc]

    msg.attach_alternative(email_html, "text/html")
    msg.send()

    return

def email_card_expired(user,
                       subject_template_name='billing/card_expired_subject.txt',
                       email_template_name_html='billing/card_expired_email.html',
                       email_template_name_txt='billing/card_expired_email.txt',
                       from_email=FROM_EMAIL,
                       exp_date=None):
    """
    Send an email to a user notifying them that their
    credit card has expired
    """
    from datetime import date

    try:
        profile = user.get_profile()
    except ObjectDoesNotExist:
        profile = None

    if profile is not None:
        email = profile.billing_email or user.email
    else:
        email = user.email

    c = {
        'email': email,
        'user': user,
        'date': date.today(),
        'exp_date': exp_date
    }

    subject = loader.render_to_string(subject_template_name, c)
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    email_html = loader.render_to_string(email_template_name_html, c)
    email_txt = loader.render_to_string(email_template_name_txt, c)

    msg = EmailMultiAlternatives(subject, email_txt, from_email, [email])

    bcc = getattr(settings, 'BILLING_BCC_EMAIL')
    if bcc:
        msg.bcc = [bcc]

    msg.attach_alternative(email_html, "text/html")
    msg.send()

    return
