from calendar import monthrange
from datetime import date
from django import forms
from django.contrib.localflavor.us.forms import (USPhoneNumberField,
                                                 USZipCodeField)
from django.contrib.localflavor.us.us_states import US_STATES
from django.core.exceptions import ObjectDoesNotExist

from utils.forms import (CreditCardField, CreditCardTypeField,
                         CreditCardExpMonthField, CreditCardExpYearField,
                         PaymentField)

from models import CreditCard

def get_card_type(number):
    """
    Gets credit card type given number. Based on values from Wikipedia page
    "Credit card number".
    http://en.wikipedia.org/w/index.php?title=Credit_card_number
    """

    if not number:
        return

    number = str(number)

    #group checking by ascending length of number
    if len(number) == 13:
        if number[0] == '4':
            return 'VISA'
    elif len(number) == 14:
        if number[:2] == '36':
            return 'MSTR'
    elif len(number) == 15:
        if number[:2] in ('34', '37'):
            return 'AMEX'
    elif len(number) == 16:
        if number[:4] == '6011':
            return 'DISC'
        if number[:2] in ('51', '52', '53', '54', '55'):
            return 'MSTR'
        if number[0] == '4':
            return 'VISA'
    return


class CreditCardForm(forms.ModelForm):

    firstname = forms.CharField(label="First Name", max_length=64)
    lastname = forms.CharField(label="Last Name", max_length=64)
    phone = USPhoneNumberField(label="Phone Number")
    address = forms.CharField(label="Street Address", max_length=64)
    city = forms.CharField(max_length=64)
    state = forms.ChoiceField(choices=US_STATES)
    postal = USZipCodeField()
    #country = CountryField()
    card_type = CreditCardTypeField()
    num = CreditCardField(label="Card Number")
    exp_month = CreditCardExpMonthField(label="Expiration Month")
    exp_year = CreditCardExpYearField(label="Expiration Year")

    def clean_exp_year(self):
        exp_month = self.cleaned_data.get("exp_month", 1)
        exp_year = self.cleaned_data["exp_year"]

        month = int(exp_month)
        year = int(exp_year)
        # find last day of the month
        day = monthrange(year, month)[1]
        exp_date = date(year, month, day)

        if date.today() > exp_date:
            raise forms.ValidationError("The expiration date you entered is in the past.")
        return exp_year

    def clean(self):
        cleaned_data = super(CreditCardForm, self).clean()
        card_type = cleaned_data.get("card_type")
        num = cleaned_data.get("num")

        if card_type and num and get_card_type(num) != card_type:
            msg = u"The Credit Card type does not match the number entered."
            self._errors["card_type"] = self.error_class([msg])
            del cleaned_data["card_type"]

        return cleaned_data

    class Meta:
        model = CreditCard


class CardPaymentForm(CreditCardForm):

    stored_card = forms.ModelChoiceField(queryset=CreditCard.objects.none,
                                         required=False)
    cvv2 = forms.CharField(label="CVV2 Code", max_length=4,
                help_text="The verification code is a 3 or 4 digit number printed on the back of your card.")
    payment_amount = PaymentField()
    save_card = forms.BooleanField(label="Keep card on file", required=False)

    def __init__(self, user, *args, **kwargs):
        super(CardPaymentForm, self).__init__(*args, **kwargs)
        self.fields['stored_card'].queryset = CreditCard.objects.filter(user=user)
        try:
            self.fields['payment_amount'].help_text = "Current account balance: $%s" % user.get_profile().acct_balance
        except ObjectDoesNotExist:
            pass

    class Meta(CreditCardForm.Meta):
        exclude = ('autopay',)
