import os

from copy import deepcopy
from django.core.exceptions import ObjectDoesNotExist
from io import BytesIO
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas

from utils.dst import is_dst

IMG_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/img/"
LOGO = IMG_ROOT + "invoice_logo.jpg"

tz = lambda: 'PDT' if is_dst() else 'PST'
TZ = tz()

def gen_payment_receipt(payment):

    buffer = BytesIO()
    user = payment.user

    try:
        profile = user.get_profile()
    except ObjectDoesNotExist:
        profile = None

    time_stamp = payment.time_stamp
    p = canvas.Canvas(buffer, pagesize=letter)

    # header
    p.setFillColorRGB(0.4,0.4,0.4)
    p.setFont("Helvetica", 18)
    p.drawRightString(540, 724, "Payment Receipt")

    p.setFillColorRGB(0,0,0)
    p.setFont("Helvetica", 10)
    p.drawString(468, 696, "DATE")
    p.drawRightString(540, 696, time_stamp.strftime('%x'))
    p.drawString(468, 684, "ID")
    p.drawRightString(540, 684, str(payment.pk))

    # logo
    p.drawImage(LOGO, 72, 724, width=144, height=32)
    p.drawString(72, 708, "5Nine Solutions LLC")
    p.drawString(72, 696, "7111 Garden Grove Blvd, Suite 201")
    p.drawString(72, 684, "Garden Grove, CA 92841")

    # questions box
    p.setStrokeColorRGB(0.4,0.4,0.4)
    p.setFillColorRGB(0.4,0.4,0.4)
    p.rect(396, 631, 144, 18, stroke=1, fill=1)
    p.rect(396, 572, 144, 59, stroke=1, fill=0)
    p.setFillColorRGB(1,1,1)
    p.drawCentredString(468, 636, "Billing Questions?")
    p.setFillColorRGB(0,0,0)
    p.drawString(410, 612, "billing@5ninesolutions.com")
    p.drawString(410, 600, "888.271.7959")
    p.drawString(410, 588, "M-F 8-5pm %s" % TZ)

    # received from
    line_height = 12
    p.setFillColorRGB(0.4,0.4,0.4)
    p.setFont("Helvetica", 14)
    p.drawString(72, 633, "RECEIVED FROM")
    p.setFillColorRGB(0,0,0)
    p.setFont("Helvetica", 10)
    y = 612
    p.drawString(72, y, user.get_full_name())
    if profile is not None:
        if profile.company_name:
            y -= line_height
            p.drawString(72, y, profile.company_name)
        y -= line_height
        p.drawString(72, y, profile.address_line1)
        if profile.address_line2:
            y -= line_height
            p.drawString(72, y, profile.address_line2)
            y -= line_height
            p.drawString(72, y, "%s, %s %s" % (profile.address_city,
                                               profile.address_state,
                                               profile.address_postal))
        else:
            y -= line_height
            p.drawString(72, y, "%s, %s %s" % (profile.address_city,
                                               profile.address_state,
                                               profile.address_postal))

    # transaction box
    p.setStrokeColorRGB(0.4,0.4,0.4)
    p.setFillColorRGB(0.4,0.4,0.4)
    p.rect(72, 500, 468, 18, stroke=1, fill=1)
    p.grid([72, 228, 384, 540], [500,482])

    p.setFillColorRGB(1,1,1)
    p.drawCentredString(150, 505, "Payment Method")
    p.drawCentredString(306, 505, "Reference Number")
    p.drawCentredString(462, 505, "Amount")
    p.setFillColorRGB(0,0,0)
    p.drawCentredString(150, 487, payment.get_method_display())
    p.drawCentredString(306, 487, payment.ref_id)
    p.drawCentredString(462, 487, str(payment.amount))

    # footer
    p.setFillColorRGB(0.4,0.4,0.4)
    p.setFont("Helvetica", 12)
    p.drawCentredString(306, 72, "Manage your account and view current balance at www.5ninesolutions.com")
    p.setFont("Helvetica", 6)
    p.drawCentredString(306, 36, "7111 Garden Grove Blvd, Suite 201, Garden Grove, CA 92841    888.271.7959    support@5ninesolutions.com")
    p.setFont("Helvetica", 10)

    p.showPage()
    p.save()

    pdf = buffer.getvalue()
    buffer.close()
    return pdf


def gen_invoice(invoice):

    buffer = BytesIO()

    user = invoice.user

    try:
        profile = user.get_profile()
    except ObjectDoesNotExist:
        profile = None

    time_stamp = invoice.time_stamp
    date_due = invoice.date_due
    items = invoice.items
    p = canvas.Canvas(buffer, pagesize=letter)

    # header
    p.setFillColorRGB(0.4,0.4,0.4)
    p.setFont("Helvetica", 18)
    p.drawRightString(540, 724, "Invoice")

    p.setFillColorRGB(0,0,0)
    p.setFont("Helvetica", 10)
    p.drawString(440, 696, "DATE")
    p.drawRightString(540, 696, time_stamp.strftime('%x'))
    p.drawString(440, 684, "NUMBER")
    p.drawRightString(540, 684, str(invoice.pk))
    p.drawString(440, 672, "DUE DATE")
    p.drawRightString(540, 672, date_due.strftime('%x'))

    # logo
    p.drawImage(LOGO, 72, 724, width=144, height=32)
    p.drawString(72, 708, "5Nine Solutions LLC")
    p.drawString(72, 696, "7111 Garden Grove Blvd, Suite 201")
    p.drawString(72, 684, "Garden Grove, CA 92841")

    # questions box
    p.setStrokeColorRGB(0.4,0.4,0.4)
    p.setFillColorRGB(0.4,0.4,0.4)
    p.rect(396, 631, 144, 18, stroke=1, fill=1)
    p.rect(396, 572, 144, 59, stroke=1, fill=0)
    p.setFillColorRGB(1,1,1)
    p.drawCentredString(468, 636, "Questions?")
    p.setFillColorRGB(0,0,0)
    p.drawString(405, 612, "support@5ninesolutions.com")
    p.drawString(405, 600, "888.271.7959")
    p.drawString(405, 588, "M-F 8-5pm %s" % TZ)

    # bill to
    line_height = 12
    p.setFillColorRGB(0.4,0.4,0.4)
    p.setFont("Helvetica", 14)
    p.drawString(72, 633, "BILL TO")
    p.setFillColorRGB(0,0,0)
    p.setFont("Helvetica", 10)
    y = 612
    p.drawString(72, y, user.get_full_name())
    if profile is not None:
        if profile.company_name:
            y -= line_height
            p.drawString(72, y, profile.company_name)
        y -= line_height
        p.drawString(72, y, profile.address_line1)
        if profile.address_line2:
            y -= line_height
            p.drawString(72, y, profile.address_line2)
            y -= line_height
            p.drawString(72, y, "%s, %s %s" % (profile.address_city,
                                               profile.address_state,
                                               profile.address_postal))
        else:
            y -= line_height
            p.drawString(72, y, "%s, %s %s" % (profile.address_city,
                                               profile.address_state,
                                               profile.address_postal))

    start_y = 500
    row_height = 18
    page_start_y = 720
    page_min_y = 150
    pages = []
    page = { 'grid_y': [], 'grid_items': [] }

    # build list of y coords
    y = start_y
    grid_rows = max(len(items), 6)
    for i in xrange(0, grid_rows):
        item_y = y - 13
        page['grid_y'].append(y)

        try:
            grid_item = (items[i], item_y,)
            page['grid_items'].append(grid_item)
        except IndexError:
            pass

        y -= row_height

        if y <= page_min_y:
            page['grid_y'].append(y)
            pages.append(deepcopy(page))
            page = { 'grid_y': [], 'grid_items': [] }
            y = page_start_y

    # add last page
    page['grid_y'].append(y)
    pages.append(deepcopy(page))

    # helpers
    first_page = pages[0]
    last_page = pages[-1]

    for page in pages:
        if page != first_page:
            p.showPage()

        p.setFillColorRGB(0.4,0.4,0.4)
        p.setFont("Helvetica", 6)
        p.drawRightString(540, 750, "Page %s of %s" % (pages.index(page) + 1,
                                                       pages.index(last_page) +1))
        p.setFont("Helvetica", 9)
        p.setFillColorRGB(0,0,0)

        # item header box
        p.setStrokeColorRGB(0.4,0.4,0.4)
        p.setFillColorRGB(0.4,0.4,0.4)
        p.rect(72, page['grid_y'][0], 468, row_height, stroke=1, fill=1)

        # item header
        p.setFillColorRGB(1,1,1)
        p.drawCentredString(100, page['grid_y'][0] + 5, "Item")
        p.drawCentredString(239, page['grid_y'][0] + 5, "Description")
        p.drawCentredString(423, page['grid_y'][0] + 5, "Qty")
        p.drawCentredString(465, page['grid_y'][0] + 5, "Rate")
        p.drawCentredString(512, page['grid_y'][0] + 5, "Amount")

        # alternate row background
        p.setStrokeColorRGB(0.9,0.9,0.9)
        p.setFillColorRGB(0.9,0.9,0.9)
        rect_y = page['grid_y'][2::2]
        for y in rect_y:
            p.rect(72, y, 468, row_height, stroke=1, fill=1)

        # draw grid
        p.setStrokeColorRGB(0.4,0.4,0.4)
        p.setFillColorRGB(0.4,0.4,0.4)
        p.grid([72, 129, 405, 440, 485, 540], page['grid_y'])

        # Add the invoice items
        p.setFillColorRGB(0,0,0)

        for item, y in page['grid_items']:
            p.drawString(80, y, item.name)
            p.drawString(135, y, item.description)
            p.drawCentredString(423, y, str(item.quantity))
            p.drawRightString(480, y, str(item.rate))
            p.drawRightString(532, y, str(item.amount))

        # footer
        p.setFillColorRGB(0.4,0.4,0.4)
        p.setFont("Helvetica", 6)
        p.drawCentredString(306, 36, "7111 Garden Grove Blvd, Suite 201, Garden Grove, CA 92841    888.271.7959    support@5ninesolutions.com")

    # Totals
    p.setFillColorRGB(0,0,0)
    p.setFont("Helvetica", 14)
    p.drawRightString(460, last_page['grid_y'][-1] - 36, "TOTAL DUE:")
    p.drawRightString(532, last_page['grid_y'][-1] - 36, "$%s" % invoice.amount)
    if profile.autopay_enabled:
        p.drawRightString(532, last_page['grid_y'][-1] - 72, "**AUTO PAYMENT ON YOUR DUE DATE - DO NOT SEND PAYMENT**")

    p.setFillColorRGB(0.4,0.4,0.4)
    p.setFont("Helvetica", 12)
    p.drawCentredString(306, 72, "Manage your account and view current balance at www.5ninesolutions.com")

    p.showPage()
    p.save()

    pdf = buffer.getvalue()
    buffer.close()
    return pdf

