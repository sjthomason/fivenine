import simplejson as json

from datetime import date
from django.conf import settings
from django.contrib.localflavor.us.models import USStateField
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models.signals import post_save

from billing_utils import get_next_billing_date
from email import email_invoice, email_payment_receipt
from utils.cache import key_from_args
from utils.encryption import decrypt, encrypt
from utils.forms import PAYMENT_METHOD_CHOICES, mask_cc
from utils.models import CountryField


TERM_CHOICES = (
    (1, "Monthly"),
    (3, "Quarterly"),
    (12, "Annually"),
)

INVOICEITEM_CHOICES = (
    ('product', "Products"),
    ('service', "Services"),
    ('tax', "Taxes and Surcharges"),
    ('late', "Late Charges"),
    ('shipping', "Shipping Charges"),
    ('misc', "Misc. Adjustments"),
)

TODAY = date.today()


def exclusive_boolean_handler(sender, instance, created, **kwargs):
    eb_fields = getattr(sender, '_exclusive_boolean_fields', [])
    with_fields = getattr(sender, '_exclusive_boolean_with_fields', [])
    uargs = {}
    for field in eb_fields:
        ifield = getattr(instance, field)
        if ifield == True:
            uargs.update({field:False})
    fargs = {}
    for field in with_fields:
        ifield = getattr(instance, field)
        fargs.update({field:ifield})
    sender.objects.filter(**fargs).exclude(pk=instance.pk).update(**uargs)

def exclusive_boolean_fields(model, eb_fields=[], with_fields=[]):
    setattr(model, '_exclusive_boolean_fields', eb_fields)
    setattr(model, '_exclusive_boolean_with_fields', with_fields)
    post_save.connect(exclusive_boolean_handler, sender=model)


class CreditCard(models.Model):
    """
    Stores an encrypted credit card number and billing information
    """

    user = models.ForeignKey(User, editable=False)
    data = models.TextField(editable=False)
    autopay = models.BooleanField("enable autopay", default=False)
    declined_stamp = models.DateTimeField(blank=True, null=True, editable=False)
    exp_notice_stamp = models.DateTimeField(blank=True, null=True,
                                            editable=False)

    _encrypted_fields = ['firstname', 'lastname', 'phone', 'address',
                         'city', 'state', 'postal', 'country', 'card_type',
                         'num', 'exp_month', 'exp_year']

    enable_save = False

    class Meta:
        verbose_name = "Credit Card"
        verbose_name_plural = "Credit Cards"

    def __init__(self, *args, **kwargs):
        super(CreditCard, self).__init__(*args, **kwargs)
        if self.pk:
            self.enable_save = True
        if self.data:
            data = decrypt(self.data)
            for field in self._encrypted_fields:
                field_data = data.get(field)
                setattr(self, field, field_data)
        else:
            for field in self._encrypted_fields:
                setattr(self, field, None)

    def save(self, *args, **kwargs):
        if self.enable_save:
            from django.core.exceptions import ValidationError

            data = {}
            for field in self._encrypted_fields:
                data[field] = getattr(self, field, None)

            for k in data:
                if not data[k]:
                    raise ValidationError("%s must not be blank or null" % k)
            self.data = encrypt(data)
            super(CreditCard, self).save(*args, **kwargs)

    def __unicode__(self):
        return mask_cc(self.num)

    def get_exp_date(self):
        import calendar
        year = int(self.exp_year)
        month = int(self.exp_month)
        day = calendar.monthrange(year, month)[1]
        return date(year, month, day)

exclusive_boolean_fields(CreditCard, ('autopay',), ('user',))


class RecurringItemBase(models.Model):

    user = models.ForeignKey(User, blank=True, null=True)
    billing_start = models.DateField(blank=True, null=True)
    billing_term = models.IntegerField(choices=TERM_CHOICES)
    billing_last_invoice = models.DateField(blank=True, null=True)
    billing_last_renew = models.DateField(blank=True, null=True)
    price_override = models.DecimalField(max_digits=10, decimal_places=2,
                                         blank=True, null=True)
    price_override_exp_date = models.DateField(blank=True, null=True)

    _orig_user = None
    _orig_price_override = None

    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        super(RecurringItemBase, self).__init__(*args, **kwargs)
        self._orig_user = self.user
        self._orig_price_override = self.price_override

    def save(self, *args, **kwargs):
        if self._orig_user and self.user != self._orig_user:
            self.billing_start = None
            self.billing_last_invoice = None
            self.billing_last_renew = None
            # If the user changed but the price override did not,
            # remove the old value
            if self.price_override == self._orig_price_override:
                self.price_override = None
                self.price_override_exp_date = None

        super(RecurringItemBase, self).save(*args, **kwargs)
        self._orig_user = self.user
        self._orig_price_override = self.price_override

    def get_billing_next_invoice(self):
        if self.billing_start:
            from datetime import timedelta
            # this is the first time this has been billed
            if not self.billing_last_renew:
                if self.billing_start >= TODAY:
                    return self.billing_start
                return TODAY
            next_invoice = self.billing_last_renew - \
                           timedelta(days=settings.RECURRING_INV_TERM)
            if next_invoice >= TODAY:
                return next_invoice
            else:
                return TODAY

    def get_billing_next_renew(self):
        if self.billing_start:
            return get_next_billing_date(self.billing_last_renew,
                                         settings.RECURRING_RENEW_DAY,
                                         self.billing_term)

    billing_next_invoice = property(get_billing_next_invoice)
    billing_next_renew = property(get_billing_next_renew)


class RecurringPlanBase(models.Model):

    name = models.SlugField()
    monthly_price = models.DecimalField(max_digits=10, decimal_places=2)
    setup_fee = models.DecimalField(max_digits=10, decimal_places=2)
    description = models.CharField(max_length=64)
    display_data = models.TextField(blank=True, null=True)

    class Meta:
        abstract = True
        ordering = ['name']

    def __unicode__(self):
        return "%s ($%s/mo)" % (self.name, self.monthly_price)


class DidPlan(RecurringPlanBase):
    pass


class CpanelPlan(RecurringPlanBase):

    class Meta:
        verbose_name = "web hosting plan"
        ordering = ['monthly_price']


class OtherPlan(RecurringPlanBase):
    pass


class PbxPlan(RecurringPlanBase):

    class Meta:
        verbose_name = "PBX hosting plan"
        ordering = ['monthly_price']


class SipChannelPlan(RecurringPlanBase):
    pass


class Provider(models.Model):
    name = models.CharField(max_length=128)
    address_line1 = models.CharField(max_length=128)
    address_line2 = models.CharField(max_length=128, blank=True)
    address_city = models.CharField(max_length=64)
    address_state = USStateField()
    address_postal = models.CharField(max_length=16)
    address_country = CountryField(default='US')
    phone_num = models.CharField(max_length=16)
    fax_num = models.CharField(max_length=16, blank=True)
    website_url = models.URLField(blank=True, null=True)
    portal_url = models.URLField(blank=True, null=True)
    billing_contact_name = models.CharField(max_length=128, blank=True, null=True)
    billing_contact_phone = models.CharField(max_length=16, blank=True, null=True)
    billing_contact_email = models.EmailField(blank=True, null=True)
    prov_contact_name = models.CharField(max_length=128, blank=True, null=True)
    prov_contact_phone = models.CharField(max_length=16, blank=True, null=True)
    prov_contact_email = models.EmailField(blank=True, null=True)
    support_contact_name = models.CharField(max_length=128, blank=True, null=True)
    support_contact_phone = models.CharField(max_length=16, blank=True, null=True)
    support_contact_email = models.EmailField(blank=True, null=True)

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return self.name


class TransactionManager(models.Manager):

    def get_balance(self, enable_cache=True, *args, **kwargs):
        key = key_from_args('get_balance', *args, **kwargs)
        if enable_cache:
            balance = cache.get(key)
            if balance is not None:
                return balance

        qset = super(TransactionManager, self).get_query_set().filter(*args, **kwargs)
        balance = qset.aggregate(models.Sum('_amount'))['_amount__sum']
        if balance is None:
            from decimal import Decimal
            balance = Decimal(0)
        cache.set(key, balance, 600)
        return balance

    def get_credit_sum(self, *args, **kwargs):
        qset = super(TransactionManager, self).get_query_set().filter(*args, **kwargs)
        credits = qset.filter(_amount__gt=0)
        credit_sum = credits.aggregate(models.Sum('_amount'))['_amount__sum']
        if credit_sum is None:
            from decimal import Decimal
            credit_sum = Decimal(0)
        return credit_sum

    def get_debit_sum(self, *args, **kwargs):
        qset = super(TransactionManager, self).get_query_set().filter(*args, **kwargs)
        debits = qset.filter(_amount__lt=0)
        debit_sum = debits.aggregate(models.Sum('_amount'))['_amount__sum']
        if debit_sum is None:
            from decimal import Decimal
            debit_sum = Decimal(0)
        debit_sum = abs(debit_sum)
        return debit_sum


class Transaction(models.Model):

    objects = TransactionManager()

    user = models.ForeignKey(User)
    time_stamp = models.DateTimeField('created', db_index=True)
    description = models.CharField(max_length=255, blank=True, null=True)

    # this should not be set directly.  Instead use the properties
    # credit_amount and debit_amount as they ensure the proper value will be stored
    _amount = models.DecimalField(db_column='amount', max_digits=10,
                                  decimal_places=2, editable=False)

    class Meta:
        ordering = ['-time_stamp']

    def get_credit_amount(self):
        if self._amount > 0:
            return self._amount

    def set_credit_amount(self, value):
        self._amount = abs(value)

    def get_debit_amount(self):
        if self._amount < 0:
            return abs(self._amount)

    def set_debit_amount(self, value):
        self._amount = -abs(value)

    def get_balance(self):
        return self._default_manager.get_balance(user=self.user, time_stamp__lte=self.time_stamp)

    def get_prev_balance(self):
        return self._default_manager.get_balance(user=self.user, time_stamp__lt=self.time_stamp)

    _orig_amount = None

    credit_amount = property(get_credit_amount, set_credit_amount)
    debit_amount = property(get_debit_amount, set_debit_amount)
    balance = property(get_balance)
    prev_balance = property(get_prev_balance)

    def __init__(self, *args, **kwargs):
        super(Transaction, self).__init__(*args, **kwargs)
        if self._amount is None:
            from decimal import Decimal
            self._amount = Decimal(0)
        if self.time_stamp is None:
            from datetime import datetime
            from django.utils.timezone import utc
            self.time_stamp = datetime.utcnow().replace(tzinfo=utc)
        self._orig_amount = self._amount

    def save(self, *args, **kwargs):
        # clear balance cache is this is a new transaction
        if self._orig_amount != self._amount or self.pk is None:
            key = key_from_args('get_balance', user=self.user)
            cache.delete(key)
        super(Transaction, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        # clear balance cache on delete
        key = key_from_args('get_balance', user=self.user)
        cache.delete(key)
        super(Transaction, self).delete(*args, **kwargs)


class InvoiceManager(TransactionManager):

    def get_overdue(self, *args, **kwargs):
        qset = super(InvoiceManager, self).get_query_set().filter(*args, **kwargs)
        key = key_from_args('get_overdue', *args, **kwargs)
        overdue_list = cache.get(key)
        if overdue_list is None:
            overdue_list = []
            for model in qset:
                if model.overdue:
                    overdue_list.append(model.pk)
            cache.set(key, overdue_list, 600)
        return qset.filter(pk__in=overdue_list)


class Invoice(Transaction):

    objects = InvoiceManager()

    number = models.AutoField(primary_key=True)
    date_due = models.DateField()
    void = models.BooleanField(default=False)
    overdue_sent_stamp = models.DateTimeField(blank=True, null=True,
                                              editable=False)

    def get_items(self):
        return self.invoiceitem_set.all()

    items = property(get_items)

    def get_amount(self):
        if not self.void:
            return self.debit_amount
        else:
            # if the invoice is void, return the sum of the items,
            # not the stored amount
            amount = 0
            if self.items:
                for item in self.items:
                    amount += item.amount
            return amount

    def set_amount(self):
        if not self.void:
            amount = 0
            if self.items:
                for item in self.items:
                    amount += item.amount
            self.debit_amount = amount
        else:
            self.debit_amount = 0

    amount = property(get_amount)

    def is_paid(self):
        credits = super(Invoice, self)._default_manager.\
                    get_credit_sum(user=self.user,
                                   time_stamp__gte=self.time_stamp)

        balance = super(Invoice, self)._default_manager.\
                    get_balance(enable_cache=False,
                                user=self.user,
                                time_stamp__lte=self.time_stamp)

        if (credits + balance) >= 0:
            return True
        return False

    def is_overdue(self):
        if not self.is_paid() and date.today() > self.date_due:
            return True
        return False

    overdue = property(is_overdue)

    def get_absolute_url(self):
        return "/accounts/billing/invoices/%s/" % self.pk

    def email_to_user(self):
        email_invoice(self)

    def __unicode__(self):
        return '%s - %s' % (self.pk, self.amount)

    def save(self, *args, **kwargs):
        self.set_amount()
        if not self.description:
            if self.pk is None:
                super(Invoice, self).save(*args, **kwargs)
            self.description = "Invoice %s" % self.pk
        super(Invoice, self).save(*args, **kwargs)


class InvoiceItem(models.Model):

    invoice = models.ForeignKey(Invoice, editable=False)
    item_type = models.CharField(max_length=16, choices=INVOICEITEM_CHOICES,
                                 db_index=True)
    name = models.CharField(max_length=16)
    description = models.CharField(max_length=128)
    quantity = models.IntegerField()
    rate = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        ordering = ["id"]

    ## TODO: Add Tax Model as foreign key

    def get_amount(self):
        return self.quantity * self.rate

    amount = property(get_amount)

    def __unicode__(self):
        return "%s - %s - Amount:$ %s" % (self.name, self.description, self.amount)

    def save(self, *args, **kwargs):
        super(InvoiceItem, self).save(*args, **kwargs)
        self.invoice.save()


class Payment(Transaction):

    objects = TransactionManager()

    number = models.AutoField(primary_key=True)
    method = models.CharField('payment method', max_length=16,
                              choices=PAYMENT_METHOD_CHOICES)
    ref_id = models.CharField('reference number', max_length=64,
                               blank=True, null=True)
    memo = models.TextField('memo', blank=True, null=True)
    logged_ip = models.GenericIPAddressField(blank=True, null=True,
                                             editable=False)

    def get_amount(self):
        return self.credit_amount

    def set_amount(self, value):
        self.credit_amount = value

    amount = property(get_amount, set_amount)

    def get_absolute_url(self):
        return "/accounts/billing/payment/%s/" % self.pk

    def email_to_user(self):
        email_payment_receipt(self)

    def __unicode__(self):
        return '%s - %s' % (self.pk, self.amount)

    def save(self, *args, **kwargs):
        if not self.description:
            if self.pk is None:
                super(Payment, self).save(*args, **kwargs)
            self.description = "Payment ID:%s  Method:%s" % (self.pk, self.get_method_display())
        super(Payment, self).save(*args, **kwargs)
