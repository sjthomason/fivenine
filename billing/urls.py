from django.conf.urls.defaults import patterns, url

from views import *

urlpatterns = patterns('',
    url(r'^$', TransactionListView.as_view()),
    url(r'^history/$', TransactionListView.as_view()),
    url(r'^invoices/$', InvoiceListView.as_view()),
    url(r'^invoices/(?P<pk>\d+)/$', InvoiceDetailView.as_view()),
    url(r'^invoices/(?P<pk>\d+)/as-pdf/$', InvoicePdfDetailView.as_view()),
    url(r'^payment/$', CardPaymentView.as_view()),
    url(r'^payment/(?P<pk>\d+)/$', PaymentDetailView.as_view()),
    url(r'^payment/(?P<pk>\d+)/as-pdf/$', PaymentPdfDetailView.as_view()),
    url(r'^stored-cards/$', CreditCardListView.as_view()),
    url(r'^stored-cards/add/$', CreditCardCreateView.as_view()),
    url(r'^stored-cards/(?P<pk>\d+)/edit/$', CreditCardUpdateView.as_view()),
    url(r'^stored-cards/(?P<pk>\d+)/delete/$', CreditCardDeleteView.as_view()),
)
