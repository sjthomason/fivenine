from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.forms import ValidationError
from django.http import HttpResponseRedirect

from authorizenet import process_payment, CardDeclined, CardProcessingError
from base_views.protected import (ProtectedTemplateView, ProtectedListView,
                                  ProtectedCreateView, ProtectedUpdateView,
                                  ProtectedDeleteView, ProtectedDetailView)
from forms import CreditCardForm, CardPaymentForm
from models import CreditCard, Invoice, Payment, Transaction


class CardPaymentView(ProtectedCreateView):

    form_class = CardPaymentForm
    model = CreditCard
    template_name = "billing/cardpayment_form.html"

    def get_client_ip(self):
        request = self.request
        ip_address = request.META.get('HTTP_X_FORWARDED_FOR', None) or request.META.get('REMOTE_ADDR')
        return ip_address

    def get_initial(self):
        user = self.request.user
        initial = {}
        initial['firstname'] = user.first_name
        initial['lastname'] = user.last_name

        try:
            profile = user.get_profile()
        except ObjectDoesNotExist:
            profile = None

        if profile is not None:
            initial['state'] = profile.address_state

        return initial

    def get_form(self, form_class):
        return form_class(self.request.user, **self.get_form_kwargs())

    def form_valid(self, form):
        user = self.request.user
        cvv2 = form.cleaned_data['cvv2']
        client_ip = self.get_client_ip()

        # credit card
        self.object = form.save(commit=False)
        self.object.user = user
        self.object.firstname = form.cleaned_data['firstname']
        self.object.lastname = form.cleaned_data['lastname']
        self.object.phone = form.cleaned_data['phone']
        self.object.address = form.cleaned_data['address']
        self.object.city = form.cleaned_data['city']
        self.object.state = form.cleaned_data['state']
        self.object.postal = form.cleaned_data['postal']
        #self.object.country = form.cleaned_data['country']
        self.object.country = 'US'
        self.object.card_type = form.cleaned_data['card_type']
        self.object.num = form.cleaned_data['num']
        self.object.exp_month = form.cleaned_data['exp_month']
        self.object.exp_year = form.cleaned_data['exp_year']
        self.object.autopay = False
        if form.cleaned_data['save_card'] == True:
            self.object.enable_save = True
            self.object.save()

        payment_amount = form.cleaned_data['payment_amount']
        try:
            payment = process_payment(user, self.object, payment_amount,
                                      cvv2=cvv2, ip_addr=client_ip)
            payment.email_to_user()
            self.success_url = payment.get_absolute_url()
            return HttpResponseRedirect(self.get_success_url())
        except CardDeclined, e:
            form._errors.clear()
            msg = "Card declined: %s" % (e)
            form._errors['num'] = form.error_class([msg])
            form.cleaned_data={}
            return super(CardPaymentView, self).form_invalid(form)
        except CardProcessingError:
            form._errors.clear()
            msg = "Card processing error"
            form._errors['num'] = form.error_class([msg])
            form.cleaned_data={}
            return super(CardPaymentView, self).form_invalid(form)

    def form_invalid(self, form):
        # the user might have selected a stored card
        self.object = form.fields['stored_card'].clean(self.request.POST['stored_card'])
        try:
            payment_amount = form.fields['payment_amount'].clean(self.request.POST['payment_amount'])
        except ValidationError, e:
            return super(CardPaymentView, self).form_invalid(form)

        if self.object is not None:
            try:
                payment = process_payment(self.request.user, self.object,
                                          payment_amount,
                                          ip_addr=self.get_client_ip())
                payment.email_to_user()
                self.success_url = payment.get_absolute_url()
                return HttpResponseRedirect(self.get_success_url())
            except CardDeclined, e:
                form._errors.clear()
                msg = "Card declined: %s" % (e)
                form._errors['num'] = form.error_class([msg])
            except CardProcessingError, e:
                form._errors.clear()
                msg = "Card processing error" % (e)
                form._errors['num'] = form.error_class([msg])

        return super(CardPaymentView, self).form_invalid(form)


class CreditCardListView(ProtectedListView):

    model = CreditCard

    def get_queryset(self):
        return CreditCard.objects.filter(user=self.request.user)


class CreditCardCreateView(ProtectedCreateView):

    form_class = CreditCardForm
    model = CreditCard
    success_url = "/accounts/billing/stored-cards/"
    template_name = "billing/creditcard_form.html"

    def get_initial(self):
        user = self.request.user
        initial = {}
        initial['firstname'] = user.first_name
        initial['lastname'] = user.last_name

        try:
            profile = user.get_profile()
        except ObjectDoesNotExist:
            profile = None

        if profile is not None:
            initial['state'] = profile.address_state

        return initial

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.firstname = form.cleaned_data['firstname']
        self.object.lastname = form.cleaned_data['lastname']
        self.object.phone = form.cleaned_data['phone']
        self.object.address = form.cleaned_data['address']
        self.object.city = form.cleaned_data['city']
        self.object.state = form.cleaned_data['state']
        self.object.postal = form.cleaned_data['postal']
        #self.object.country = form.cleaned_data['country']
        self.object.country = 'US'
        self.object.card_type = form.cleaned_data['card_type']
        self.object.num = form.cleaned_data['num']
        self.object.exp_month = form.cleaned_data['exp_month']
        self.object.exp_year = form.cleaned_data['exp_year']
        self.object.enable_save = True

        return super(CreditCardCreateView, self).form_valid(form)


class CreditCardUpdateView(ProtectedUpdateView):

    form_class = CreditCardForm
    model = CreditCard
    success_url = "/accounts/billing/stored-cards/"
    template_name = "billing/creditcard_form.html"

    def get_initial(self):

        initial = {}
        initial['firstname'] = self.object.firstname
        initial['lastname'] = self.object.lastname
        initial['phone'] = self.object.phone
        initial['address'] = self.object.address
        initial['city'] = self.object.city
        initial['state'] = self.object.state
        initial['postal'] = self.object.postal
        #initial['country'] = self.object.country
        initial['card_type'] = self.object.card_type
        initial['num'] = self.object.num
        initial['exp_month'] = self.object.exp_month
        initial['exp_year'] = self.object.exp_year

        return initial

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.firstname = form.cleaned_data['firstname']
        self.object.lastname = form.cleaned_data['lastname']
        self.object.phone = form.cleaned_data['phone']
        self.object.address = form.cleaned_data['address']
        self.object.city = form.cleaned_data['city']
        self.object.state = form.cleaned_data['state']
        self.object.postal = form.cleaned_data['postal']
        #self.object.country = form.cleaned_data['country']
        self.object.country = 'US'
        self.object.card_type = form.cleaned_data['card_type']
        self.object.num = form.cleaned_data['num']
        self.object.exp_month = form.cleaned_data['exp_month']
        self.object.exp_year = form.cleaned_data['exp_year']
        self.object.enable_save = True

        return super(CreditCardUpdateView, self).form_valid(form)


class CreditCardDeleteView(ProtectedDeleteView):

    model = CreditCard
    success_url = "/accounts/billing/stored-cards/"


class InvoiceListView(ProtectedListView):

    model = Invoice
    template_name = "billing/invoice_list.html"

    def get_queryset(self):
        return Invoice.objects.filter(user=self.request.user)


class InvoiceDetailView(ProtectedDetailView):

    model = Invoice
    template_name = "billing/invoice_detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super(InvoiceDetailView, self).get_context_data(*args, **kwargs)
        context['user'] = self.object.user
        try:
            context['profile'] = self.object.user.get_profile()
        except ObjectDoesNotExist:
            pass

        return context


class InvoicePdfDetailView(ProtectedDetailView):

    model = Invoice

    def render_to_response(self, context, **response_kwargs):
        from django.http import HttpResponse
        from gen_pdf import gen_invoice

        # Create the HttpResponse object with the appropriate PDF headers.
        response = HttpResponse(mimetype='application/pdf')
        response['Content-Disposition'] = 'filename=invoice-%s.pdf' % (
            self.object.pk)

        pdf = gen_invoice(self.object)
        response.write(pdf)
        return response


class PaymentDetailView(ProtectedDetailView):

    model = Payment
    context_object_name = 'payment'
    template_name = "billing/payment_detail.html"


class PaymentPdfDetailView(ProtectedDetailView):

    model = Payment

    def render_to_response(self, context, **response_kwargs):
        from django.http import HttpResponse
        from gen_pdf import gen_payment_receipt

        # Create the HttpResponse object with the appropriate PDF headers.
        response = HttpResponse(mimetype='application/pdf')
        response['Content-Disposition'] = 'filename=payment-receipt-%s.pdf' % (
            self.object.pk)
        #response['Content-Disposition'] = 'attachment; filename=somefilename.pdf'

        pdf = gen_payment_receipt(self.object)
        response.write(pdf)
        return response


class TransactionListView(ProtectedListView):

    model = Transaction
    template_name = "billing/transaction_list.html"

    def get_queryset(self):
        return Transaction.objects.filter(user=self.request.user).exclude(_amount=0)


