from django import forms

from portal.models import Endpoint

class CdrForm(forms.Form):
    DIRECTION_CHOICES = (
        ('', 'Any'),
        ('inbound', 'Inbound'),
        ('outbound', 'Outbound'),
        ('onnet', 'On Network'),
    )

    direction = forms.ChoiceField(choices=DIRECTION_CHOICES, required=False)

    def __init__(self, user=None, *args, **kwargs):
        super(CdrForm, self).__init__(*args, **kwargs)
        endpoint_list = Endpoint.objects.all().filter(user=user)
        if len(endpoint_list) > 1:
            self.fields['endpoint'] = forms.ChoiceField(
                choices=[('', 'All')] + [(ep.ep_id, str(ep)) for ep in
                                         endpoint_list],
                required=False)
