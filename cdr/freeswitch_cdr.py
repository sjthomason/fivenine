import simplejson as json

from datetime import datetime
from django.utils.timezone import utc


def epoch_to_dt(epoch_secs=0):

    """
    Converts a timestamp expressed as epoch seconds to a DateTime object
    """

    epoch_secs = int(epoch_secs)
    return datetime.utcfromtimestamp(epoch_secs).replace(tzinfo=utc)


def str_to_bool(string):

    """
    Converts a boolean value represented as a string to a boolean object
    """

    string = str(string)
    true_vals = ['t', 'true', '1', 'y', 'yes']
    if string.lower() in true_vals:
        return True
    else:
        return False


def decode_fs_post(post_data):
    """
    Takes a raw FreeSWITCH JSON POST and returns a dictionary of values
    """

    json_cdr = json.loads(post_data)

    cdr = {}

    try:
        cdr['user_id'] = int(json_cdr['variables']['user_id'])
    except (KeyError, ValueError):
        cdr['user_id'] = None

    try:
        cdr['ep_id'] = json_cdr['variables']['ep_id']
    except KeyError:
        cdr['ep_id'] = None

    try:
        cdr['context'] = json_cdr['callflow']['caller_profile']['context']
    except KeyError:
        cdr['context'] = None

    try:
        cdr['uuid'] = json_cdr['callflow']['caller_profile']['uuid']
    except KeyError:
        cdr['uuid'] = None

    try:
        cdr['bleg_uuid'] = json_cdr['callflow']['caller_profile']\
                                   ['originatee']['originatee_caller_profile']\
                                   ['uuid']
    except KeyError:
        cdr['bleg_uuid'] = None

    try:
        cdr['call_direction'] = json_cdr['variables']['call_direction']
    except KeyError:
        cdr['call_direction'] = None

    # Times
    try:
        cdr['start_time'] = epoch_to_dt(json_cdr['variables']['start_epoch'])
    except (KeyError, ValueError):
        cdr['start_time'] = None

    try:
        cdr['answer_time'] = epoch_to_dt(json_cdr['variables']['answer_epoch'])
    except (KeyError, ValueError):
        cdr['answer_time'] = None

    try:
        cdr['end_time'] = epoch_to_dt(json_cdr['variables']['end_epoch'])
    except (KeyError, ValueError):
        cdr['end_time'] = None

    try:
        cdr['cnam'] = str_to_bool(json_cdr['variables']['cnam'])
    except KeyError:
        cdr['cnam'] = False

    try:
        cdr['fax'] = str_to_bool(json_cdr['variables']['fax'])
    except KeyError:
        cdr['fax'] = False

    try:
        cdr['duration'] = int(json_cdr['variables']['duration'])
    except KeyError:
        cdr['duration'] = 0

    try:
        cdr['billsec'] = int(json_cdr['variables']['billsec'])
    except KeyError:
        cdr['billsec'] = 0

    try:
        progressmsec = int(json_cdr['variables']['progressmsec'])
    except KeyError:
        progressmsec = 0

    try:
        progress_mediamsec = int(json_cdr['variables']['progress_mediamsec'])
    except KeyError:
        progress_mediamsec = 0

    cdr['pdd_msec'] = progressmsec + progress_mediamsec

    try:
        cdr['caller_name'] = str(json_cdr['variables']\
                                 ['effective_caller_id_name']).upper()
    except KeyError:
        cdr['caller_name'] = str(json_cdr['callflow']['caller_profile']\
                                 ['caller_id_name']).upper()

    try:
        cdr['caller_number'] = json_cdr['variables']\
                                       ['effective_caller_id_number']
    except KeyError:
        cdr['caller_number'] = json_cdr['callflow']['caller_profile']\
                                       ['caller_id_number']

    try:
        cdr['caller_ip'] = json_cdr['variables']['caller_ip']
    except KeyError:
        cdr['caller_ip'] = None

    try:
        cdr['caller_sdp'] = json_cdr['variables']['switch_r_sdp']
    except KeyError:
        cdr['caller_sdp'] = None

    try:
        cdr['callee_number'] = json_cdr['callflow']['caller_profile']\
                                       ['destination_number']
    except KeyError:
        cdr['callee_number'] = None

    try:
        cdr['callee_sdp'] = json_cdr['variables']['switch_m_sdp']
    except KeyError:
        cdr['callee_sdp'] = None

    try:
        cdr['sip_local_network_addr'] = json_cdr['variables']\
                                                ['sip_local_network_addr']
    except KeyError:
        cdr['sip_local_network_addr'] = None

    try:
        cdr['sip_call_id'] = json_cdr['variables']['sip_call_id']
    except KeyError:
        cdr['sip_call_id'] = None

    try:
        cdr['sip_from_uri'] = json_cdr['variables']['sip_from_uri']
    except KeyError:
        cdr['sip_from_uri'] = None

    try:
        cdr['sip_to_uri'] = json_cdr['variables']['sip_to_uri']
    except KeyError:
        cdr['sip_to_uri'] = None

    try:
        cdr['sip_req_uri'] = json_cdr['variables']['sip_req_uri']
    except KeyError:
        cdr['sip_req_uri'] = None

    try:
        cdr['sip_contact_uri'] = json_cdr['variables']['sip_contact_uri']
    except KeyError:
        cdr['sip_user_agent'] = None

    try:
        cdr['sip_user_agent'] = json_cdr['variables']['sip_user_agent']
    except KeyError:
        cdr['sip_user_agent'] = None

    try:
        cdr['sip_term_status'] = json_cdr['variables']['sip_term_status']
    except KeyError:
        cdr['sip_term_status'] = None

    try:
        cdr['hangup_cause'] = json_cdr['variables']['hangup_cause']
    except KeyError:
        cdr['hangup_cause'] = None

    try:
        cdr['originate_disposition'] = json_cdr['variables']\
                                               ['originate_disposition']
    except KeyError:
        cdr['originate_disposition'] = None

    return cdr
