import re
import urllib2
import xml.etree.cElementTree as etree

from datetime import datetime
from django.utils.http import urlencode
from django.utils.timezone import utc
from pymongo import Connection

from rating.models import WholesaleOBRate


connection = Connection('127.0.0.1')
db = connection.sipswitch              # MongoDB database to use


def xml_loc(npa, nxx):
    """
    Queries an Internet database to lookup NANPA location data
    Returns a dictionary with the results
    """

    API_URL = "http://www.localcallingguide.com/xmlprefix.php?npa=%s&nxx=%s" % (npa, nxx)

    data = None

    try:
        tree = etree.parse(urllib2.urlopen(API_URL))
        data = tree.getroot().find('prefixdata')
    except urllib2.URLError:
        return

    if data is None:
        return

    data_dict = {}
    for element in data:
        k = element.tag
        v = element.text
        if v == '\n':
            v = None

        if v:
            data_dict[k] = v

    if 'effdate' in data_dict:
        effdate = datetime.strptime(data_dict['effdate'], '%Y-%m-%d %H:%M:%S')
        data_dict['effdate'] = effdate.replace(tzinfo=utc)

    if 'discdate' in data_dict:
        discdate = datetime.strptime(data_dict['discdate'], '%Y-%m-%d %H:%M:%S')
        data_dict['discdate'] = discdate.replace(tzinfo=utc)

    if 'udate' in data_dict:
        udate = datetime.strptime(data_dict['udate'], '%Y-%m-%d %H:%M:%S')
        data_dict['udate'] = udate.replace(tzinfo=utc)

    if 'rc' in data_dict:
        data_dict['tstamp'] = datetime.utcnow().replace(tzinfo=utc)
        return data_dict

def nanpa_loc(npa, nxx):
    """
    Returns a dictionary of location data for a NPA,NXX
    """

    # Try the local database first
    col = db.nanpadata                  # collection for location data

    loc_data = col.find_one({"npa": npa, "nxx": nxx})

    if loc_data is None:
        loc_data = xml_loc(npa, nxx)
        if loc_data is not None and 'rc' in loc_data:
            col.insert(loc_data)
            return loc_data
    else:
        return loc_data

def lookup_loc(number):
    """
    Returns a human readable location string for a telephone number
    """
    UNKNOWN = 'Unknown'
    TF_CALL = 'Toll Free Call'

    location = UNKNOWN

    if number.startswith('+'):
        number = number[1:]

    if number.startswith('011'):
        number = number[3:]

    nanpa = re.compile('^1([2-9]\d{2})([2-9]\d{2})\d{4}$')

    if not nanpa.match(number):
        if number[0] == '1':
            return location

        try:
            location = WholesaleOBRate.objects.get_longest_prefix_match(number).description
        except:
            pass

        return location

    npa = nanpa.match(number).group(1)
    nxx = nanpa.match(number).group(2)

    # Dont lookup Toll Free
    if npa == '800':
        location = TF_CALL
    elif npa == '855':
        location = TF_CALL
    elif npa == '866':
        location = TF_CALL
    elif npa == '877':
        location = TF_CALL
    elif npa == '888':
        location = TF_CALL
    else:
        loc_data = nanpa_loc(npa, nxx)
        try:
            location = "%s %s" % (loc_data['rc'], loc_data['region'])
        except (KeyError, TypeError):
            # If all else fails use the location from rate tables
            try:
                location = WholesaleOBRate.objects.get_longest_prefix_match(number).description
            except:
                pass

    return location
