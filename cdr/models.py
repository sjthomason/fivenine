import mongoengine
from django.contrib.auth.models import User

from portal.models import Endpoint

class Cdr(mongoengine.Document):
    id = mongoengine.ObjectIdField(db_field='_id')
    user_id = mongoengine.IntField()
    ep_id = mongoengine.StringField()
    context = mongoengine.StringField()
    uuid = mongoengine.StringField()
    bleg_uuid = mongoengine.StringField()
    call_direction = mongoengine.StringField()
    start_time = mongoengine.DateTimeField()
    answer_time = mongoengine.DateTimeField()
    end_time = mongoengine.DateTimeField()
    duration = mongoengine.IntField()
    cnam = mongoengine.BooleanField()
    fax = mongoengine.BooleanField()
    billsec = mongoengine.IntField()
    billedsec = mongoengine.IntField()
    billed_tstamp = mongoengine.DateTimeField()
    rate = mongoengine.DecimalField()
    rate_description = mongoengine.StringField()
    rate_tstamp = mongoengine.DateTimeField()
    charge = mongoengine.DecimalField()
    pdd_msec = mongoengine.IntField()
    caller_name = mongoengine.StringField()
    caller_number = mongoengine.StringField()
    caller_ip = mongoengine.StringField()
    caller_location = mongoengine.StringField()
    caller_sdp = mongoengine.StringField()
    callee_number = mongoengine.StringField()
    callee_location = mongoengine.StringField()
    callee_sdp = mongoengine.StringField()
    sip_local_network_addr = mongoengine.StringField()
    sip_call_id = mongoengine.StringField()
    sip_from_uri = mongoengine.StringField()
    sip_to_uri = mongoengine.StringField()
    sip_req_uri = mongoengine.StringField()
    sip_contact_uri = mongoengine.StringField()
    sip_user_agent = mongoengine.StringField()
    sip_term_status = mongoengine.StringField()
    hangup_cause = mongoengine.StringField()
    originate_disposition = mongoengine.StringField()

    def get_user(self):
        if self.user_id is not None:
            return User.objects.get(id=self.user_id)

    def get_endpoint(self):
        if self.ep_id is not None:
            return Endpoint.objects.get(ep_id=self.ep_id)

    meta = {
        'collection': 'cdr_data',
    }
