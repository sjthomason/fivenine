from django.conf.urls.defaults import patterns, include, url

from views import CdrDetailView, CdrListView

urlpatterns = patterns('',
    url(r'^$', CdrListView.as_view()),
    url(r'^(?P<pk>\w+)/$', CdrDetailView.as_view()),
)
