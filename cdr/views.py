from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.views.decorators.csrf import csrf_exempt
from mongoengine import ValidationError

from base_views.protected import ProtectedDetailView, ProtectedListView

from models import Cdr
from forms import CdrForm
from location import lookup_loc
from rating.rating import rate_call
from freeswitch_cdr import decode_fs_post

@csrf_exempt
def cdr_import(request):

    # Only accept POST requests, else pretend we're not here
    if request.method != 'POST':
        raise Http404

    client_ip = request.META.get('REMOTE_ADDR')
    if client_ip not in settings.SIP_PROXY_IPS:
        return HttpResponse('%s Not Authorized' % (client_ip), status=403)

    cdr = Cdr()

    fs_cdr = decode_fs_post(request.raw_post_data)
    fs_cdr['caller_location'] = lookup_loc(fs_cdr['caller_number'])
    fs_cdr['callee_location'] = lookup_loc(fs_cdr['callee_number'])

    for k in fs_cdr:
        setattr(cdr, k, fs_cdr[k])

    cdr = rate_call(cdr)

    cdr.save()
    return HttpResponse(status=201)


class CdrListView(ProtectedListView):

    context_object_name = "cdr_list"
    template_name = "cdr/cdr_list.html"
    paginate_by = 100
    success_url = "/accounts/cdrs"

    def get_success_url(self):
        if self.success_url:
            url = self.success_url
        else:
            raise ImproperlyConfigured(
                "No URL to redirect to. Provide a success_url.")
        return url

    def get(self, request, form=None, *args, **kwargs):
        cdr_list = Cdr.objects.filter(user_id=request.user.id).order_by('-start_time')
        cdr_ep_id = request.session.get('cdr_ep_id')
        cdr_direction = request.session.get('cdr_direction')

        if form is None:
            form = CdrForm(request.user, initial={'endpoint': cdr_ep_id,
                                                  'direction': cdr_direction})

        if cdr_ep_id and cdr_direction:
            self.object_list = cdr_list.filter(ep_id=cdr_ep_id,
                                               call_direction=cdr_direction)
        elif cdr_ep_id:
            self.object_list = cdr_list.filter(ep_id=cdr_ep_id)
        elif cdr_direction:
            self.object_list = cdr_list.filter(call_direction=cdr_direction)
        else:
            self.object_list = cdr_list

        context = self.get_context_data(object_list=self.object_list)
        context['form'] = form
        context['cdr_ep_id'] = cdr_ep_id
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        form = CdrForm(request.user, request.POST)
        if form.is_valid():
            cdr_ep_id = form.cleaned_data.get('endpoint')
            cdr_direction = form.cleaned_data.get('direction')

        for i in ('cdr_ep_id', 'cdr_direction'):
            if locals()[i]:
                request.session[i] = locals()[i]
            else:
                try:
                    del request.session[i]
                except KeyError:
                    pass

        return HttpResponseRedirect(self.get_success_url())


class CdrDetailView(ProtectedDetailView):

    model = Cdr
    template_name = "cdr/cdr_detail.html"
    context_object_name = "cdr"

    def get_object(self):
        pk = self.kwargs.get(self.pk_url_kwarg, None)
        if pk is not None:
            try:
                obj = self.model.objects.with_id(pk)
            except ValidationError:
                raise Http404

        if obj is None or self.request.user.id != obj.user_id:
            raise Http404

        return obj

    def get_template_names(self):
        return [self.template_name]
