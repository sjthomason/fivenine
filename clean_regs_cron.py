#!/usr/bin/env python
from django.core.management import setup_environ
import settings
setup_environ(settings)

from web.models import Registration


def main():
    Registration.objects.delete_expired_users()


if __name__ == "__main__":
    main()
