import urllib2

from django.http import HttpResponse
from django.views.generic import View
from datetime import datetime, timedelta
from redis import StrictRedis

from portal.views import UserProfile

API_KEY = '64f139d712fa147236d503a13390b61f'
DB_HOST = '127.0.0.1'
DB_PORT = 6379
DB = 0
EXP_DAYS = 180

def get_cnam(number):

    api_url = "http://cnam.bulkcnam.com/?id=%s&did=%s" % (API_KEY, number)
    return urllib2.urlopen(api_url).read().strip()

def get_cnam_cache(number):

    cnam = 'UNKNOWN'
    r = StrictRedis(host=DB_HOST, port=DB_PORT, db=DB)

    q = r.get(number)

    if q is not None:
        cnam = q
    else:
        cnam = get_cnam(number)
        if cnam and cnam.upper() != 'UNKNOWN':
            exp = datetime.now() + timedelta(days=EXP_DAYS)
            r.set(number, cnam)
            r.expireat(number, exp)

    return cnam

class CnamQueryView(View):

    number_url_kwarg = 'number'

    def get(self, request, *args, **kwargs):
        number = self.kwargs.get(self.number_url_kwarg, None)
        cnam = get_cnam_cache(number)
        return HttpResponse(cnam)


class CustLookupView(View):

    number_url_kwarg = 'number'

    def get(self, request, *args, **kwargs):
        phone_num = number = self.kwargs.get(self.number_url_kwarg, '')
        profile = None
        cust_name = 'FALSE'

        # try to find to user by a few permutations of phone number
        if number[0] == '1' and len(number) == 11:
            phone_num = '%s-%s-%s' % (number[1:4], number[4:7], number[7:11])

        profile_list = UserProfile.objects.filter(phone_num=phone_num)
        if profile_list:
            profile = profile_list[0]
        else:
            if number[0] == '1' and len(number) == 11:
                phone_num = number[1:11]
                profile_list = UserProfile.objects.filter(phone_num=phone_num)
                if profile_list:
                    profile = profile_list[0]

        if profile:
            if profile.company_name:
                cust_name = profile.company_name
            else:
                cust_name = profile.user.get_full_name()

        return HttpResponse(cust_name.upper())


query = CnamQueryView.as_view()
cust_lookup = CustLookupView.as_view()
