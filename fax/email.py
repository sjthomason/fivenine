from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template import loader

FROM_ADDR = 'no-reply@5ninesolutions.com'
FROM_NAME = '5Nine Solutions Fax Server'
FROM_EMAIL = '%s <%s>' % (FROM_NAME, FROM_ADDR)


def email_rxfax(rxfax,
                subject_template_name='fax/rxfax_subject.txt',
                email_template_name_html='fax/rxfax_email.html',
                email_template_name_txt='fax/rxfax_email.txt',
                from_email=FROM_EMAIL):
    """
    Generates a fax recieved email and attaches a pdf fax
    """
    recipients = rxfax.did.fax_email.split(',')

    if not recipients:
        return

    c = {
        'remote_id': rxfax.remote_id,
        'pages': rxfax.num_pages,
        'caller_id': rxfax.caller_id,
        'received_stamp': rxfax.time_stamp,
        'did': rxfax.did
    }

    subject = loader.render_to_string(subject_template_name, c)
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    email_html = loader.render_to_string(email_template_name_html, c)
    email_txt = loader.render_to_string(email_template_name_txt, c)

    msg = EmailMultiAlternatives(subject, email_txt, from_email, recipients)

    bcc = getattr(settings, 'BILLING_BCC_EMAIL', None)
    debug = getattr(settings, 'DEBUG', False)
    if debug and bcc:
        msg.bcc = [bcc]

    msg.attach_alternative(email_html, "text/html")

    pdf = open(rxfax.file_path, 'rb').read()
    filename = rxfax.get_filename()

    msg.attach(filename, pdf, 'application/pdf')
    msg.send()

    return
