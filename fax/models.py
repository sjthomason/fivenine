from django.contrib.auth.models import User
from django.db import models

from utils.fts import SearchManager, VectorField
from utils.models import UUIDModel
from email import email_rxfax
from portal.models import Did

class RxFax(UUIDModel):
    """
    A received fax
    """
    search_index = VectorField()
    user = models.ForeignKey(User, blank=True, null=True)
    fax_did = models.ForeignKey(Did, editable=False)
    file_path = models.CharField(max_length=255, blank=True, null=True)
    time_stamp = models.DateTimeField('time recieved')
    remote_id = models.CharField(max_length=60, blank=True, null=True)
    caller_id = models.CharField(max_length=40, blank=True, null=True)
    num_pages = models.IntegerField(blank=True, null=True)
    ocr_text = models.TextField(blank=True, null=True, editable=False)

    objects = SearchManager(fields=('ocr_text','caller_id','remote_id'))

    class Meta:
        verbose_name = 'received fax'
        verbose_name_plural = 'received faxes'
        ordering = ['-time_stamp']

    def get_absolute_url(self):
        return "/fax/received/%s/" % self.pk

    def update_index(self):
        if hasattr(self, '_search_manager'):
            self._search_manager.update_index(pk=self.pk)

    def save(self, *args, **kwargs):
        super(RxFax, self).save(*args, **kwargs)
        self.update_index()

    def delete(self, *args, **kwargs):
        import os
        os.remove(self.file_path)
        super(RxFax, self).delete(*args, **kwargs)

    def __unicode__(self):
        if self.caller_id and self.num_pages:
            return "Fax from %s - %s Pages" % (self.caller_id, self.num_pages)
        else:
            return super(RxFax, self).__unicode__()

    def email_to_user(self):
        email_rxfax(self)

    def get_did(self):
        return self.fax_did

    def set_did(self, number):
        try:
            self.fax_did = Did.objects.get(number=number)
        except Did.DoesNotExist:
            pass

    def get_storage_filename(self, ext='pdf'):
        ext = ext.split('.')[-1]      # ext should not contain a period
        if self.pk:
            return "fax-%s.%s" % (self.pk, ext)

    def get_filename(self, ext='pdf'):
        ext = ext.split('.')[-1]      # ext should not contain a period
        if self.time_stamp:
            return 'fax-%s.%s' % (self.time_stamp.strftime('%m-%d-%Y-%H%M%S'),
                                  ext)

    def get_view_url(self):
        return self.get_absolute_url() + self.get_filename()

    def get_download_url(self):
        return self.get_absolute_url() + "dl/" + self.get_filename()

    did = property(get_did, set_did)
