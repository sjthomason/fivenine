from django.conf.urls import patterns, url

urlpatterns = patterns('fax.views',
    url(r'^received/$', 'rxfax_list_view'),
    url(r'^received/(?P<pk>[0-9a-f]+)/fax-([-0-9]+)\.pdf$', 'rxfax_pdf_view'),
    url(r'^received/(?P<pk>[0-9a-f]+)/dl/fax-([-0-9]+)\.pdf$',
            'rxfax_download_view'),
    url(r'^received/(?P<pk>[0-9a-f]+)/delete/$', 'rxfax_delete_view'),
    url(r'^received/search/$', 'rxfax_search_view')
    #url(r'^sent/$', 'txfax_list_view')
)
