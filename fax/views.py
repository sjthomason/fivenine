from django.conf import settings
from django.db.models import Q
from django.http import HttpResponse
from os.path import relpath

from base_views.protected import (ProtectedDetailView, ProtectedListView,
                                  ProtectedDeleteView)
from models import RxFax

class RxFaxDownloadView(ProtectedDetailView):

    download = False
    filename = None

    def render_to_response(self, context, **response_kwargs):
        response = HttpResponse(content_type='application/pdf')
        response['X-Accel-Redirect'] = settings.FS_MEDIA_URL + \
            relpath(self.object.file_path, settings.FS_MEDIA_ROOT)
        if self.filename:
            filename = self.filename
        else:
            filename = self.object.get_filename()

        if self.download:
            response['Content-Disposition'] = 'attachment; filename=%s' % filename
        else:
            response['Content-Disposition'] = 'filename=%s' % filename

        return response


class RxFaxSearchView(ProtectedListView):

    def get_queryset(self):
        if self.query is not None:
            qset = (
                Q(caller_id__icontains=self.query) |
                Q(remote_id__icontains=self.query) |
                Q(ocr_text__icontains=self.query)
            )
        if self.model is not None:
            # Try text search first
            queryset = self.model._default_manager.search(self.query,
                                                          rank_field='rank')
            if not queryset:
                queryset = self.model._default_manager.filter(qset)

        queryset = queryset.filter(user=self.request.user)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(RxFaxSearchView, self).get_context_data(**kwargs)
        context['query'] = self.query
        return context

    def get(self, request, **kwargs):
        self.query = request.GET.get('q', '')
        return super(RxFaxSearchView, self).get(request, **kwargs)

rxfax_list_view = ProtectedListView.as_view(
                        model=RxFax,
                        context_object_name='rxfax_list',
                        template_name='fax/rxfax_list.html',
                        paginate_by=100)

rxfax_delete_view = ProtectedDeleteView.as_view(
                        model=RxFax,
                        success_url='/fax/received/',
                        context_object_name='rxfax')
rxfax_pdf_view = RxFaxDownloadView.as_view(model=RxFax)
rxfax_download_view = RxFaxDownloadView.as_view(model=RxFax, download=True)
rxfax_search_view = RxFaxSearchView.as_view(
                        model=RxFax,
                        context_object_name='rxfax_list',
                        template_name='fax/rxfax_search.html',
                        paginate_by=100)
