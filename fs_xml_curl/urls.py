from django.conf.urls import patterns, url

urlpatterns = patterns('fs_xml_curl.views',
    url(r'^dialplan/$', 'dialplan'),
    url(r'^directory/$', 'directory')
)
