from fs_xml_curl.views.dialplan import dialplan
from fs_xml_curl.views.directory import directory

__all__ = ('dialplan', 'directory',)
