import xml.etree.cElementTree as etree

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt

from portal.models import Did


DP_ACTION_RXFAX = 'rxfax'
DP_ACTION_CHECK_VM = 'check-vm'
DP_ACTION_VM = 'vm'
DP_ACTION_CF_ON = 'forward-on'
DP_ACTION_CF_OFF = 'forward-off'
DP_ACTION_US_BUSY = 'busy-us'
DP_ACTION_LIMIT_MSG = 'limit-msg'


def _get_dp_base(context):
    """
    Adds the base extensions for the dialplan
    """

    ext = etree.SubElement(context, 'extension',
                {'name': "get_sip_acct", 'continue': "true"})
    cond = etree.SubElement(ext, 'condition',
                {'field': "${sip_h_P-Charge-Info}",
                 'expression': "<sip:(.*)@(.*)>;direction=(.*)"})
    etree.SubElement(cond, 'action',
                     {'application': "set",
                      'data': "presence_data=$1"})
    etree.SubElement(cond, 'action',
                     {'application': "set",
                      'data': "domain_name=$2"})
    etree.SubElement(cond, 'action',
                     {'application': "set",
                      'data': "call_direction=$3"})

    ext = etree.SubElement(context, 'extension',
                {'name': "get_user_id", 'continue': "true"})
    cond = etree.SubElement(ext, 'condition',
                {'field': "${sip_h_X-User-ID}",
                 'expression': "^(.*)$",
                 'break': "never"})
    etree.SubElement(cond, 'action',
                     {'application': "set",
                      'data': "user_id=$1"})
    etree.SubElement(cond, 'action',
                     {'application': "unset",
                      'data': "sip_h_X-User-ID"})

    ext = etree.SubElement(context, 'extension',
                {'name': "get_src_ip", 'continue': "true"})
    cond = etree.SubElement(ext, 'condition',
                {'field': "${sip_h_X-Originating-IP}",
                 'expression': "^(.*)$",
                 'break': "never"})
    etree.SubElement(cond, 'action',
                     {'application': "set",
                      'data': "caller_ip=$1"})
    etree.SubElement(cond, 'action',
                     {'application': "unset",
                      'data': "sip_h_X-Originating-IP"})
    return

def _get_ext_base(context, dest_num=""):
    """
    Returns a ElementTree SubElement object for a base FreeSWITCH extension
    """
    ext = etree.SubElement(context, 'extension', {'name': dest_num})
    # Must escape special chars in the destination number since its a
    # regular expression
    dest_num = dest_num.replace('+', '\+')
    cond = etree.SubElement(ext, 'condition',
                            {'field': 'destination_number',
                             'expression': '^' + dest_num + '$'})
    return cond

@csrf_exempt
def dialplan(request):
    """
    Generates a single entry XML dialplan for Freeswitch
    The destination number is expected to be normalied to
    '[action]+[data]'
    """

    #if request.method != 'POST':
    #    raise Http404
    document = etree.Element('document', {'type': 'freeswitch/xml'})
    section = etree.SubElement(document, 'section', {'name': 'dialplan'})


    document = etree.Element('document', {'type': 'freeswitch/xml'})
    section = etree.SubElement(document, 'section', {'name': 'dialplan'})

    dest_context = request.POST.get('Hunt-Context', 'default')
    dest_num = request.POST.get('Hunt-Destination-Number', 'unknown')

    context = etree.SubElement(section, 'context', {'name': dest_context})

    _get_dp_base(context)
    ext = _get_ext_base(context, dest_num=dest_num)

    action = dest_num.split('+')[0]
    try:
        data = dest_num.split('+')[1]
    except (IndexError):
        data = None

    # Get actions for destination number
    if action == DP_ACTION_RXFAX:
        did = Did.objects.filter(number=data)[0]
        try:
            tz = did.user.get_profile().timezone
        except (AttributeError, ObjectDoesNotExist):
            tz = settings.TIME_ZONE

        if did and did.enable_fax_srv:
            etree.SubElement(ext, 'action',
                    {'application': 'set', 'data': 'timezone=' + tz })
            etree.SubElement(ext, 'action',
                    {'application': 'set', 'data': 'fax_did=' + did.number })
            etree.SubElement(ext, 'action',
                    {'application': 'python', 'data': 'fax_receive'})
            etree.SubElement(ext, 'action',
                    {'application': 'set',
                     'data': 'sip_bye_h_X-FS-Fax-Result=${fax_result_text}'})
            etree.SubElement(ext, 'action', {'application': 'hangup'})

    elif action == DP_ACTION_CHECK_VM:
        etree.SubElement(ext, 'action',
            {'application': 'answer'})
        etree.SubElement(ext, 'action',
            {'application': 'sleep', 'data': '1000'})
        etree.SubElement(ext, 'action',
            {'application': 'voicemail',
             'data': 'check default ${domain_name} ' + data })

    elif action == DP_ACTION_VM:
        etree.SubElement(ext, 'action',
            {'application': 'answer'})
        etree.SubElement(ext, 'action',
            {'application': 'sleep', 'data': '1000'})
        etree.SubElement(ext, 'action',
            {'application': 'voicemail',
             'data': 'check default ${domain_name} ' + data })

    elif action == DP_ACTION_CF_ON:
        etree.SubElement(ext, 'action',
            {'application': 'answer'})
        etree.SubElement(ext, 'action',
            {'application': 'sleep', 'data': '1000'})
        etree.SubElement(ext, 'action',
            {'application': 'playback',
             'data': 'ivr/ivr-call_forwarding_has_been_set.wav'})
        etree.SubElement(ext, 'action', {'application': 'hangup'})

    elif action == DP_ACTION_CF_OFF:
        etree.SubElement(ext, 'action',
            {'application': 'answer'})
        etree.SubElement(ext, 'action',
            {'application': 'sleep', 'data': '1000'})
        etree.SubElement(ext, 'action',
            {'application': 'playback',
             'data': 'ivr/ivr-call_forwarding_has_been_cancelled.wav'})
        etree.SubElement(ext, 'action', {'application': 'hangup'})

    elif action == DP_ACTION_US_BUSY:
        pass

    elif action == DP_ACTION_LIMIT_MSG:
        pass

    return HttpResponse(etree.tostring(document, 'UTF-8'),
                        content_type="application/xml")
