import xml.etree.cElementTree as etree

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt

from portal.models import Did

DIAL_STRING = '{sip_invite_domain=${dialed_domain},\
               presence_id=${dialed_user}@${dialed_domain}}\
               ${sofia_contact(${dialed_user}@${dialed_domain})}'

@csrf_exempt
def directory(request):
    """
    Generates and XML document for a FreeSWITCH user directory entry
    """

    #if request.method != 'POST':
    #    raise Http404

    did = Did.objects.filter(number=request.GET['user'])[0]

    document = etree.Element('document', {'type': 'freeswitch/xml'})
    section = etree.SubElement(document, 'section', {'name': 'directory'})
    domain = etree.SubElement(section, 'domain', {'name': '${domain_name}'})
    params = etree.SubElement(domain, 'params')
    etree.SubElement(params, 'param',
                     {'name': 'dial-string', 'value': DIAL_STRING })
    groups = etree.SubElement(domain, 'groups')
    group = etree.SubElement(groups, 'group', {'name': 'default'})
    users = etree.SubElement(group, 'users')

    if did:
        user = etree.SubElement(users, 'user', {'id': str(did.number)})

        ### params
        params = etree.SubElement(user, 'params')

        #value = endpoint.password
        #param = etree.SubElement(params, 'param',
        #            {'name': 'password', 'value': value})

        value = str(did.enable_vm).lower()
        etree.SubElement(params, 'param',
                         {'name': 'vm-enabled', 'value': value})

        value = str(did.vm_to_email).lower()
        etree.SubElement(params, 'param',
                         {'name': 'vm-email-all-messages', 'value': value})

        etree.SubElement(params, 'param',
                         {'name': 'vm-attach-file', 'value': "true"})

        value = str(did.vm_keep_after_email).lower()
        etree.SubElement(params, 'param',
                         {'name': 'vm-keep-local-after-email', 'value': value})

        value = str(did.vm_email).lower()
        etree.SubElement(params, 'param',
                         {'name': 'vm-mailto', 'value': value})

        etree.SubElement(params, 'param',
                         {'name': 'allow-empty-password', 'value': 'false'})

        # Strip leading 1 from NANPA dids
        if did.number[0] == '1':
            value = did.number[1:]
        else:
            value = did.number
        etree.SubElement(params, 'param',
                         {'name': 'vm-alternate-greet-id', 'value': value})

        try:
            value = did.user.get_profile().timezone
        except (AttributeError, ObjectDoesNotExist):
            value = settings.TIME_ZONE
        etree.SubElement(params, 'param', {'name': 'timezone', 'value': value})
        etree.SubElement(params, 'param',
                         {'name': 'vm-password', 'value': 'user-choose'})

        ### variables
        variables = etree.SubElement(user, 'variables')
        value = str(did.user.id)
        etree.SubElement(variables, 'variable',
                         {'name': 'accountcode', 'value': value})

    return HttpResponse(etree.tostring(document, 'UTF-8'),
                        content_type="application/xml")
