PROJECT_DIRECTORY = '/srv/wsgi/fivenine'    # No trailing slash
SETTINGS_FILENAME = 'settings.py'

TIFF2PDF = '/usr/bin/tiff2pdf'
TESSERACT = '/usr/bin/tesseract'

# Setup environment
import os, sys
from django.utils.importlib import import_module

def setup_environ(project_directory=PROJECT_DIRECTORY,
                  settings_filename=SETTINGS_FILENAME):

    project_name = os.path.basename(project_directory)

    # Strip filename suffix to get the module name.
    settings_name = os.path.splitext(settings_filename)[0]

    # If DJANGO_SETTINGS_MODULE is already set, use it.
    os.environ['DJANGO_SETTINGS_MODULE'] = os.environ.get(
        'DJANGO_SETTINGS_MODULE',
        '%s.%s' % (project_name, settings_name)
    )

    sys.path.insert(1, project_directory)
    sys.path.append(os.path.join(project_directory, os.pardir))
    import_module(project_name)
    sys.path.pop()

# ------------------- Start script here -------------------
setup_environ()
from datetime import datetime
from django.template.defaultfilters import pluralize
from django.utils import timezone
from freeswitch import consoleLog
from uuid import uuid4 as uuid

import subprocess

from fax.models import RxFax
from portal.templatetags.cdr_formatting import num2tel


# FreeSWITCH event handlers.  TODO: Implement hangup event handler and parse errors from spandsp.  

def tiff2pdf(tiff_file, pdf_file=None, caller_id=None,
             remote_id=None, pages=None, remove=False):

    if not pdf_file:
        pdf_file = os.path.splitext(tiff_file)[0] + '.pdf'

    cmd = '%s -c \"5Nine PBX\" ' % (TIFF2PDF)

    if caller_id:
        tmp = '-a \"Caller-ID: %s\" ' % (caller_id)
        cmd += tmp

    if remote_id:
        tmp = '-t \"Fax from %s\" ' % (remote_id)
        cmd += tmp
        tmp = '-s \"Fax received from %s' % (remote_id)
        if pages:
            tmp += ' - %s %s\" ' % (pages, "page" + pluralize(pages))
        else:
            tmp += '\" '
        cmd += tmp

    cmd += '-f -o %s %s' % (pdf_file, tiff_file)
    ret_code = subprocess.call(cmd, shell=True)
    if ret_code == 0:
        if remove:
            os.remove(tiff_file)
        return pdf_file

def perform_ocr(tiff_file, out_base=None, lang='eng'):

    ocr_text = None

    if not out_base:
        out_base = os.path.splitext(tiff_file)[0]

    out_file = out_base + '.txt'

    cmd = '%s %s %s -l %s' % (TESSERACT, tiff_file, out_base, lang)
    ret_code = subprocess.call(cmd, shell=True)
    if ret_code == 0:
        fp = open(out_file, 'rb')
        ocr_text = ''.join(fp.read().splitlines())
    os.remove(out_file)
    return ocr_text

def handler(session, args):
    # Get required variables
    base_dir = session.getVariable("base_dir")
    domain_name = session.getVariable("domain_name")
    caller_id_num = session.getVariable("caller_id_number")
    fax_did = session.getVariable("fax_did")
    tz = session.getVariable("timezone")

    try:
        timezone.activate(tz)
    except:
        pass

    storage_path = os.path.join(base_dir, "storage/fax/%s" % (fax_did))
    if not os.path.exists(storage_path):
        os.makedirs(storage_path, 0750)

    consoleLog("info", "Receiving fax from %s, DID: %s, uuid: %s\n" % (caller_id_num, fax_did, uuid))

    r = RxFax()
    r.id=uuid().hex

    pdf_file = r.get_storage_filename(ext="pdf")
    tiff_file = r.get_storage_filename(ext="tiff")
    pdf_file_path = os.path.join(storage_path, pdf_file)
    tiff_file_path = os.path.join(storage_path, tiff_file)

    # answer the phone, save the fax file in .tiff format.
    session.answer()
    session.execute("playback", "silence_stream://2000")
    session.execute("set", "fax_enable_t38_request=true")
    session.execute("set", "fax_enable_t38=true")
    session.execute("set", "fax_ident=%s" % (num2tel(fax_did)))
    session.execute("rxfax", tiff_file_path)

    # Get fax variables
    try:
        num_pages = int(session.getVariable("fax_document_transferred_pages"))
    except TypeError:
        num_pages = 0

    remote_id = session.getVariable("fax_remote_station_id")
    if remote_id is None:
        remote_id = "Unknown"

    remote_id = remote_id.strip()

    fax_result = session.getVariable("fax_result_text")
    fax_success = session.getVariable("fax_success")

    if fax_success != '1':
        consoleLog("ERR", "Fax Reception from %s to %s failed: %s\n" % (caller_id_num, fax_did, fax_result))

    # Even if the fax failed we might have got a few pages
    if not os.path.isfile(tiff_file_path):
        return

    # Processing
    r.ocr_text = perform_ocr(tiff_file_path)
    tiff2pdf(tiff_file_path, pdf_file_path, num2tel(caller_id_num),
             remote_id, pages=num_pages, remove=True)

    if not os.path.isfile(pdf_file_path):
        consoleLog("ERR", "Conversion to .pdf failed!\n")
        return

    consoleLog("info", "Fax converted to pdf, processing\n")

    r.did = fax_did
    r.user = r.did.user
    r.file_path = pdf_file_path
    r.time_stamp = datetime.utcnow().replace(tzinfo=timezone.utc)
    r.remote_id = remote_id
    r.caller_id = caller_id_num
    r.num_pages = num_pages
    r.save()
    r.email_to_user()
    return
