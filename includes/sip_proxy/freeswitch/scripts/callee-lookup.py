from redis import StrictRedis

from freeswitch import consoleLog


DB_HOST = '10.59.1.96'
DB_PORT = 6379
DB = 0

def handler(session,args):

    r = StrictRedis(host=DB_HOST, port=DB_PORT, db=DB)
    callee_num = session.getVariable("callee_num")

    try:
        q = r.get(callee_num)
    except:
        consoleLog("err", "Redis Connection Failed!\n")
        return

    if q is not None:
        consoleLog("info", "Using Callee-ID from cache\n")
        callee_name = q
    else:
        callee_name = '1-%s-%s-%s' % (callee_num[0:3],
                               callee_num[3:6],
                               callee_num[6:10])

    session.setVariable("effective_callee_id_name", str(callee_name))
    return

