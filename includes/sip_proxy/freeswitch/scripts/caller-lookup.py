from datetime import datetime, timedelta
from redis import StrictRedis

from freeswitch import API, consoleLog


DB_HOST = '10.59.1.96'
DB_PORT = 6379
DB = 0
EXP_DAYS = 180

def clidlookup(number):

    api = API()
    number = str(number)
    cmd = 'cidlookup 1%s' % (number)
    return api.executeString(cmd).strip()


def handler(session,args):

    r = StrictRedis(host=DB_HOST, port=DB_PORT, db=DB)
    caller_num = session.getVariable("caller_num")

    try:
        q = r.get(caller_num)
    except:
        consoleLog("err", "Redis Connection Failed!\n")
        return

    if q is not None:
        consoleLog("info", "Using Caller-ID from cache\n")
        caller_name = str(q)
    else:
        consoleLog("info", "Caller-ID not in cache, looking up\n")
        caller_name = clidlookup(caller_num)
        if caller_name and caller_name.upper() != 'UNKNOWN':
            exp = datetime.now() + timedelta(days=EXP_DAYS)
            r.set(caller_num, caller_name)
            r.expireat(caller_num, exp)

    session.setVariable("effective_caller_id_name", caller_name)
    return

