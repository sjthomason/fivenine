#!/usr/bin/env python
from django.core.management import setup_environ
import settings
setup_environ(settings)

from decimal import Decimal
from datetime import datetime, date, timedelta
from django.contrib.auth.models import User
from django.utils.timezone import utc

from billing.authorizenet import process_payment, CardDeclined, CardProcessingError
from billing.models import CreditCard, Invoice, InvoiceItem, Transaction
from billing.billing_utils import get_next_billing_date, months_between
from portal.models import CpanelAcct, Did, Endpoint, PbxAcct, OtherService


TODAY = date.today()
DATE_DUE = TODAY + timedelta(days=10)
NOW = datetime.utcnow().replace(tzinfo=utc)

def get_charges_for_qset(qset, itemize=False):
    """
    Returns a list of InvoiceItem objects for a given queryset

    If itemize is not set (the default behavior), items with like plans and
    identical start and end billing dates are grouped together
    """

    if not qset:
        return

    charge_list = []    # a list of InvoiceItem objects for the Invoice

    itemized_list = []  # a list of tuples containing a model and price
                        # that get billed individually

    summarized_list = [] # a list of models that get summarized

    for model in qset:
        if model.billing_next_invoice <= TODAY:

            # If the model has never been billed,
            # use the billing start date
            if not model.billing_last_renew:
                model.billing_last_renew = model.billing_start

            if not model.billing_term:
                model.billing_term = 1

            if isinstance(model, Endpoint):
                if not model.channels:
                    continue

            if isinstance(model, OtherService):
                if not model.qty:
                    continue

            if model.price_override == "":
                model.price_override = None

            # if we got a price override that has not expired, use that price
            if model.price_override is not None and model.price_override >= 0 \
                and model.price_override_exp_date and model.price_override_exp_date >= TODAY:
                itemized_list.append((model, model.price_override,))

            # price_override is set but expired, use plan price
            elif model.price_override_exp_date and model.price_override_exp_date < TODAY:
                if itemize:
                    itemized_list.append((model,
                                          model.billing_plan.monthly_price,))
                else:
                    summarized_list.append(model)

            # price_override is set without expiration,
            # assume indefinite expiration
            elif model.price_override is not None and model.price_override >= 0 \
                and not model.price_override_exp_date:
                itemized_list.append((model, model.price_override,))

            elif itemize:
                itemized_list.append((model,
                                      model.billing_plan.monthly_price,))
            else:
                summarized_list.append(model)


    if summarized_list:
        plan_qty_dict = {}  # a key, value mapping of billable time to plan and
                            # quantity

        for model in summarized_list:
            plan = model.billing_plan
            billed_time = (model.billing_last_renew,
                           model.billing_next_renew,)

            qty = 1
            if isinstance(model, Endpoint):
                qty = model.channels

            if isinstance(model, OtherService):
                qty = model.qty

            # Use billed_time as dictionary key so that we
            # can group items with the same billing start and end times
            # together
            if billed_time in plan_qty_dict:
                if plan in plan_qty_dict[billed_time]:
                    plan_qty_dict[billed_time][plan] += qty
                else:
                    plan_qty_dict[billed_time][plan] = qty
            else:
                plan_qty_dict[billed_time] = {plan:qty}

            # The model has been counted, update billing info
            model.billing_last_invoice = TODAY
            model.billing_last_renew = model.billing_next_renew
            model.save()


        # add the summarized items
        for billed_time in plan_qty_dict:
            for plan in plan_qty_dict[billed_time]:
                charge = InvoiceItem()
                charge.item_type = 'service'
                charge.name = plan.name
                charge.description = "%s (%s-%s)" % (plan.description,
                                                     billed_time[0].strftime('%x'),
                                                     billed_time[1].strftime('%x'))
                charge.quantity = plan_qty_dict[billed_time][plan]
                months_billed = Decimal(str(months_between(billed_time[0],
                                                           billed_time[1])))
                charge.rate = plan.monthly_price * months_billed
                charge_list.append(charge)


    # add the individual line items
    if itemized_list:
        for model, monthly_price in itemized_list:

            qty = 1
            if isinstance(model, Endpoint):
                qty = model.channels

            if isinstance(model, OtherService):
                qty = model.qty

            charge = InvoiceItem()
            plan = model.billing_plan
            charge.item_type = 'service'
            charge.name = plan.name
            if isinstance(model, Endpoint):
                charge.description = "%s - %s (%s-%s)" % (plan.description,
                                                          model.label,
                                                          model.billing_last_renew.strftime('%x'),
                                                          model.billing_next_renew.strftime('%x'))
            elif isinstance(model, Did):
                charge.description = "%s - %s (%s-%s)" % (plan.description,
                                                          model,
                                                          model.billing_last_renew.strftime('%x'),
                                                          model.billing_next_renew.strftime('%x'))

            else:
                charge.description = "%s (%s-%s)" % (plan.description,
                                                     model.billing_last_renew.strftime('%x'),
                                                     model.billing_next_renew.strftime('%x'))
            charge.quantity = qty

            months_billed = Decimal(str(months_between(model.billing_last_renew,
                                                       model.billing_next_renew)))

            charge.rate = monthly_price * months_billed
            charge_list.append(charge)

            # The model has been counted, update billing info
            model.billing_last_invoice = TODAY
            model.billing_last_renew = model.billing_next_renew
            model.save()

    return charge_list


def create_invoice_from_charges(user, charge_list):

    invoice = Invoice()
    invoice.user = user
    invoice.date_due = DATE_DUE
    invoice.save()

    for charge in charge_list:
        charge.invoice = invoice
        charge.save()

    return invoice


def autopay_invoices(user):

    # lookup the account balance first
    account_balance = Transaction.objects.get_balance(user=user)
    if account_balance >= 0:
        return

    try:
        stored_card = CreditCard.objects.get(user=user, autopay=True)
    except CreditCard.DoesNotExist:
        return

    if not stored_card:
        return

    if stored_card.declined_stamp and \
            stored_card.declined_stamp > NOW - timedelta(days=3):
        return

    invoice_list = Invoice.objects.filter(user=user, date_due__lte=TODAY)

    if not invoice_list:
        return

    invoice_amount = 0
    for invoice in invoice_list:
        if not invoice.is_paid():
            invoice_amount += invoice.amount

    # account balance will be negative if the user owes money
    payment_amount = min(invoice_amount, abs(account_balance))

    if payment_amount > 0:
        try:
            payment = process_payment(user, stored_card, payment_amount,
                                      recurring=True)
            return payment
        except CardDeclined, e:
            from billing.email import email_card_declined
            email_card_declined(user, error_msg=e)
            stored_card.declined_stamp = NOW
            stored_card.save()
            return
        except CardProcessingError:
            return


def send_overdue_notications(user):

    overdue_date = TODAY - timedelta(days=settings.OVERDUE_NOTIFICATION_DAYS)
    overdue_invoices = Invoice.objects.get_overdue()\
                           .filter(user=user,
                                   date_due__lte=overdue_date)

    if not overdue_invoices.count():
        return

    notification_list = []

    from billing.email import email_invoice
    for invoice in overdue_invoices:
        # send notifications every 5 days
        if invoice.overdue_sent_stamp and \
               invoice.overdue_sent_stamp > NOW - timedelta(days=5):
            continue

        email_invoice(invoice,
                      subject_template_name='billing/overdue_subject.txt',
                      email_template_name_html='billing/overdue_email.html',
                      email_template_name_txt='billing/overdue_email.txt')
        invoice.overdue_sent_stamp = NOW
        invoice.save()
        notification_list.append(invoice)

    if notification_list:
        return notification_list

def send_exp_cc_notications(user):

    exp_date = TODAY - timedelta(days=settings.CC_EXP_NOTIFICATION_DAYS)
    cc_list = CreditCard.objects.filter(user=user, autopay=True)

    if not cc_list:
        return

    notification_list = []

    for cc in cc_list:
        # send notifications every 5 days
        if cc.exp_notice_stamp and \
               cc.exp_notice_stamp > NOW - timedelta(days=5):
            continue

        # decrypting the credit card is relatively expensive
        # do this after the notification stamp is checked
        if cc.get_exp_date() > exp_date:
            continue

        from billing.email import email_card_expired
        email_card_expired(user, exp_date=cc.get_exp_date())
        cc.exp_notice_stamp = NOW
        cc.save()
        notification_list.append(cc)

    if notification_list:
        return notification_list


def main():

    user_list = User.objects.all()
    model_list = [Did, Endpoint, PbxAcct, CpanelAcct, OtherService] # A list of models we are billing
    print "INVOICE Generation Run"
    print ""
    for user in user_list:
        print "--------------------------------------------------------------"
        print "User: %s" % user.username
        charges = []

        for model in model_list:
            qset = model._default_manager.filter(user=user, billing_start__lte=TODAY)
            itemize = False
            if model == Endpoint:
                itemize = True
            qset_charges = get_charges_for_qset(qset, itemize)
            if qset_charges is not None:
                charges += qset_charges

        if charges:
            invoice = create_invoice_from_charges(user, charges)
            if invoice is not None:
                invoice.email_to_user()
                print "Invoice %s created - Total: $%s" % (invoice.number,
                                                           invoice.amount)

        # process autopayments
        payment = autopay_invoices(user)
        if payment is not None:
            payment.email_to_user()
            print "Payment %s created - Method: %s  Total: $%s" % (payment.number,
                                                                   payment.get_method_display(),
                                                                   payment.amount)

        # send overdue notifications
        overdue_invoices = send_overdue_notications(user)
        if overdue_invoices is not None:
            print "Overdue Notification Sent - Invoice %s" %\
                (', '.join([str(i.pk) for i in overdue_invoices]))

        # send expired credit card notifications
        exp_ccs = send_exp_cc_notications(user)
        if exp_ccs is not None:
            print "Expired Credit Card Notification Sent - Credit Card %s" %\
                (', '.join([str(i) for i in exp_ccs]))


if __name__ == "__main__":
    main()
