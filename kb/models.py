from django.db import models
from django.contrib.auth.models import User

from utils.models import AutoSlugField


class KbCategory(models.Model):
    added = models.DateTimeField(auto_now_add=True)
    lastchanged = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    slug = AutoSlugField(unique=True, populate_from='title')

    class Meta:
        ordering = ['title']
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return "/kb/category/%s/" % self.slug


class KbArticle(models.Model):
    added = models.DateTimeField(auto_now_add=True)
    lastchanged = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, blank=True, null=True)
    kbcategory = models.ManyToManyField(KbCategory)
    title = models.CharField(max_length=255)
    body = models.TextField(blank=True, null=True)
    slug = AutoSlugField(unique=True, populate_from='title')

    class Meta:
        ordering = ['title']

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return "/kb/article/%s/" % self.slug


class KbImage(models.Model):
    description = models.CharField(max_length=255, blank=True, null=True)
    image_height = models.PositiveIntegerField()
    image_width = models.PositiveIntegerField()
    image = models.ImageField(upload_to='kb',
                              height_field='image_height',
                              width_field='image_width')
    kbarticle = models.ForeignKey(KbArticle)

    def __unicode__(self):
        return self.description
