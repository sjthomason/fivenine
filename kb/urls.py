from django.conf.urls import patterns, url

urlpatterns = patterns('kb.views',
    url(r'^$', 'kb_view'),
    url(r'^category/(?P<slug>[-\w]+)/$', 'kb_category_view'),
    url(r'^article/(?P<slug>[-\w]+)/$', 'kb_article_view'),
    url(r'^search/$', 'kb_search_view'),
)
