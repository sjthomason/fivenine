from django.db.models import Q
from django.views.generic import DetailView, ListView

from models import KbCategory, KbArticle


class KbDetailView(DetailView):

    def get_context_data(self, **kwargs):
        context = super(KbDetailView, self).get_context_data(**kwargs)
        context['category_list'] = KbCategory.objects.all()

        # if the model is a category, provide a list of orticles for this
        # KbCategory
        if isinstance(self.object, KbCategory):
            context['article_list'] = KbArticle.objects.filter(
                                                kbcategory=self.object)
        return context


class KbSearchView(ListView):

    def get_queryset(self):
        if self.query is not None:
            qset = (
                Q(title__icontains=self.query) |
                Q(body__icontains=self.query)
            )
        if self.model is not None:
            queryset = self.model._default_manager.filter(qset)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(KbSearchView, self).get_context_data(**kwargs)
        context['category_list'] = KbCategory.objects.all()
        context['query'] = self.query
        return context

    def get(self, request, **kwargs):
        self.query = request.GET.get('q', '')
        return super(KbSearchView, self).get(request, **kwargs)


kb_view = ListView.as_view(
                model=KbCategory,
                context_object_name='category_list',
                template_name='kb/kb_list.html')

kb_article_view = KbDetailView.as_view(
                model=KbArticle,
                context_object_name='selected_article',
                template_name='kb/kb_detail.html')

kb_category_view = KbDetailView.as_view(
                model=KbCategory,
                context_object_name='selected_category',
                template_name='kb/kb_detail.html')

kb_search_view = KbSearchView.as_view(
                model=KbArticle,
                context_object_name='article_list',
                template_name='kb/kb_search.html',
                paginate_by=20)

