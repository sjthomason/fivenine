#!/usr/bin/env python
from datetime import datetime, timedelta
from pymongo import Connection
from redis import StrictRedis

DB_HOST = '10.59.1.96'
DB_NAME = 'sipswitch'
COLLECTION = 'cnam_cache'

connection = Connection(DB_HOST)
db = connection[DB_NAME]
col = db[COLLECTION]

r = StrictRedis(host='127.0.0.1', db=0)

q = col.find()
for i in q:
    name = i['name']
    number = i['number']
    exp = i['created'] + timedelta(days=180)
    if exp >= datetime.now():
        r.set(number, name)
        r.expireat(number, exp)

