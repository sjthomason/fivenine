from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.contrib.localflavor.us.forms import USZipCodeField
from django.contrib.sites.models import get_current_site
from django.utils.http import int_to_base36
from django.template import loader

from utils.timezones import get_country_tz_choices, PRETTY_TIMEZONE_CHOICES
from models import (CpanelAcct, Did, Endpoint, PbxAcct,
                    ServiceAddress, UserAuthIP, UserProfile)


class AuthIPForm(forms.ModelForm):

    def __init__(self, user, *args, **kwargs):
        super(AuthIPForm, self).__init__(*args, **kwargs)
        self.fields['endpoint'].queryset = Endpoint.objects.filter(user=user)

    class Meta:
        model = UserAuthIP
        fields = ("src_ip", "endpoint",)


class CpanelAcctForm(forms.ModelForm):

    server_username = forms.RegexField(max_length=8, regex=r'^[a-z][a-z0-9]+$')
    server_password = forms.CharField(max_length=16, required=False,
        help_text="Leave blank to automatically create.")

    def clean_server_password(self):
        password = self.cleaned_data['server_password']
        if not password:
            password = User.objects.make_random_password(length=12)
        return password

    class Meta:
        model = CpanelAcct
        fields = ('domain', 'server_username', 'server_password',)


class DidForm(forms.ModelForm):

    def __init__(self, user, *args, **kwargs):
        super(DidForm, self).__init__(*args, **kwargs)
        self.fields['endpoint'].queryset = Endpoint.objects.filter(user=user)
        self.fields['svc_addr'].queryset = ServiceAddress.objects.filter(user=user)

    class Meta:
        model = Did
        fields = ('endpoint', 'route', 'fail_route', 'description',
                  'enable_cnam', 'enable_vm', 'vm_to_email',
                  'vm_keep_after_email', 'vm_email', 'enable_fax_srv',
                  'vm_ring_msec', 'fax_email', 'svc_addr',)


class EmailChangeForm(forms.Form):
    error_messages = {
        'inuse': "That e-mail address is already in use."
    }
    email = forms.EmailField(label="E-mail", max_length=75,
                help_text="We will send a confirmation link to the new address.")

    def clean_email(self):
        """
        Validates that the email address give is not already in use.
        """
        email = self.cleaned_data['email']
        if email != self.initial['email']:
            user_list = User.objects.filter(email__iexact=email)
            if len(user_list):
                raise forms.ValidationError(self.error_messages['inuse'])
        return email

    def save(self, domain_override=None,
             subject_template_name='registration/email_change_subject.txt',
             email_template_name='registration/email_change_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None):
        """
        Generates a one-use only link for changing email address and sends
        to the user.
        Returns the email address where the confirmation link was sent or None
        if no link was sent
        """
        user = request.user
        new_email = self.cleaned_data['email']
        if new_email == self.initial['email']:
            return

        from django.core.mail import send_mail
        from utils.encryption import encode
        if not domain_override:
            current_site = get_current_site(request)
            site_name = current_site.name
            domain = current_site.domain
        else:
            site_name = domain = domain_override
        c = {
            'email': user.email,
            'domain': domain,
            'site_name': site_name,
            'uid': int_to_base36(user.id),
            'user': user,
            'token': token_generator.make_token(user),
            'protocol': use_https and 'https' or 'http',
            'new_email': encode(new_email),
        }
        subject = loader.render_to_string(subject_template_name, c)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        email = loader.render_to_string(email_template_name, c)
        send_mail(subject, email, from_email, [new_email])
        return new_email

class EndpointForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EndpointForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['reset_password'] = forms.BooleanField(required=False)

    class Meta:
        model = Endpoint
        fields = ("default_clid", "default_route", "label", "channels",)


class PbxAcctForm(forms.ModelForm):

    class Meta:
        model = PbxAcct
        fields = ('domain', 'server_username', 'server_password',)


class ServiceAddressForm(forms.ModelForm):
    zip = USZipCodeField()

    class Meta:
        model = ServiceAddress
        exclude = ("user",)


class TimezoneForm(forms.Form):

    timezone = forms.ChoiceField(choices=PRETTY_TIMEZONE_CHOICES)

    def __init__(self, country=None, *args, **kwargs):
        super(TimezoneForm, self).__init__(*args, **kwargs)
        if country:
            self.fields['timezone'].choices = get_country_tz_choices(country)


class UserProfileForm(forms.ModelForm):
    error_messages = {
        'inuse': "That e-mail address is already in use."
    }

    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)

    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder = [
            'first_name',
            'last_name',
            'phone_num',
            'billing_email',
            'company_name',
            'company_position',
            'company_state',
            'address_line1',
            'address_line2',
            'address_city',
            'address_state',
            'address_postal',
            'address_country' ]

    def clean_billing_email(self):
        """
        Validates that the email address given is not currently in use.
        """
        billing_email = self.cleaned_data['billing_email']

        if billing_email and billing_email != self.initial['billing_email']:
            user_list = User.objects.filter(email__iexact=billing_email)
            profile_list = UserProfile.objects.filter(
                                        billing_email__iexact=billing_email)
            if len(user_list) or len(profile_list):
                raise forms.ValidationError(self.error_messages['inuse'])
        return billing_email

    class Meta:
        model = UserProfile
        exclude = ("acct_type","timezone",)

