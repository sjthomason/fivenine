from django.db import models
from django.contrib.localflavor.us.models import USStateField
from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_in
from hashlib import md5

from billing.models import RecurringItemBase, SipChannelPlan, Transaction
from cdr.location import lookup_loc

from utils import cpanel
from utils.models import CountryField, MultipleEmailField
from utils.address import ADDRESS_TYPE_CHOICES
from utils.encryption import decrypt, encrypt
from utils.timezones import PRETTY_TIMEZONE_CHOICES

class UserProfile(models.Model):
    TYPE_CHOICES = (
        ('R', 'Retail'),
        ('W', 'Wholesale'),
        ('C', 'Carrier'),
    )

    user = models.OneToOneField(User, editable=False)
    acct_type = models.CharField(max_length=2, blank=True, null=True,
                                 choices=TYPE_CHOICES)
    phone_num = models.CharField("phone number", max_length=16, db_index=True)
    billing_email = models.EmailField(blank=True, null=True,
        help_text="Optional: a separate email for billing related messages")
    company_name = models.CharField(max_length=64, blank=True, null=True)
    company_position = models.CharField(max_length=64, blank=True, null=True)
    company_state = USStateField(blank=True)
    address_line1 = models.CharField(max_length=128)
    address_line2 = models.CharField(max_length=128, blank=True)
    address_city = models.CharField("city", max_length=64)
    address_state = models.CharField("State/Province", max_length=128)
    address_postal = models.CharField("Zip/Postal Code", max_length=16)
    address_country = CountryField("country", default='US')
    timezone = models.CharField(max_length=32, choices=PRETTY_TIMEZONE_CHOICES,
                                default='America/Los_Angeles')

    class Meta:
        db_table = 'user_profile'

    def get_autopay(self):
        from billing.models import CreditCard
        if CreditCard.objects.filter(user=self.user, autopay=True):
            return True
        return False

    def get_acct_balance(self):
        return Transaction.objects.get_balance(user=self.user)

    acct_balance = property(get_acct_balance)
    autopay_enabled = property(get_autopay)


class LoginHistory(models.Model):
    user = models.ForeignKey(User, editable=False)
    ip_addr = models.GenericIPAddressField(editable=False)
    ip_country = CountryField(blank=True, null=True, editable=False)
    time_stamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-id']
        db_table = 'login_history'


def update_login_history(sender, request, user, **kwargs):
    if request:
        ip_addr = request.META.get('HTTP_X_FORWARDED_FOR', None) \
                    or request.META.get('REMOTE_ADDR')
        if ip_addr:
            import GeoIP
            gi = GeoIP.new(GeoIP.GEOIP_MEMORY_CACHE)
            ip_country = gi.country_code_by_addr(ip_addr)

        if ip_country:
            # save the country code in session for initial data on forms
            request.session['login_from_country'] = ip_country

        login_history = LoginHistory.objects.create(user=user,
                                                    ip_addr=ip_addr,
                                                    ip_country=ip_country)
        login_history.save()

user_logged_in.connect(update_login_history)


class CpanelServer(models.Model):
    name = models.CharField(max_length=64)
    ip_addr = models.GenericIPAddressField()
    is_active = models.BooleanField(default=True)
    default_ns1 = models.CharField(max_length=128)
    default_ns2 = models.CharField(max_length=128)
    admin_user = models.CharField(max_length=64)
    admin_key = models.TextField()

    def get_conn_status(self):
        try:
            if cpanel.version(self):
                return True
        except:
            return False

    conn_status = property(get_conn_status)

    def __unicode__(self):
        return "%s - %s" % (self.name, self.ip_addr)

    def __init__(self, *args, **kwargs):
        super(CpanelServer, self).__init__(*args, **kwargs)
        if self.admin_key is not None:
            self.admin_key = decrypt(self.admin_key)

    def save(self, *args, **kwargs):
        self.admin_key = encrypt(self.admin_key)
        super(CpanelServer, self).save(*args, **kwargs)

    class Meta:
        db_table = 'cpanel_servers'


class CpanelAcct(RecurringItemBase):

    domain = models.CharField(max_length=64)
    server = models.ForeignKey(CpanelServer)
    ip_addr = models.GenericIPAddressField(blank=True, null=True)
    server_username = models.CharField(max_length=64)
    server_password = models.CharField(max_length=255)
    billing_plan = models.ForeignKey('billing.CpanelPlan')

    _orig_server_password = None

    def set_server_password(self):
        cpanel.passwd(server=self.server,
                      user=self.server_username,
                      password=self.server_password)

    def create_server_acct(self):
        cpanel.createacct(server=self.server,
                          user=self.server_username,
                          password=self.server_password,
                          domain=self.domain,
                          plan='default')

    class Meta:
        db_table = 'cpanel_accts'
        ordering = ['domain']
        verbose_name = "hosting account"
        unique_together = (("server", "server_username"),)

    def __init__(self, *args, **kwargs):
        super(CpanelAcct, self).__init__(*args, **kwargs)
        if self.server_password:
            self.server_password = decrypt(self.server_password)
        self._orig_server_password = self.server_password

    def save(self, *args, **kwargs):
        if self.pk:
            if self.server_password != self._orig_server_password:
                self.set_server_password()
        else:
            self.create_server_acct()

        self.server_password = encrypt(self.server_password)
        super(CpanelAcct, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        cpanel.removeacct(self.server, self.server_username)
        super(CpanelAcct, self).delete(*args, **kwargs)

    def send_activation_email(self):
        pass

    def get_bandwidth_used(self):
        from django.core.cache import cache
        from utils.cache import key_from_args
        key = key_from_args('bandwidth', self.server.pk, self.server_username)
        bw = cache.get(key)
        if bw is not None:
            return bw
        bw = cpanel.show_bw(self.server, self.server_username)
        cache.set(key, bw, 600)
        return bw

    def get_admin_url(self):
        """
        Returns the URL of the admin login for the account
        """
        return "http://%s/cpanel/" % self.domain


class Domain(RecurringItemBase):
    """
    A registered domain name
    """

    sld = models.CharField("second level domain", max_length=128)
    tld = models.CharField("top level domain", max_length=10)
    expire_date = models.DateField("expiration date")
    auto_renew = models.BooleanField(default=True)
    registrar_id = models.CharField(max_length=64, editable=False)
    logged_ip = models.GenericIPAddressField(blank=True, null=True,
                                             editable=False)
    ns1 = models.CharField(max_length=60)
    ns2 = models.CharField(max_length=60)
    ns3 = models.CharField(max_length=60, blank=True, null=True)
    ns4 = models.CharField(max_length=60, blank=True, null=True)
    locked = models.BooleanField(default=True)

    reg_first_name = models.CharField("registrant first name", max_length=30)
    reg_last_name = models.CharField("registrant last name", max_length=30)
    reg_email = models.EmailField("registrant e-mail address")
    reg_addr_1 = models.CharField("registrant address line 1", max_length=60)
    reg_addr_2 = models.CharField("registrant address line 2", max_length=60)
    reg_city = models.CharField("registrant city", max_length=64)
    reg_state = USStateField("registrant state", blank=True)
    reg_postal_code = models.CharField('zip/postal code', max_length=16)
    reg_country = CountryField(default='US')
    reg_phone = models.CharField('phone number', max_length=16)
    reg_fax = models.CharField('fax number', max_length=16,
                               blank=True, null=True)
    reg_organization = models.CharField('organization', max_length=64,
                                    blank=True, null=True)
    reg_job_title = models.CharField('job title', max_length=64,
                                 blank=True, null=True)
    tech_first_name = models.CharField("tech first name", max_length=30,
                                       blank=True, null=True)
    tech_last_name = models.CharField("tech last name", max_length=30,
                                      blank=True, null=True)
    tech_email = models.EmailField("tech e-mail address",
                                   blank=True, null=True)
    tech_addr_1 = models.CharField("tech address line 1", max_length=60,
                                   blank=True, null=True)
    tech_addr_2 = models.CharField("tech address line 2", max_length=60,
                                   blank=True, null=True)
    tech_city = models.CharField("tech city", max_length=64,
                                 blank=True, null=True)
    tech_state = USStateField("tech state", blank=True, null=True)
    tech_postal_code = models.CharField('zip/postal code', max_length=16,
                                        blank=True, null=True)
    tech_country = CountryField(default='US', blank=True, null=True)
    tech_phone = models.CharField('phone number', max_length=16,
                                  blank=True, null=True)
    tech_fax = models.CharField('fax number', max_length=16,
                               blank=True, null=True)
    tech_organization = models.CharField('organization', max_length=64,
                                    blank=True, null=True)
    tech_job_title = models.CharField('job title', max_length=64,
                                 blank=True, null=True)
    admin_first_name = models.CharField("admin first name", max_length=30,
                                        blank=True, null=True)
    admin_last_name = models.CharField("admin last name", max_length=30,
                                       blank=True, null=True)
    admin_email = models.EmailField("admin e-mail address",
                                    blank=True, null=True)
    admin_addr_1 = models.CharField("admin address line 1", max_length=60,
                                    blank=True, null=True)
    admin_addr_2 = models.CharField("admin address line 2", max_length=60,
                                    blank=True, null=True)
    admin_city = models.CharField("admin city", max_length=64,
                                  blank=True, null=True)
    admin_state = USStateField("admin state", blank=True, null=True)
    admin_postal_code = models.CharField('zip/postal code', max_length=16,
                                         blank=True, null=True)
    admin_country = CountryField(default='US', blank=True, null=True)
    admin_phone = models.CharField('phone number', max_length=16,
                                   blank=True, null=True)
    admin_fax = models.CharField('fax number', max_length=16,
                               blank=True, null=True)
    admin_organization = models.CharField('organization', max_length=64,
                                    blank=True, null=True)
    admin_job_title = models.CharField('job title', max_length=64,
                                 blank=True, null=True)

    _md5_hash = models.TextField(db_column='md5_hash', editable=False,
                                 blank=True, null=True)

    # A list of fields to use when calculating the MD5 hash
    _hash_fields = ['admin_addr_1', 'admin_addr_2', 'admin_city',
                    'admin_country', 'admin_email', 'admin_fax',
                    'admin_first_name', 'admin_job_title', 'admin_last_name',
                    'admin_organization', 'admin_phone', 'admin_postal_code',
                    'admin_state', 'auto_renew', 'locked', 'logged_ip',
                    'ns1', 'ns2', 'ns3', 'ns4', 'reg_addr_1', 'reg_addr_2',
                    'reg_city', 'reg_country', 'reg_email', 'reg_fax',
                    'reg_first_name', 'reg_job_title', 'reg_last_name',
                    'reg_organization', 'reg_phone', 'reg_postal_code',
                    'reg_state', 'registrar_id', 'tech_addr_1', 'tech_addr_2',
                    'tech_city', 'tech_country', 'tech_email', 'tech_fax',
                    'tech_first_name', 'tech_job_title', 'tech_last_name',
                    'tech_organization', 'tech_phone', 'tech_postal_code',
                    'tech_state']

    class Meta:
        db_table = 'domains'
        ordering = ['sld']

    def __unicode__(self):
        return "%s.%s" % (self.sld, self.tld)

    def save(self, update_reg=True, *args, **kwargs):
        if self.pk:
            hash_data = ','.join('%s:%s' % (n, getattr(self, n)) for n in self._hash_fields)
            md5_hash = md5(hash_data).hexdigest()
            if not self._md5_hash:
                self._md5_hash = md5_hash
            elif self._md5_hash != md5_hash:
                self._md5_hash = md5_hash

            if update_reg:
                pass
                # TODO: Update Enom DB

        super(Domain, self).save(*args, **kwargs)

    def get_info_from_registrar(self):
        from utils.enom import get_domain_info
        domain_info = get_domain_info(sld=self.sld, tld=self.tld)
        self.registrar_id = domain_info['domain_id']
        self.expire_date = domain_info['expiration']
        self.ns1 = domain_info['ns1']
        self.ns2 = domain_info['ns2']
        self.ns3 = domain_info['ns3']
        self.ns4 = domain_info['ns4']
        self.save(update_reg=False)

class PbxAcct(RecurringItemBase):

    domain = models.CharField(max_length=64)
    server_username = models.CharField(max_length=64)
    server_password = models.CharField(max_length=255)
    billing_plan = models.ForeignKey('billing.PbxPlan')

    class Meta:
        db_table = 'pbx_accts'
        ordering = ['domain']
        verbose_name = 'PBX account'

    def __init__(self, *args, **kwargs):
        super(PbxAcct, self).__init__(*args, **kwargs)
        if self.server_password:
            self.server_password = decrypt(self.server_password)

    def save(self, *args, **kwargs):
        self.server_password = encrypt(self.server_password)
        super(PbxAcct, self).save(*args, **kwargs)


class Registration(models.Model):
    ruid = models.CharField(max_length=64, default='')
    username = models.CharField(max_length=64, default='')
    domain = models.CharField(max_length=64, blank=True, null=True)
    contact = models.CharField(max_length=255)
    received = models.CharField(max_length=128, blank=True, null=True)
    path = models.CharField(max_length=128, blank=True, null=True)
    expires = models.DateTimeField()
    q = models.FloatField()
    callid = models.CharField(max_length=255)
    cseq = models.IntegerField()
    last_modified = models.DateTimeField()
    flags = models.IntegerField()
    cflags = models.IntegerField()
    user_agent = models.CharField(max_length=255)
    socket = models.CharField(max_length=64, blank=True, null=True)
    methods = models.IntegerField(blank=True, null=True)
    instance = models.CharField(max_length=255, blank=True, null=True)
    reg_id = models.IntegerField()

    def get_absolute_url(self):
        return "/accounts/endpoint/%s/edit/" % self.username

    class Meta:
        db_table = 'registrations'


class Endpoint(RecurringItemBase):

    CH_CHOICES = tuple([(i, str(i)) for i in xrange(1, 25)])

    ep_id = models.CharField(primary_key=True, max_length=64,
                             db_index=True, editable=False)
    password = models.CharField(max_length=25, editable=False)
    default_clid = models.CharField('default caller ID', max_length=32,
                                     default='10000000000',
                                     help_text='The Caller ID that will be '\
                                               'used if one is not provided.')
    default_route = models.CharField(max_length=64, blank=True, null=True,
                                     help_text='A phone number, SIP URI, or '\
                                               'hostname to route to if no '\
                                               'registration is present.')
    label = models.CharField(max_length=64)
    channels = models.IntegerField("call channels", blank=True, null=True,
                                   choices=CH_CHOICES)
    billing_plan = models.ForeignKey('billing.SipChannelPlan')

    def gen_ep_id(self):
        acct_id = str(self.user.id)
        ep_list = self.user.endpoint_set.order_by('-ep_id')

        if ep_list:
            return str(int(ep_list[0].ep_id) + 1)

        return acct_id + '01'

    def get_registrations(self):
        from django.conf import settings
        from utils.kamailio import KamailioServer
        kam = KamailioServer(settings.SIP_REGISTRAR_IPS[0])
        return kam.ul_show_contact(self.ep_id)

    def __unicode__(self):
        return '%s - %s' % (self.ep_id, self.label)

    def save(self, *args, **kwargs):
        if not self.ep_id:
            self.ep_id = self.gen_ep_id()
        if not hasattr(self, 'billing_plan'):
            self.billing_plan = SipChannelPlan.objects.get(name='cc-1')
        super(Endpoint, self).save(args, **kwargs)

    def get_absolute_url(self):
        return "/accounts/endpoint/%s/edit/" % self.pk

    class Meta:
        db_table = 'endpoints'
        ordering = ['ep_id']


class ServiceAddress(models.Model):

    first_name = models.CharField('first name', max_length=64)
    last_name = models.CharField('last name', max_length=64)
    street_num = models.CharField('street number', max_length=8)
    street_name = models.CharField('street name', max_length=128)
    addr_type = models.CharField('address type', max_length=8,
                                 choices=ADDRESS_TYPE_CHOICES,
                                 blank=True, null=True)
    addr_type_num = models.CharField('address type number',
                                     max_length=8, blank=True, null=True)
    city = models.CharField('city', max_length=64)
    state = USStateField('state')
    zip = models.CharField('zip code', max_length=10)
    description = models.CharField(max_length=64)
    user = models.ForeignKey(User)

    def __unicode__(self):
        if self.addr_type and self.addr_type_num:
            return '%s %s, %s %s, %s, %s' % (self.street_num, self.street_name,
                                            self.addr_type.title(),
                                            self.addr_type_num,
                                            self.city, self.state)

        else:
            return '%s %s, %s, %s' % (self.street_num, self.street_name,
                                     self.city, self.state)

    class Meta:
        db_table = 'svc_addr'
        ordering = ['-id']


class Did(RecurringItemBase):

    # US Ring is 2000ms on / 4000ms off.  Subtract 1000ms to prevent overlap
    RING_CHOICES = tuple([((i * 6000) - 1000,
                           str(i) + ' rings') for i in xrange(2, 7)])

    number = models.CharField(primary_key=True, max_length=64)
    endpoint = models.ForeignKey(Endpoint, blank=True, null=True,
                                 on_delete=models.SET_NULL)
    route = models.CharField(max_length=64, blank=True, null=True,
                    help_text='A phone number, SIP URI, or host '\
                              'to route to.  If none is specified '\
                              'the Endpoint routing will be used.')
    fail_route = models.CharField(max_length=64, blank=True, null=True,
                    help_text='A secondary route to try if the '\
                              'primary route fails.')
    description = models.CharField(max_length=64, blank=True, null=True)
    rate_center = models.CharField(max_length=64, blank=True, null=True)
    force_pstn = models.BooleanField("Force PSTN",
                    help_text='Force outbound routing to PSTN',
                    default=False)
    enable_cnam = models.BooleanField("Enable CNAM",
                    help_text='Enable Caller Name for this number.')
    enable_vm = models.BooleanField("Enable voicemail", default=False)
    vm_to_email = models.BooleanField("Voicemail to e-mail",
                    help_text='Send voicemail messages as e-mail '\
                              'attachments.',
                    default=False)
    vm_keep_after_email = models.BooleanField("Keep after e-mail",
                    help_text='Keep voicemail message after sending e-mail.',
                    default=False)
    vm_email = MultipleEmailField("Voicemail E-mail",
                    help_text='E-mail address that will receive voicemails.',
                    blank=True, null=True)
    vm_ring_msec = models.IntegerField("Rings to voicemail",
                                       choices=RING_CHOICES, default=29000)
    enable_fax_srv = models.BooleanField("Enable Fax Server")
    fax_email = MultipleEmailField("Fax E-mail",
                    help_text='E-mail address that will receive faxes.',
                    blank=True, null=True)
    svc_addr = models.ForeignKey(ServiceAddress,
                                 verbose_name="Service Address",
                                 blank=True, null=True,
                                 on_delete=models.SET_NULL)
    provider = models.ForeignKey('billing.Provider', blank=True, null=True,
                                 on_delete=models.SET_NULL)
    billing_plan = models.ForeignKey('billing.DidPlan')

    def get_rate_center(self):
        return lookup_loc(self.number)

    def save(self, *args, **kwargs):
        from billing.models import DidPlan
        if not self.rate_center:
            self.rate_center = self.get_rate_center()
        try:
            self.billing_plan
        except DidPlan.DoesNotExist:
            import re
            tf = re.compile('^1(800|855|866|877|888)[2-9]\d{6}$')

            if tf.search(self.number):
                self.billing_plan = DidPlan.objects.get(name='usa-tf-did')
            else:
                self.billing_plan = DidPlan.objects.get(name='usa-did')

        super(Did, self).save(*args, **kwargs)

    def delete(self, using=None):
        self.endpoint = None
        self.route_to = None
        self.forward_to = None
        self.description = None
        self.enable_cnam = False
        self.svc_addr = None
        self.user = None
        self.save()

    def remove(self, *args, **kwargs):
        super(Did, self).delete(*args, **kwargs)

    def __unicode__(self):
        number = self.number
        if number[0] == '1':
            return '1-%s-%s-%s' % (number[1:4], number[4:7], number[7:11])
        else:
            return number

    def get_absolute_url(self):
        return "/accounts/dids/%s/" % self.pk

    class Meta:
        ordering = ['number']
        db_table = 'dids'


class OtherService(RecurringItemBase):

    description = models.CharField(max_length=128)
    billing_plan = models.ForeignKey('billing.OtherPlan')
    qty = models.PositiveIntegerField("quantity")

    def save(self, *args, **kwargs):
        if self.qty > 0:
            super(OtherService, self).save(*args, **kwargs)
        else:
            if self.pk:
                self.delete()

    class Meta:
        ordering = ['user', 'description']
        verbose_name = 'other services'
        verbose_name_plural = 'other services'


class UserAuthIP(models.Model):
    PROTO_CHOICES = (
        ('any', 'Any'),
        ('udp', 'UDP'),
        ('tcp', 'TCP'),
        ('tls', 'TLS'),
        ('sctp', 'SCTP'),
        ('none', 'None'),
    )

    src_ip = models.IPAddressField("IP address", unique=True, db_index=True)
    proto = models.CharField(max_length=4, choices=PROTO_CHOICES, default='any')
    from_pattern = models.CharField(max_length=64, blank=True, null=True)
    tag = models.CharField(max_length=64, blank=True, null=True)
    endpoint = models.ForeignKey(Endpoint)
    user = models.ForeignKey(User)

    def __unicode__(self):
        return self.src_ip

    def get_absolute_url(self):
        return "/accounts/authorized-ips/%s/" % self.pk

    class Meta:
        db_table = 'user_auth_ip'
        verbose_name = 'Authorized IP address'
        verbose_name_plural = 'Authorized IP addresses'
