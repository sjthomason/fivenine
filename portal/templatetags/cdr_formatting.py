import re

from django.template import Library
from django.utils.safestring import mark_safe

register = Library()

@register.filter
def num2tel(value):
    """
    Takes a number as an argument and returns it formatted
    as a telephone number.  i.e. (212) 555-1212
    The number MUST be formatted as an e.164 number
    """
    if not value:
        return value

    # Remove leading '+' from e.164 formatted numbers
    if value[0] == '+':
        value = value[1:]

    # Ensure all we have are digits left
    try:
        int(value)
    except (ValueError):
        return value

    if value[0] == '1':
        return '1-%s-%s-%s' % (value[1:4], value[4:7], value[7:11])

    elif value[0:2] == '44':
        uk_02 = re.compile('^44(2[03489])(\d{4})(\d{4})$')
        uk_mobile = re.compile('^44(7[4-9]\d{2})(\d{6})$')

        if uk_02.match(value):
            area_code = uk_02.match(value).group(1)
            local1 = uk_02.match(value).group(2)
            local2 = uk_02.match(value).group(3)
            return '+44 %s %s %s' % (area_code, local1, local2)

        elif uk_mobile.match(value):
            area_code = uk_mobile.match(value).group(1)
            local = uk_mobile.match(value).group(2)
            return '+44 %s %s' % (area_code, local)

    elif value[0:2] == '61':
        australia = re.compile('^61([1-8])(\d{4})(\d{4})$')

        if australia.match(value):
            area_code = australia.match(value).group(1)
            local1 = australia.match(value).group(2)
            local2 = australia.match(value).group(3)
            return '+61 %s %s %s' % (area_code, local1, local2)

    elif value[0:2] == '34':
        spain_land = re.compile('^34([89][1-8]\d)(\d{2})(\d{2})(\d{2})$')
        spain_mobile = re.compile('^34(6\d{2}|7[1-9]\d)(\d{3})(\d{3})$')

        if spain_land.match(value):
            area_code = spain_land.match(value).group(1)
            local1 = spain_land.match(value).group(2)
            local2 = spain_land.match(value).group(3)
            local3 = spain_land.match(value).group(4)
            return '+34 %s %s %s %s' % (area_code, local1, local2, local3)

        elif spain_mobile.match(value):
            area_code = spain_mobile.match(value).group(1)
            local1 = spain_mobile.match(value).group(2)
            local2 = spain_mobile.match(value).group(3)
            return '+34 %s %s %s' % (area_code, local1, local2)

    elif value[0:2] == '91':
        india_tier1 = re.compile('^91(11|20|22|33|40|44|79|80)(\d{8})$')

        if india_tier1.match(value):
            area_code = india_tier1.match(value).group(1)
            local = india_tier1.match(value).group(2)
            return '+91 %s %s' % (area_code, local)

    else:
        return '+%s' % (value)

@register.filter
def sec2hms(value):
    s = int(value)
    h = s / 3600
    s -= 3600*h
    m = s / 60
    s -= 60*m
    if h == 0:
        hms = '%d:%02d' % (m, s)
    else:
        hms = '%d:%d:%02d' % (h, m, s)
    return hms

@register.filter
def ms2sec(value):
    ms = float(value)
    sec = ms / 1000
    return sec

@register.filter
def crlf2html(value):
    s = str(value)
    return mark_safe(s.replace('\r\n', '<br />'))

