from django.template import Library
from django.utils.safestring import mark_safe

register = Library()

@register.filter
def as_currency(value):
    """
    formats a value as currency
    negative numbers are represented with parentheses
    """

    if not value:
        return value

    try:
        if value < 0:
            return mark_safe(u'<span class="red">($%s)</span>' % abs(value))
    except ValueError:
        pass

    return u'$%s' % value
