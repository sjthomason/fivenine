from django.conf import settings
from django.template import Library
from django.utils.safestring import mark_safe

register = Library()

@register.filter
def boolean_icon(value):
    BOOLEAN_MAPPING = {True: 'yes', False: 'no', None: 'unknown'}

    try:
        mapping = BOOLEAN_MAPPING[value]
    except KeyError:
        return

    return mark_safe(u'<img src="%simg/icon-%s.gif" alt="%s" />' %
                     (settings.STATIC_URL, mapping, mapping))
