from django.db.models.query import QuerySet
from django.db.models import Model
from django.template import Library

register = Library()

@register.filter
def name(value):
    if isinstance(value, QuerySet):
        return value.model._meta.verbose_name_plural.title()
    if isinstance(value, Model):
        return value._meta.verbose_name.title()
    return value

