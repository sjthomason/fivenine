import datetime
import simplejson as json

from django.contrib import messages
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.utils.http import base36_to_int
from django.views.generic import View, TemplateView
from django.utils.timezone import utc

from base_views.protected import (ProtectedCreateView, ProtectedUpdateView,
                                  ProtectedDeleteView, ProtectedFormView,
                                  ProtectedTemplateView, ProtectedListView)
from cdr.models import Cdr
from utils.cache import key_from_args

from forms import (AuthIPForm, CpanelAcctForm, DidForm, EmailChangeForm,
                   EndpointForm, PbxAcctForm, ServiceAddressForm, TimezoneForm,
                   UserProfileForm)
from models import (CpanelAcct, Did, Endpoint, LoginHistory, PbxAcct,
                    Registration, ServiceAddress, UserAuthIP, UserProfile)


class ActiveCallsView(ProtectedTemplateView):
    def get_context_data(self, *args, **kwargs):
        from utils.freeswitch import get_calls_for_user
        context = super(ActiveCallsView, self).get_context_data(*args, **kwargs)
        context['call_list'] = get_calls_for_user(self.request.user)
        return context


class UserProfileUpdateView(ProtectedFormView):
    form_class_1 = UserProfileForm
    form_class_2 = EmailChangeForm
    form_class_3 = PasswordChangeForm
    form_class_4 = TimezoneForm
    (initial_1, initial_2, initial_3, initial_4) = ({}, {}, {}, {})

    from_email = "no-reply@5ninesolutions.com"
    token_generator = default_token_generator

    def get_initial(self):
        initial_1 = self.initial_1.copy()
        initial_2 = self.initial_2.copy()
        initial_3 = self.initial_3.copy()
        initial_4 = self.initial_4.copy()

        if not self.profile.address_country:
            # Attempt to set default country from session
            country = self.request.session.get('login_from_country')
            if country:
                initial_1['address_country'] = country

        initial_1['first_name'] = self.user.first_name
        initial_1['last_name'] = self.user.last_name
        initial_2['email'] = self.user.email
        initial_4['timezone'] = self.profile.timezone
        return [initial_1 , initial_2, initial_3, initial_4]

    def get_form_class(self):
        return [self.form_class_1, self.form_class_2,
                self.form_class_3, self.form_class_4]

    def get_form(self, form_class_list):
        (form_1, form_2, form_3, form_4) = form_class_list
        (initial_1, initial_2, initial_3, initial_4) = self.get_initial()

        kwargs_1 = {'initial': initial_1, 'instance': self.profile}
        kwargs_2 = {'initial': initial_2}
        kwargs_3 = {'initial': initial_3, 'user': self.user}
        kwargs_4 = {'initial': initial_4,
                    'country': self.profile.address_country}

        if self.request.method in ('POST', 'PUT'):
            if 'contactinfo-submit' in self.request.POST:
                kwargs_1.update({
                    'data': self.request.POST,
                    'files': self.request.FILES,
                })
            elif 'changeemail-submit' in self.request.POST:
                kwargs_2.update({
                    'data': self.request.POST,
                    'files': self.request.FILES,
                })
            elif 'changepassword-submit' in self.request.POST:
                kwargs_3.update({
                    'data': self.request.POST,
                    'files': self.request.FILES,
                })
            elif 'timezone-submit' in self.request.POST:
                kwargs_4.update({
                    'data': self.request.POST,
                    'files': self.request.FILES,
                })

        return [form_1(**kwargs_1), form_2(**kwargs_2),
                form_3(**kwargs_3), form_4(**kwargs_4)]

    def form_valid(self, form):
        if isinstance(form, self.form_class_1):
            user = form.save().user
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            user.save()
            msg = "Your contact information has been successfully updated."
            messages.success(self.request, msg, fail_silently=True)
            return super(UserProfileUpdateView, self).form_valid(form)

        if isinstance(form, self.form_class_2):
            opts = {
                'use_https': self.request.is_secure(),
                'token_generator': self.token_generator,
                'from_email': self.from_email,
                'request': self.request,
            }
            new_email = form.save(**opts)
            if new_email is not None:
                msg = "A confirmation link has been sent to %s" % new_email
                messages.success(self.request, msg, fail_silently=True)
            return super(UserProfileUpdateView, self).form_valid(form)

        if isinstance(form, self.form_class_3):
            form.save()
            msg = "Your password has been successfully changed"
            messages.success(self.request, msg, fail_silently=True)
            return super(UserProfileUpdateView, self).form_valid(form)

        if isinstance(form, self.form_class_4):
            self.profile.timezone = form.cleaned_data['timezone']
            self.profile.save()
            self.request.session['timezone'] = form.cleaned_data['timezone']

            # purge timezone cache
            key = key_from_args('timezone', self.request.session.session_key)
            cache.delete(key)
            msg = "Your preferred display timezone has been changed to "\
                  "%s" % self.profile.timezone
            messages.success(self.request, msg, fail_silently=True)
            return super(UserProfileUpdateView, self).form_valid(form)

    def form_invalid(self, invalid_form):
        form_class_list = self.get_form_class()
        (form_1, form_2, form_3, form_4) = self.get_form(form_class_list)

        if isinstance(invalid_form, self.form_class_1):
            return self.render_to_response(
                self.get_context_data(pref_form=invalid_form,
                                      email_form=form_2,
                                      pass_form=form_3,
                                      timezone_form=form_4))

        if isinstance(invalid_form, self.form_class_2):
            return self.render_to_response(
                self.get_context_data(pref_form=form_1,
                                      email_form=invalid_form,
                                      pass_form=form_3,
                                      timezone_form=form_4))

        if isinstance(invalid_form, self.form_class_3):
            return self.render_to_response(
                self.get_context_data(pref_form=form_1,
                                      email_form=form_2,
                                      pass_form=invalid_form,
                                      timezone_form=form_4))

        if isinstance(invalid_form, self.form_class_4):
            return self.render_to_response(
                self.get_context_data(pref_form=form_1,
                                      email_form=form_2,
                                      pass_form=form_3,
                                      timezone_form=invalid_form))

    def get(self, request, *args, **kwargs):
        self.user = request.user
        try:
            self.profile = request.user.get_profile()
        except ObjectDoesNotExist:
            self.profile = UserProfile()
            self.profile.user = request.user

        form_class_list = self.get_form_class()
        (form_1, form_2, form_3, form_4) = self.get_form(form_class_list)
        return self.render_to_response(
                    self.get_context_data(pref_form=form_1,
                                          email_form=form_2,
                                          pass_form=form_3,
                                          timezone_form=form_4))

    def post(self, request, *args, **kwargs):
        self.user = request.user
        try:
            self.profile = request.user.get_profile()
        except ObjectDoesNotExist:
            self.profile = UserProfile()
            self.profile.user = request.user

        form_class_list = self.get_form_class()
        (form_1, form_2, form_3, form_4) = self.get_form(form_class_list)

        if 'contactinfo-submit' in request.POST:
            form = form_1
        elif 'changeemail-submit' in request.POST:
            form = form_2
        elif 'changepassword-submit' in request.POST:
            form = form_3
        elif 'timezone-submit' in request.POST:
            form = form_4

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class DashboardView(ProtectedTemplateView):

    def get_context_data(self, **kwargs):
        key = key_from_args('portal_voip_stats', self.request.user.id)
        voip_stats = cache.get(key)
        if not voip_stats:
            voip_stats = {}

            now = datetime.datetime.utcnow().replace(tzinfo=utc)
            half_day_ago = now - datetime.timedelta(hours=12)
            three_days_ago = now - datetime.timedelta(days=3)
            one_week_ago = now - datetime.timedelta(days=7)
            one_month_ago = now - datetime.timedelta(days=30)

            ep_list = Endpoint.objects.filter(user=self.request.user)
            if ep_list.count():
                # Calculate VoIP Stats
                reg_list = []
                for ep in ep_list:
                    reg_list += ep.get_registrations()

                cdr_list = Cdr.objects.filter(user_id=self.request.user.id)
                half_day_calls = cdr_list.clone().filter(
                                    start_time__gt=half_day_ago)
                three_day_calls = cdr_list.clone().filter(
                                    start_time__gt=three_days_ago)
                one_week_calls = cdr_list.clone().filter(
                                    start_time__gt=one_week_ago)
                one_month_calls = cdr_list.clone().filter(
                                    start_time__gt=one_month_ago)

                voip_stats['ep_count'] = ep_list.count()
                voip_stats['reg_count'] = len(reg_list)
                voip_stats['12h_calls'] = half_day_calls.count()
                voip_stats['12h_mins'] = int(half_day_calls.sum('billedsec')/60)
                voip_stats['3d_calls'] = three_day_calls.count()
                voip_stats['3d_mins'] = int(three_day_calls.sum('billedsec')/60)
                voip_stats['week_calls'] = one_week_calls.count()
                voip_stats['week_mins'] = int(one_week_calls.sum('billedsec')/60)
                voip_stats['month_calls'] = one_month_calls.count()
                voip_stats['month_mins'] = int(one_month_calls.sum('billedsec')/60)
            cache.set(key, voip_stats, 600)

        return {'voip_stats': voip_stats, 'params': kwargs}


class DidExportView(ProtectedListView):

    context_object_name = "did_list"
    template_name = "portal/didexport_list.txt"

    def render_to_response(self, context):
        from django.template import loader, Context

        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename=did-export.csv'
        t = loader.select_template(self.get_template_names())
        c = Context(context)
        response.write(t.render(c))
        return response


class DidJsonView(ProtectedListView):

    model = Did

    def render_to_response(self, context, **response_kwargs):

        area_codes = []
        prefixes = []

        if self.query is None:
            for obj in self.object_list:
                area_code = obj.number[1:4]
                if area_code not in area_codes:
                    area_codes.append(area_code)
        else:
            area_codes.append(self.query)
            for obj in self.object_list:
                prefix = obj.number[4:7]
                if prefix not in prefixes:
                    prefixes.append(prefix)

        response_kwargs['content_type'] = 'application/json'
        json_dict = {'area_codes': area_codes,
                     'prefixes': prefixes }
        return HttpResponse(json.dumps(json_dict))


    def get_queryset(self):
        if self.query is not None:
            queryset = self.model._default_manager.filter(user__isnull=True,
                number__startswith='1' + self.query)
        else:
            queryset = self.model._default_manager.filter(user__isnull=True)
        return queryset

    def get(self, request, **kwargs):
        self.query = request.GET.get('q', None)
        return super(DidJsonView, self).get(request, **kwargs)


class DidPrefixView(ProtectedListView):

    context_object_name = "did_list"
    template_name = "portal/did_prefix_list.html"

    def get_queryset(self):
        prefix = self.kwargs.get('prefix', None)
        return Did.objects.filter(user__isnull=True, number__startswith=prefix)


class DidSearchView(ProtectedListView):

    model = Did
    context_object_name = 'did_list'
    template_name = "portal/did_list.html"

    def get_queryset(self):
        from django.db.models import Q
        import re
        if self.query is not None:
            qset = (
                Q(number__icontains=re.sub('(-|\(|\)|\.)', '', self.query)) |
                Q(description__icontains=self.query) |
                Q(rate_center__icontains=self.query)
            )
        if self.model is not None:
            queryset = self.model._default_manager.filter(qset, user=self.request.user)
        return queryset

    def get(self, request, **kwargs):
        self.query = request.GET.get('q', '')
        return super(DidSearchView, self).get(request, **kwargs)


class DidUpdateView(ProtectedUpdateView):
    form_class = DidForm
    model = Did
    success_url = "/accounts/dids/"
    template_name = "portal/did_form.html"

    def get_form(self, form_class):
        return form_class(self.request.user, **self.get_form_kwargs())


class EmailChangeConfirmView(TemplateView):

    token_generator = default_token_generator

    def get_context_data(self, *args, **kwargs):
        context = super(EmailChangeConfirmView, self).get_context_data(*args, **kwargs)
        uidb36 = self.kwargs.get('uidb36', None)
        emailb64 = self.kwargs.get('emailb64', None)
        token = self.kwargs.get('token', None)

        token_generator = self.token_generator
        validlink = False

        assert uidb36 is not None and emailb64 is not None \
            and token is not None # checked by URLconf

        try:
            uid_int = base36_to_int(uidb36)
            user = User.objects.get(id=uid_int)
        except (ValueError, User.DoesNotExist):
            user = None

        if user is not None and token_generator.check_token(user, token):
            from utils.encryption import decode, ChecksumError
            try:
                new_email = decode(emailb64)
                user.email = new_email
                user.save()
                validlink = True
            except ChecksumError:
                pass

        context['validlink'] = validlink
        return context

class InteropView(ProtectedListView):

    model = UserAuthIP
    context_object_name = "auth_ip_list"
    template_name = "portal/interop.html"

    def get_context_data(self, *args, **kwargs):
        context = super(InteropView, self).get_context_data(*args, **kwargs)
        reg_list = []
        for ep in Endpoint.objects.filter(user=self.request.user):
            for reg in ep.get_registrations():
                reg['endpoint'] = ep
                reg_list.append(reg)

        context['reg_list'] = reg_list
        return context


class EndpointCreateView(ProtectedCreateView):
    form_class = EndpointForm
    model = Endpoint
    success_url = "/accounts/endpoint/"
    template_name = "portal/endpoint_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.password = User.objects.make_random_password(length=12)
        return super(EndpointCreateView, self).form_valid(form)


class EndpointUpdateView(ProtectedUpdateView):
    form_class = EndpointForm
    model = Endpoint
    success_url = "/accounts/endpoint/"
    template_name = "portal/endpoint_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if form.cleaned_data['reset_password']:
            self.object.password = User.objects.make_random_password(length=12)
        return super(EndpointUpdateView, self).form_valid(form)


class UserAuthIPCreateView(ProtectedCreateView):
    form_class = AuthIPForm
    model = UserAuthIP
    success_url = "/accounts/interconnection/"
    template_name = "portal/auth_ip_form.html"

    def get_form(self, form_class):
        return form_class(self.request.user, **self.get_form_kwargs())


class UserAuthIPUpdateView(ProtectedUpdateView):
    form_class = AuthIPForm
    model = UserAuthIP
    success_url = "/accounts/interconnection/"
    template_name = "portal/auth_ip_form.html"

    def get_form(self, form_class):
        return form_class(self.request.user, **self.get_form_kwargs())


active_calls_view = ProtectedTemplateView.as_view(template_name="portal/active_calls.html")
active_calls_list_view = ActiveCallsView.as_view(
                            template_name="portal/active_calls_list.html")
cpanelacct_create_view = ProtectedCreateView.as_view(
                            model=CpanelAcct,
                            template_name="portal/cpanelacct_form.html",
                            success_url="/accounts/web-hosting/",
                            form_class=CpanelAcctForm)
cpanelacct_delete_view = ProtectedDeleteView.as_view(
                            model=CpanelAcct,
                            success_url="/accounts/web-hosting/",
                            template_name="portal/confirm_delete.html")
cpanelacct_list_view = ProtectedListView.as_view(
                            model=CpanelAcct,
                            template_name="portal/cpanelacct_list.html")
cpanelacct_update_view = ProtectedUpdateView.as_view(
                            model=CpanelAcct,
                            success_url="/accounts/web-hosting/",
                            template_name="portal/cpanelacct_form.html",
                            form_class=CpanelAcctForm)

dashboard_view = DashboardView.as_view(template_name="portal/dashboard.html")

did_export_view = DidExportView.as_view(model=Did)
did_list_view = ProtectedListView.as_view(
                            model=Did,
                            template_name="portal/did_list.html",
                            paginate_by=20)
did_json_view = DidJsonView.as_view()
did_purchase_view = ProtectedTemplateView.as_view(
                            template_name="portal/did_purchase.html")
did_prefix_view = DidPrefixView.as_view()
did_search_view = DidSearchView.as_view(paginate_by=20)
did_update_view = DidUpdateView.as_view()

email_change_confirm = EmailChangeConfirmView.as_view(
                        template_name="registration/email_change_confirm.html")
endpoint_create_view = EndpointCreateView.as_view()
endpoint_list_view = ProtectedListView.as_view(
                            model=Endpoint,
                            template_name="portal/endpoint_list.html")
endpoint_update_view = EndpointUpdateView.as_view()

interop_view = InteropView.as_view()

loginhistory_list_view = ProtectedListView.as_view(model=LoginHistory)

pbxacct_create_view = ProtectedCreateView.as_view(
                            model=PbxAcct,
                            template_name="portal/pbxacct_form.html",
                            success_url="/accounts/pbx/",
                            form_class=PbxAcctForm)
pbxacct_delete_view = ProtectedDeleteView.as_view(
                            model=PbxAcct,
                            template_name="portal/confirm_delete.html",
                            success_url="/accounts/pbx/")
pbxacct_list_view = ProtectedListView.as_view(
                            model=PbxAcct,
                            template_name="portal/pbxacct_list.html")
pbxacct_update_view = ProtectedUpdateView.as_view(
                            model=PbxAcct,
                            template_name="portal/pbxacct_form.html",
                            success_url="/accounts/pbx/",
                            form_class=PbxAcctForm)

serviceaddress_create_view = ProtectedCreateView.as_view(
                            model=ServiceAddress,
                            template_name="portal/serviceaddress_form.html",
                            success_url="/accounts/service-location/",
                            form_class=ServiceAddressForm)
serviceaddress_delete_view = ProtectedDeleteView.as_view(
                            model=ServiceAddress,
                            template_name="portal/confirm_delete.html",
                            success_url="/accounts/service-location/")
serviceaddress_list_view = ProtectedListView.as_view(
                            model=ServiceAddress,
                            template_name = "portal/serviceaddress_list.html")
serviceaddress_update_view = ProtectedUpdateView.as_view(
                            model=ServiceAddress,
                            template_name="portal/serviceaddress_form.html",
                            success_url="/accounts/service-location/",
                            form_class=ServiceAddressForm)

userauthip_create_view = UserAuthIPCreateView.as_view()
userauthip_update_view = UserAuthIPUpdateView.as_view()
userauthip_delete_view = ProtectedDeleteView.as_view(
                            model=UserAuthIP,
                            template_name="portal/confirm_delete.html",
                            success_url="/accounts/interconnection/")

userprofile_update_view = UserProfileUpdateView.as_view(
                            success_url="/accounts/preferences/",
                            template_name="portal/profile_form.html")

