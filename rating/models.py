from django.core.cache import cache
from django.db import models
from django.db.models.manager import EmptyManager

from utils.cache import key_from_args

class NoRate(object):

    objects = EmptyManager()

    prefix = 0
    description = 'No Matching Rate Found'
    first_interval = 6
    sub_interval = 6
    rate = 0

    def __init__(self):
        pass

    def __unicode__(self):
        return 'NoRate'

    def __str__(self):
        return unicode(self).encode('utf-8')

    def __eq__(self, other):
        return isinstance(other, self.__class__)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return 1 # instances always return the same hash value

    def save(self):
        raise NotImplementedError

    def delete(self):
        raise NotImplementedError


class RateManager(models.Manager):

    def get_longest_prefix_match(self, value):
        """
        Returns a single object with the longest prefix matching the given value.
        """
        key = key_from_args(__file__, 'get_longest_prefix_match', value)
        match = cache.get(key)
        if match is not None:
            return match

        from django.db import connection
        cursor = connection.cursor()
        select = 'SELECT id FROM %s' % (self.model._meta.db_table)
        cursor.execute(select + ' WHERE %s LIKE prefix || \'%%\' ORDER BY length(prefix) DESC LIMIT 1', [value])
        try:
            id = cursor.fetchone()[0]
            match = self.get(id=id)
            cache.set(key, match, 3600)
            return match
        except TypeError:
            return NoRate()

    def longest_prefix_match(self, value):
        """
        Returns a queryset with the longest prefix matching the given value.
        """
        key = key_from_args(__file__, 'longest_prefix_match', value)
        pk_list = cache.get(key)
        if pk_list is None:
            from django.db import connection
            cursor = connection.cursor()
            select = 'SELECT id FROM %s' % (self.model._meta.db_table)
            cursor.execute(select + ' WHERE %s LIKE prefix || \'%%\' ORDER BY length(prefix) DESC LIMIT 1', [value])
            pk_list = [col[0] for col in cursor.fetchall()]
            cache.set(key, pk_list, 3600)
        return self.filter(pk__in=pk_list)


class RateBase(models.Model):

    objects = RateManager()

    prefix = models.CharField(max_length=16,db_index=True)
    description = models.CharField(max_length=255)
    first_interval = models.IntegerField()
    sub_interval = models.IntegerField()
    rate = models.DecimalField(max_digits=8, decimal_places=5)

    class Meta:
        abstract = True
        ordering = ['description', 'prefix']


class WholesaleIBRate(RateBase):
    pass


class WholesaleOBRate(RateBase):
    pass


class WholesaleTFRate(RateBase):
    pass

