import re
from datetime import datetime
from django.utils.timezone import utc

from models import *


CNAM_LOOKUP_CHARGE = 0.009

class OnNetRate(NoRate):
    description = 'On Network Call'


def round_billsec(billsec, first_interval, sub_interval):
    """
    Returns the billable seconds of a call rounded to the next billing interval
    """

    billsec = int(billsec)
    first_interval = int(first_interval)
    sub_interval = int(sub_interval)

    if billsec > 0:
        if billsec <= first_interval:
            billsec = first_interval

        elif (billsec % sub_interval) > 0:
            num_interval = (billsec / sub_interval) + 1
            billsec = sub_interval * num_interval

    return billsec

def get_ib_rate(number, acct_type):

    rate_cls = WholesaleIBRate
    return rate_cls.objects.get_longest_prefix_match(number)

def get_ob_rate(number, acct_type):

    rate_cls = WholesaleOBRate
    return rate_cls.objects.get_longest_prefix_match(number)

def get_tf_rate(number, acct_type):

    rate_cls = WholesaleTFRate
    return rate_cls.objects.get_longest_prefix_match(number)

def rate_call(cdr):
    """
    Calculates the total price of a call
    """
    rate = None
    user = cdr.get_user()
    try:
        acct_type = user.get_user_profile().acct_type
    except:
        acct_type = 'W'

    # Toll free regex
    tf = re.compile('^(\+|00)?(1)(800|855|866|877|888)([2-9]\d{6})$')

    # Lookup the rate for this call
    if cdr.call_direction == 'inbound':
        if tf.match(cdr.callee_number):
            rate = get_tf_rate(cdr.caller_number, acct_type)
        else:
            rate = get_ib_rate(cdr.callee_number, acct_type)

    elif cdr.call_direction == 'outbound' and cdr.callee_number != '911':
        rate = get_ob_rate(cdr.callee_number, acct_type)

    elif cdr.call_direction == 'onnet':
        rate = OnNetRate

    if rate is not None:
        # Round the billed time
        cdr.billedsec = round_billsec(cdr.billsec, rate.first_interval, rate.sub_interval)
        cdr.rate = rate.rate
        cdr.rate_description = rate.description
        cdr.rate_tstamp = datetime.utcnow().replace(tzinfo=utc)
        if cdr.cnam:
            cdr.charge = ((float(cdr.billedsec) / 60) * float(rate.rate)) + CNAM_LOOKUP_CHARGE
        else:
            cdr.charge = (float(cdr.billedsec) / 60) * float(rate.rate)
        #cdr.fcc_usf'] = 0

    return cdr


