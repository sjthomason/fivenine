from django.conf.urls import patterns, url

urlpatterns = patterns('rating.views',
    url(r'^inbound/$', 'toll_free_rates_view'),
    url(r'^inbound/toll-free/$', 'toll_free_rates_view'),
    url(r'^termination/$', 'termination_rates_view'),
    url(r'^termination/(?P<destination>\w+)/$', 'termination_rates_view'),
)
