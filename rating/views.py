import string
from django.views.generic import ListView

from models import WholesaleIBRate, WholesaleOBRate, WholesaleTFRate

class RateListView(ListView):

    def get_context_data(self, *args, **kwargs):
        context = super(RateListView, self).get_context_data(*args, **kwargs)
        destination = self.kwargs.get('destination')
        context['destination_list'] = map(
            (lambda x: {'value': x, 'selected':True} if x==destination \
             else {'value': x, 'selected':False }), string.ascii_uppercase)
        return context

    def get_queryset(self, *args, **kwargs):
        queryset = super(RateListView, self).get_queryset(*args, **kwargs)
        query = self.request.GET.get('q')
        destination = self.kwargs.get('destination')
        if query:
            queryset = self.model._default_manager.longest_prefix_match(query)
        if destination:
            queryset = queryset.filter(description__istartswith=destination)
        return queryset

termination_rates_view = RateListView.as_view(
                model=WholesaleOBRate,
                context_object_name='termination_rate_list',
                template_name='rating/termination_rate_list.html',
                paginate_by=100)

toll_free_rates_view = ListView.as_view(
                queryset=WholesaleTFRate.objects.filter(prefix__startswith=1),
                context_object_name='tollfree_rate_list',
                template_name='rating/tollfree_rate_list.html')
