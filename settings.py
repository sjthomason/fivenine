# Django settings for fivenine project.

import os
import mongoengine

INSTALL_ROOT = os.path.abspath(os.path.dirname(__file__))

DEBUG = False
#DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Spencer Thomason', 'spencer@5ninesolutions.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'fivenine',
        'USER': 'fivenine',
        'PASSWORD': 'KJKnZ84beLPJ',
        'HOST': '127.0.0.1',
        'PORT': '',
    }
}

# For the CDR database (MongoDB)
CDR_DB = {
    'NAME': 'sipswitch',
    'HOST': '127.0.0.1',                # IP Address of MongoDB Server
}

CDR_ARCHIVE_DB = {
    'NAME': 'cdr_archive',
    'HOST': '127.0.0.1',
}

CDR_ARCHIVE_DAYS = 90

mongoengine.connect(CDR_DB['NAME'], host=CDR_DB['HOST'], tz_aware=True)

# SIP Proxy IP addresses
SIP_PROXY_IPS = (
    '184.170.243.131',
    '184.170.249.3',
)

# SIP Registrar IP addresses
SIP_REGISTRAR_IPS = (
    '184.170.249.8',
)


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Los_Angeles'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute path to the directory that will hold database backups.
DB_BACKUP_ROOT = os.path.join(INSTALL_ROOT, 'db_backups/')
# Number of backups to keep
DB_BACKUP_COUNT = 5

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(INSTALL_ROOT, 'media/')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(INSTALL_ROOT, 'static/')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Absolute path to the directory that will hold FreeSWITCH media files.
FS_MEDIA_ROOT = '/opt/freeswitch/storage/'

# URL prefix for FreeSWITCH media files.
FS_MEDIA_URL = '/fs-media/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'v_iy4^d26+k!br$_i)psyq26%@n^y@o6(ipl7h__3+3=8npc63'
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'utils.middleware.TimezoneMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

SESSION_COOKIE_AGE = 60 * 60 * 24 * 2                  # Age of cookie, in seconds
SESSION_COOKIE_DOMAIN = "www.5ninesolutions.com"
SESSION_COOKIE_SECURE = False                           # Whether the session cookie should be secure (https:// only).
SESSION_ENGINE = 'django.contrib.sessions.backends.cache'  # The module to store session data

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
        'LOCATION': '127.0.0.1:11211'
    }
}
# The cache backend to use.  See the docstring in django.core.cache for the
# possible values.
CACHE_MIDDLEWARE_KEY_PREFIX = ''
CACHE_MIDDLEWARE_SECONDS = 600
CACHE_MIDDLEWARE_ALIAS = 'default'

ROOT_URLCONF = 'urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(INSTALL_ROOT, "templates"),
)

INSTALLED_APPS = (
    'admin',
    'billing',
    'cdr',
    'fax',
    'fs_xml_curl',
    'kb',
    'portal',
    'rating',
    'system',
    'web',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    #'django.contrib.sites',
    'django.contrib.messages',
    #'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    # 'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
)

AUTH_PROFILE_MODULE = 'portal.UserProfile'
ADMIN_LOGIN_REDIRECT_URL = '/admin/'
BILLING_BCC_EMAIL = 'admin@5ninesolutions.com'

# Days to send invoice before recurring renewal
RECURRING_INV_TERM = 14

# Day of the month reccuring invoices will be paid
RECURRING_RENEW_DAY = 1

# Maximum days for account activation
ACCOUNT_ACTIVATION_DAYS = 7

# Number of days past due date to send notification
OVERDUE_NOTIFICATION_DAYS = 21

# Number of days before credit card expiration to send notification
CC_EXP_NOTIFICATION_DAYS = 30

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
