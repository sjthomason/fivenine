area_code_url = '/accounts/dids/purchase/area_codes.json'
prefix_url = '/accounts/dids/purchase/area_codes.json?q='
list_url = '/accounts/dids/purchase/list/'

$.getJSON(area_code_url, area_code_choices);

function area_code_choices(data) {
    var len = data.area_codes.length;
    var html =''
    for (var i = 0; i< len; i++) {
      html += '<option value="' + data.area_codes[i] + '">' + data.area_codes[i] + '</option>';
    }
    $('select#area-code').html(html);
    url = prefix_url + data.area_codes[0];
    $.getJSON(url, prefix_choices);
}

function prefix_choices(data) {
    var len = data.prefixes.length;
    var html =''
    for (var i = 0; i< len; i++) {
      html += '<option value="' + data.prefixes[i] + '">' + data.prefixes[i] + '</option>';
    }
    $('select#prefix').html(html);
    url = list_url + '1' + data.area_codes[0] + data.prefixes[0] + '/';
    $('#did-list').load(url);
}

$(function(){
  $('select#area-code').bind('change', function() {
    url = prefix_url + $(this).val();
    $.getJSON(url, prefix_choices);
  })
});

$(function(){
  $('select#prefix').bind('change', function() {
    url = list_url + '1' + $('select#area-code').val() + $(this).val() + '/';
    $('#did-list').load(url);
  })
});
