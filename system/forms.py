from django import forms
from models import DrGateways, DrRules, GwAuthIP


class DrGatewayForm(forms.ModelForm):
    class Meta:
        model = DrGateways


class DrRuleForm(forms.ModelForm):

    #GWLIST_CHOICES = [(gw.pk, str(gw)) for gw in DrGateways.objects.all()]

    prefix = forms.RegexField(label="Prefix", max_length=16, regex=r'^\d+$',
        help_text = "Required.  16 digits or fewer.",
        error_messages = {'invalid': "This value may contain only numbers"})
    gwlist = forms.CharField(label="Gateway List", max_length=255)

    class Meta:
        model = DrRules
        fields = ("groupid", "prefix", "gwlist", "description",)


class GwIPForm(forms.ModelForm):
    class Meta:
        model = GwAuthIP
        fields = ("ip_addr", "port", "description",)
