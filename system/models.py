from django.db import models

import os


class DbBackup(models.Model):
    time_stamp = models.DateTimeField(editable=False)
    file_path = models.CharField(max_length=255, editable=False)

    class Meta:
        ordering = ['id']

    def __unicode__(self):
        return "Database Backup: %s" % self.time_stamp

    def delete(self, *args, **kwargs):
        os.remove(self.file_path)
        super(DbBackup, self).delete(*args, **kwargs)


class DrGateways(models.Model):
    gwid = models.AutoField(primary_key=True)
    type = models.IntegerField(default=0)
    address = models.CharField(max_length=128)
    strip = models.IntegerField(default=0)
    pri_prefix = models.CharField(max_length=64, blank=True, null=True)
    attrs = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=128)

    class Meta:
        ordering = ['gwid']
        db_table = 'dr_gateways'

    def __unicode__(self):
        return self.description

    def get_absolute_url(self):
        return "/admin/system/gateways/%s/" % self.pk


class DrRules(models.Model):
    RULE_CHOICES = (
        ('1', 'North America'),
        ('2', 'International'),
        ('3', 't.38 Fax'),
    )

    ruleid = models.AutoField(primary_key=True)
    groupid = models.CharField(max_length=255, choices=RULE_CHOICES)
    prefix = models.CharField(max_length=64)
    timerec = models.CharField(max_length=255)
    priority = models.IntegerField(default=1)
    routeid = models.CharField(max_length=64)
    gwlist = models.CharField(max_length=255)
    description = models.CharField(max_length=128, blank=True)

    class Meta:
        ordering = ['ruleid']
        db_table = 'dr_rules'

    def get_absolute_url(self):
        return "/admin/system/routes/%s/" % self.pk

    def get_drgateways(self):
        gwid_list = self.gwlist.split(',')
        drgateway_list = []
        for gw in gwid_list:
            gwid = int(gw.strip())
            drgateway_list.append(DrGateways.objects.get(pk=gwid))
        return drgateway_list

class DrGwLists(models.Model):
    gwlist = models.CharField(max_length=255)
    description = models.CharField(max_length=128, blank=True)

    class Meta:
        ordering = ['id']
        db_table = 'dr_gw_lists'


class DrGroups(models.Model):
    username = models.CharField(max_length=64)
    domain = models.CharField(max_length=128, blank=True)
    groupid = models.IntegerField()
    description = models.CharField(max_length=128, blank=True)

    class Meta:
        ordering = ['id']
        db_table = 'dr_groups'


class GwAuthIP(models.Model):
    grp = models.IntegerField(default=1)
    ip_addr = models.IPAddressField(unique=True)
    mask = models.IntegerField(default=32)
    port = models.IntegerField(default=0)
    tag = models.CharField(max_length=64, blank=True, null=True)
    description = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        ordering = ['ip_addr']
        db_table = 'gw_auth_ip'

    def __unicode__(self):
        return self.ip_addr

    def get_absolute_url(self):
        return "/admin/system/gateway-ips/%s/" % self.pk

