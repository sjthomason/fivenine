DROP TABLE dr_gw_lists;

CREATE TABLE dr_gw_lists (
    id SERIAL PRIMARY KEY NOT NULL,
    gwlist VARCHAR(255) NOT NULL,
    description VARCHAR(128) DEFAULT '' NOT NULL
);
