from django.conf.urls import patterns, url

from views import *

urlpatterns = patterns('system.views',
    url(r'^gateway-ips/$', 'gw_auth_ip_list_view'),
    url(r'^gateway-ips/add/$', 'gw_auth_ip_create_view'),
    url(r'^gateway-ips/(?P<pk>\d+)/edit/$', 'gw_auth_ip_update_view'),
    url(r'^gateway-ips/(?P<pk>\d+)/delete/$', 'gw_auth_ip_delete_view'),
    url(r'^gateways/$', DrGatewayListView.as_view()),
    url(r'^gateways/add/$', DrGatewayCreateView.as_view()),
    url(r'^gateways/(?P<pk>\d+)/edit/$', DrGatewayUpdateView.as_view()),
    url(r'^gateways/(?P<pk>\d+)/delete/$', DrGatewayDeleteView.as_view()),
    url(r'^routes/$', 'dr_rule_list_view'),
    url(r'^routes/add/$', 'dr_rule_create_view'),
    url(r'^routes/(?P<pk>\d+)/edit/$', 'dr_rule_update_view'),
    url(r'^routes/(?P<pk>\d+)/delete/$', 'dr_rule_delete_view'),
)
