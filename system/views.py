from django.conf import settings
from django.http import HttpResponseRedirect
from forms import DrGatewayForm, DrRuleForm, GwIPForm
from base_views.staff import (StaffCreateView, StaffDeleteView,
                              StaffListView, StaffUpdateView)
from models import DrGateways, DrRules, GwAuthIP
from utils.kamailio import KamailioServer


class AddressReloadListView(StaffListView):

    success_url = None

    def get_success_url(self):
        if self.success_url:
            url = self.success_url
        else:
            raise ImproperlyConfigured(
                "No URL to redirect to. Provide a success_url.")
        return url

    def post(self, request, *args, **kwargs):
        for ip in settings.SIP_PROXY_IPS:
            kam = KamailioServer(ip)
            kam.address_reload()
        return HttpResponseRedirect(self.get_success_url())


class DrReloadListView(StaffListView):

    success_url = None

    def get_success_url(self):
        if self.success_url:
            url = self.success_url
        else:
            raise ImproperlyConfigured(
                "No URL to redirect to. Provide a success_url.")
        return url

    def post(self, request, *args, **kwargs):
        for ip in settings.SIP_PROXY_IPS:
            kam = KamailioServer(ip)
            kam.dr_reload()
        return HttpResponseRedirect(self.get_success_url())

class DrRuleCreateView(StaffCreateView):

    def get_context_data(self, *args, **kwargs):
        context = super(DrRuleCreateView, self).get_context_data(*args, **kwargs)
        context['drgateway_list'] = DrGateways.objects.all()
        return context


class DrRuleUpdateView(StaffUpdateView):

    def get_context_data(self, *args, **kwargs):
        context = super(DrRuleUpdateView, self).get_context_data(*args, **kwargs)
        context['drgateway_list'] = DrGateways.objects.all()
        return context


class DrGatewayCreateView(StaffCreateView):

    form_class = DrGatewayForm
    model = DrGateways
    success_url = "/admin/system/gateways/"
    template_name = "system/dr_gw_form.html"


class DrGatewayUpdateView(StaffUpdateView):

    form_class = DrGatewayForm
    model = DrGateways
    success_url = "/admin/system/gateways/"
    template_name = "system/dr_gw_form.html"


class DrGatewayDeleteView(StaffDeleteView):
    pass


class DrGatewayListView(StaffListView):

    model = DrGateways
    context_object_name = "gw_list"
    template_name = "system/dr_gw_list.html"


dr_rule_list_view = DrReloadListView.as_view(
                            model=DrRules,
                            context_object_name="rule_list",
                            success_url="/admin/system/routes/",
                            template_name="system/dr_rule_list.html")
dr_rule_create_view = DrRuleCreateView.as_view(
                            form_class=DrRuleForm,
                            model=DrRules,
                            success_url="/admin/system/routes/",
                            template_name="system/dr_rule_form.html")
dr_rule_update_view = DrRuleUpdateView.as_view(
                            form_class=DrRuleForm,
                            model=DrRules,
                            success_url="/admin/system/routes/",
                            template_name="system/dr_rule_form.html")
dr_rule_delete_view = StaffDeleteView.as_view(
                            model=DrRules,
                            success_url="/admin/system/routes/",
                            template_name="admin/confirm_delete.html")

gw_auth_ip_list_view = AddressReloadListView.as_view(
                            model=GwAuthIP,
                            context_object_name="gw_ip_list",
                            success_url="/admin/system/gateway-ips/",
                            template_name="system/gw_ips.html")
gw_auth_ip_create_view = StaffCreateView.as_view(
                            form_class=GwIPForm,
                            model=GwAuthIP,
                            success_url="/admin/system/gateway-ips/",
                            template_name="system/gw_ip_form.html")
gw_auth_ip_update_view = StaffUpdateView.as_view(
                            form_class=GwIPForm,
                            model=GwAuthIP,
                            success_url="/admin/system/gateway-ips/",
                            template_name="system/gw_ip_form.html")
gw_auth_ip_delete_view = StaffDeleteView.as_view(
                            model=GwAuthIP,
                            success_url="/admin/system/gateway-ips/",
                            template_name="system/confirm_delete.html")

