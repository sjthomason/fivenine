Credit Card Charge Failure

Date: {{ date }}

Hello {{ user.first_name }},
We have attempted to charge your credit card that you have on file with us for your outstanding hosting/server charges due to us. Unfortunatly, the charges are being declined.

{% if error_msg %}Error Message: {{ error_msg }}{% endif %}

Please contact us as soon as possible to arrange payment.

Thank You,

Billing Dept.
5Nine Solutions

Thank you for choosing 5Nine Solutions!
