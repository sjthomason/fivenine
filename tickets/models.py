from django.db import models

# https://github.com/rossp/django-helpdesk/blob/master/helpdesk/models.py

class Queue(models.Model):

    title = models.CharField(max_length=255)
    slug = models.SlugField(help_text='This slug is used when building ticket ID\'s.')
    email_address = models.EmailField(blank=True, null=True, help_text='Outgoing e-mail addres for this queue.')
    new_ticket_cc = models.CharField(blank=True, null=True, max_length=255)
    updated_ticket_cc = models.CharField(blank=True, null=True, max_length=255)

    class Meta:
        ordering = ['title']

    def __unicode__(self):
        return u"%s" % self.title


class Ticket(models.Model):

    OPEN_STATUS = 1
    REOPENED_STATUS = 2
    RESOLVED_STATUS = 3
    CLOSED_STATUS = 4
    DUPLICATE_STATUS = 5

    OPEN_STATUS_CHOICES = (
        (OPEN_STATUS, 'Open'),
        (REOPENED_STATUS, 'Reopened'),
    )

    CLOSED_STATUS_CHOICES = (
        (RESOLVED_STATUS, 'Resolved'),
        (CLOSED_STATUS, 'Closed'),
        (DUPLICATE_STATUS, 'Duplicate'),
    )

    STATUS_CHOICES = OPEN_STATUS_CHOICES + CLOSED_STATUS_CHOICES

    PRIORITY_CHOICES = (
        (1, '1 - Critical'),
        (2, '2 - High'),
        (3, '3 - Normal'),
        (4, '4 - Low'),
        (5, '5 - Very Low'),
    )

    user = models.ForeignKey(User, blank=True, null=True)
    queue = models.ForeignKey(Queue, verbose_name="Queue")
    submitter_email = models.EmailField('Submitter E-Mail',
        blank=True, null=True,
        help_text="The submitter will receive an email for all public "\
                  "follow-ups left for this task")
    created = models.DateTimeField("Created", auto_now_add=True,
                    help_text="Date this ticket was first created")
    modified = models.DateTimeField("Modified", auto_now=True)
    subject = models.CharField("Subject", max_length=255)
    description = models.TextField("Description",
        help_text="A detailed description of your problem.")
    assigned_to = models.ForeignKey(User,
                                    verbose_name="Assigned_to",
                                    related_name="assigned_to",
                                    blank=True, null=True)
    status = models.SmallIntegerField(choices=STATUS_CHOICES,
                                      default=OPEN_STATUS)
    priority = models.IntegerField(choices=PRIORITY_CHOICES, default=3, blank=3,
        help_text="1 = Highest Priority, 5 = Low Priority")

    def _get_assigned_to(self):
        """
        Custom property to allow us to easily print 'Unassigned' if a
        ticket has no owner, or the users name if it's assigned. If the user
        has a full name configured, we use that, otherwise their username.
        """
        if not self.assigned_to:
            return "Unassigned"
        else:
            full_name = self.assigned_to.get_full_name()
            if full_name:
                return full_name
            else:
                return self.assigned_to.username
    get_assigned_to = property(_get_assigned_to)

    def _get_ticket(self):
        """
        A user-friendly ticket ID, which is a combination of ticket ID
        and queue slug. This is generally used in e-mail subjects.
        """
        return u"[%s]" % (self.ticket_for_url)
    ticket = property(_get_ticket)

    def _get_ticket_for_url(self):
        """
        A URL-friendly ticket ID, used in links.
        """
        return u"%s #%s" % (self.queue.slug, self.id)
    ticket_for_url = property(_get_ticket_for_url)

    class meta:
        ordering = ['created']

    def __unicode__(self):
        return u"%s - %s" % (self.id, self.subject)

    def is_closed(self):
        return self.status in CLOSED_STATUS_CHOICES


class UpdateManager(models.Manager):
    def private_followups(self):
        return self.filter(public=False)

    def public_followups(self):
        return self.filter(public=True)


class Update(models.Model):
    """
    An Update is a comment and/or change to a ticket. We keep a simple
    title, the comment entered by the user, and the new status of a ticket
    to enable easy flagging of details on the view-ticket page.

    The title is automatically generated at save-time, based on what action
    the user took.

    Tickets that aren't public are never shown to or e-mailed to the submitter,
    although all staff can see them.
    """

    ticket = models.ForeignKey(Ticket, verbose_name="Ticket")
    date = models.DateTimeField("Date", default=datetime.now())
    title = models.CharField("Title", max_length=255, blank=True, null=True)
    comment = models.TextField("Comment", blank=True, null=True)
    public = models.BooleanField("Public", blank=True, default=False,
        help_text="Public tickets are viewable by the submitter and all "\
            "staff, but non-public tickets can only be seen by staff.")

    user = models.ForeignKey(User,blank=True, null=True, verbose_name="User")
    new_status = models.IntegerField(
        "New Status",
        choices=Ticket.STATUS_CHOICES,
        blank=True,
        null=True,
        help_text="If the status was changed, what was it changed to?",
        )

    objects = UpdateManager()

    class Meta:
        ordering = ['date']

    def __unicode__(self):
        return u'%s' % self.title

    def get_absolute_url(self):
        return u"%s#followup%s" % (self.ticket.get_absolute_url(), self.id)

    def save(self, *args, **kwargs):
        t = self.ticket
        t.save()
        super(FollowUp, self).save(*args, **kwargs)


class Attachment(models.Model):
    pass


