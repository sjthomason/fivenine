from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.views.generic import TemplateView, RedirectView

from web.views import (DomainFormView, SupportFormView)

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'web.views.index_view'),
    url(r'^about/$', 'web.views.about_view'),
    url(r'^about/network/$', 'web.views.network_view'),
    url(r'^accounts/', include('portal.urls')),
    url(r'^admin/', include('admin.urls')),
    url(r'^contact/$', 'web.views.contact_view'),
    url(r'^contact/done/$', 'web.views.contact_done_view'),
    url(r'^cdr/import/$', 'cdr.views.cdr_import'),
    url(r'^cnam/(?P<number>[2-9]\d\d[2-9]\d{6})/$', 'cnam.views.query'),
    url(r'^crm/phone-lookup/(?P<number>\d{11,16})/$', 'cnam.views.cust_lookup'),
    url(r'^domains/check/', DomainFormView.as_view()),
    url(r'^fax/', include('fax.urls')),
    url(r'^fs-xml/', include('fs_xml_curl.urls')),
    url(r'^kb/', include('kb.urls')),
    url(r'^legal/terms-of-use/$',
        TemplateView.as_view(template_name='terms.html')),
    url(r'^legal/privacy/$',
        TemplateView.as_view(template_name='privacy.html')),
    url(r'^rates/', include('rating.urls')),
    url(r'^services/$', 'web.views.services_view'),
    url(r'^voip-pbx-hosting/$', 'web.views.pbx_service_view'),
    url(r'^web-hosting/$', 'web.views.hosting_service_view'),
    url(r'^sip-trunking/$', 'web.views.services_trunking_view'),
    url(r'^signup/', include('web.registration_urls')),
    url(r'^support/$', SupportFormView.as_view()),
    url(r'^sitemap\.xml$', 'web.views.sitemap_view'),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += patterns('',
    url(r'^kb\.php$', RedirectView.as_view(url='/kb/')),
    url(r'^helpdesk\.php$', RedirectView.as_view(url='/support/')),
    url(r'^cart\.php$', RedirectView.as_view(url='/')),
    url(r'^index\.php$', RedirectView.as_view(url='/')),
    url(r'^transcheck\.php$', RedirectView.as_view(url='/')),
    url(r'^aLogin\.php$', RedirectView.as_view(url='/accounts/login/')),
    url(r'^LostPassword\.php$', RedirectView.as_view(url='/accounts/password-reset/')),
    url(r'^contact\.php$', RedirectView.as_view(url='/contact/')),
    url(r'^terms\.php$', RedirectView.as_view(url='/legal/terms-of-use/')),
    url(r'^support\.php$', RedirectView.as_view(url='/support/')),
    url(r'^check\.php$', RedirectView.as_view(url='/web-hosting/')),
    url(r'^hosting\.php$', RedirectView.as_view(url='/web-hosting/')),
    url(r'^forsale\.php$', RedirectView.as_view(url='/web-hosting/')),
    url(r'^hsignup\.php$', RedirectView.as_view(url='/signup/')),
    url(r'^createacct\.php$', RedirectView.as_view(url='/signup/')),
    url(r'^network\.php$', RedirectView.as_view(url='/about/network/')),
)
