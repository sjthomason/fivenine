import hashlib

def key_from_args(*args, **kwargs):
    """
    generates a unique key from arguments
    """
    key = []
    for arg in args:
        key.append(str(arg))
    for k in kwargs:
        key.append(str(k) + '=' + str(kwargs[k]))

    key = ''.join(key)
    return hashlib.md5(key).hexdigest()
