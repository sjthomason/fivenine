import urllib2
import re
import simplejson as json

from datetime import date
from django.utils.http import urlencode

RESTRICTED_USERNAME_WORDS = (
    'root',
    'bin',
    'daemon',
    'adm',
    'lp',
    'sync',
    'shutdown',
    'halt',
    'mail',
    'news',
    'uucp',
    'operator',
    'games',
    'gopher',
    'ftp',
    'nobody',
    'dbus',
    'mailnull',
    'smmsp',
    'nscd',
    'vcsa',
    'rpc',
    'sshd',
    'rpcuser',
    'nfsnobody',
    'pcap',
    'rpm',
    'haldaemon',
    'cpanel',
    'named',
    'mysql',
    'mailman',
    'cpanelhorde',
    'cpanelphpmyadmin',
    'cpanelphppgadmin',
    'cpanelroundcube',
    'avahi',
    'avahi-autoipd',
    'xfs',
    'mx',
    'postgres',
    'clamav',
    'dovecot',
    'munin',
)


class CpanelError(Exception):
    pass

def createacct(server, user, password, domain, plan, **kwargs):
    """
    Creates a hosting account and sets up its associated domain information
    """

    server_url = "https://%s:2087/json-api/createacct" % (server.ip_addr)

    headers = { 'Authorization': "WHM %s:%s" % (server.admin_user,
                    re.sub('(\r|\n)','', server.admin_key)) }

    request_dict = {}
    request_dict['username'] = user
    request_dict['password'] = password
    request_dict['domain'] = domain
    request_dict['plan'] = plan

    for key, value in kwargs.iteritems():
        request_dict[key] = value

    request_string = urlencode(request_dict)
    request = urllib2.Request(server_url, request_string, headers)
    response = json.load(urllib2.urlopen(request))
    if int(response['result'][0]['status']) != 1:
        raise CpanelError(response['result'][0]['statusmsg'])


def loadavg(server):
    """
    Return the number of processes in the system run queue averaged over the last 1, 5, and 15 minutes
    """

    server_url = "https://%s:2087/json-api/loadavg" % (server.ip_addr)

    headers = { 'Authorization': "WHM %s:%s" % (server.admin_user,
                    re.sub('(\r|\n)','', server.admin_key)) }

    request_string = None
    request = urllib2.Request(server_url, request_string, headers)
    response = json.load(urllib2.urlopen(request))
    loadavg = (float(response['one']),
               float(response['five']),
               float(response['fifteen']))
    return loadavg


def passwd(server, user, password):
    """
    Changes the password of a domain owner (cPanel) or reseller (WHM) account.
    """

    server_url = "https://%s:2087/json-api/passwd" % (server.ip_addr)

    headers = { 'Authorization': "WHM %s:%s" % (server.admin_user,
                    re.sub('(\r|\n)','', server.admin_key)) }

    request_string = urlencode({'user':user, 'pass':password})
    request = urllib2.Request(server_url, request_string, headers)
    response = json.load(urllib2.urlopen(request))
    if int(response['passwd'][0]['status']) != 1:
        raise CpanelError(response['passwd'][0]['statusmsg'])


def removeacct(server, user, keepdns=False):
    """
    This function permanently removes a cPanel account
    """

    server_url = "https://%s:2087/json-api/removeacct" % (server.ip_addr)

    headers = { 'Authorization': "WHM %s:%s" % (server.admin_user,
                    re.sub('(\r|\n)','', server.admin_key)) }

    if not keepdns:
        request_string = urlencode({'user':user, 'keepdns':'2'})
    else:
        request_string = urlencode({'user':user, 'keepdns':'1'})

    request = urllib2.Request(server_url, request_string, headers)
    response = json.load(urllib2.urlopen(request))
    if int(response['result'][0]['status']) != 1:
        raise CpanelError(response['result'][0]['statusmsg'])


def show_bw(server, user, month=None, year=None):

    """
    Returns the user's bandwidth usage in bytes
    """

    if month is None:
        month = int(date.today().strftime('%m'))

    if year is None:
        year = int(date.today().strftime('%Y'))

    server_url = "https://%s:2087/json-api/showbw" % (server.ip_addr)
    headers = { 'Authorization': "WHM %s:%s" % (server.admin_user,
                    re.sub('(\r|\n)','', server.admin_key)) }
    request_string = urlencode({'month':month, 'year':year,
                                'search':user, 'searchtype':'user' })
    request = urllib2.Request(server_url, request_string, headers)
    try:
        response = json.load(urllib2.urlopen(request))
        return int(response['bandwidth'][0]['totalused'])
    except (ValueError, KeyError):
        raise CpanelError()


def suspendacct(server, user, reason=None):
    """
    Changes the password of a domain owner (cPanel) or reseller (WHM) account.
    """

    server_url = "https://%s:2087/json-api/suspendacct" % (server.ip_addr)

    headers = { 'Authorization': "WHM %s:%s" % (server.admin_user,
                    re.sub('(\r|\n)','', server.admin_key)) }

    if reason is not None:
        request_string = urlencode({'user':user, 'reason':reason})
    else:
        request_string = urlencode({'user':user})

    request = urllib2.Request(server_url, request_string, headers)
    response = json.load(urllib2.urlopen(request))
    if int(response['result'][0]['status']) != 1:
        raise CpanelError(response['result'][0]['statusmsg'])


def unsuspendacct(server, user):
    """
    Unsuspend a suspended account
    """

    server_url = "https://%s:2087/json-api/unsuspendacct" % (server.ip_addr)

    headers = { 'Authorization': "WHM %s:%s" % (server.admin_user,
                    re.sub('(\r|\n)','', server.admin_key)) }

    request_string = urlencode({'user':user})
    request = urllib2.Request(server_url, request_string, headers)
    response = json.load(urllib2.urlopen(request))
    if int(response['result'][0]['status']) != 1:
        raise CpanelError(response['result'][0]['statusmsg'])


def version(server):
    """
    Display the version of cPanel & WHM running on the server
    """

    server_url = "https://%s:2087/json-api/version" % (server.ip_addr)

    headers = { 'Authorization': "WHM %s:%s" % (server.admin_user,
                    re.sub('(\r|\n)','', server.admin_key)) }

    request_string = None
    request = urllib2.Request(server_url, request_string, headers)
    response = json.load(urllib2.urlopen(request))

    return response['version']
