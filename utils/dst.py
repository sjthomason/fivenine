import datetime
from pytz import timezone

LOCAL_TZ = 'America/Los_Angeles'

def is_dst(dt=None):
    tz = timezone(LOCAL_TZ)

    if dt is None:
        dt = datetime.datetime.utcnow()

    local_dt = tz.localize(dt)

    if local_dt.dst():
        return True

    return False
