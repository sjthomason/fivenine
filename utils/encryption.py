import base64
import cPickle as pickle
import hashlib
import struct
import zlib
from Crypto.Cipher import AES
from Crypto import Random

from django.conf import settings

IV_LEN = 16
PASSWORD = settings.SECRET_KEY
SALT_SIZE = 16
ITERATIONS = 10000


class ChecksumError(Exception):
    pass

def generate_key(salt, password=PASSWORD, iterations=ITERATIONS):
    key = password + salt
    for i in xrange(ITERATIONS):
        key = hashlib.sha256(key).digest()

    return key

def rand_bytes(n):
    """
    returns n random bytes
    """
    return Random.new().read(n)

def encrypt(data, checksum=True):
    plain = pickle.dumps(data)
    iv = rand_bytes(IV_LEN)
    salt = rand_bytes(SALT_SIZE)
    key = generate_key(salt)
    cipher = AES.new(key, AES.MODE_CFB, iv)

    if checksum:
        plain += struct.pack("i", zlib.crc32(plain))

    return base64.b64encode(iv + salt + cipher.encrypt(plain))

def decrypt(data, checksum=True):
    data = base64.b64decode(data)
    iv = data[:IV_LEN]
    salt = data[IV_LEN:IV_LEN + SALT_SIZE]
    data = data[IV_LEN + SALT_SIZE:]
    key = generate_key(salt)
    cipher = AES.new(key, AES.MODE_CFB, iv)

    plain = cipher.decrypt(data)

    if checksum:
        crc = plain[-4:]
        plain = plain[:-4]
        if not crc == struct.pack("i", zlib.crc32(plain)):
            raise ChecksumError("checksum mismatch")

    return pickle.loads(plain)

def encode(data, checksum=True):
    data = pickle.dumps(data)
    if checksum:
        data += struct.pack("i", zlib.crc32(data))
    return base64.urlsafe_b64encode(data)

def decode(data, checksum=True):
    data = base64.urlsafe_b64decode(str(data))

    if checksum:
        crc = data[-4:]
        data = data[:-4]
        if not crc == struct.pack("i", zlib.crc32(data)):
            raise ChecksumError("checksum mismatch")

    return pickle.loads(data)
