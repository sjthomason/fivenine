import xml.etree.cElementTree as etree
import urllib2
import simplejson as json

from datetime import datetime
from django.utils.http import urlencode
from pytz import timezone
from utils.encryption import decrypt

TEST_MODE = False

ENOM_USER = "sjthomason"
ENOM_PASSWORD = decrypt('bZoacZstdu38OmjUOdt4hoalkDrtyUMvY7YEB3+2xqUwJgGwGJtyB9xiwzo4HGKcINMF50t+xjc=')
ENOM_TZ = timezone('America/Los_Angeles')

API_URL = 'https://reseller.enom.com/interface.asp'
API_TEST_URL = 'https://resellertest.enom.com/interface.asp'

ENOM_DEFAULT_PARAMS = {
    'uid': ENOM_USER,
    'pw': ENOM_PASSWORD,
    'responsetype': 'XML',
}

if TEST_MODE:
    API_URL = API_TEST_URL
    ENOM_USER = "resellid"
    ENOM_PASSWORD = "resellpw"


class EnomError(Exception):
    pass


def get_enom_response(params):
    """
    processes an api request and returns an ElementTree object
    """

    query_string = urlencode(params)
    request = urllib2.Request(API_URL, query_string)
    tree = etree.parse(urllib2.urlopen(request))
    doc = tree.getroot()
    errors = int(doc.find('ErrCount').text)
    if errors:
        e = list(doc.find('errors'))[0].text
        raise EnomError(e)
    return tree


def authorize_tld(tld):
    """
    Create or add to a list of TLDs that you offer to your resellers
    and retail customers.
    """
    params = ENOM_DEFAULT_PARAMS
    params['command'] = 'authorizetld'
    if type(tld) == type(list()):
        params['domainlist'] = ','.join(tld)
    else:
        params['tld'] = tld

    get_enom_response(params)


def check(sld, tld=None):
    """
    Query the Registry to determine whether a specific domain name is available
    Returns a list of tuples of available sld's and tld's for the given sld
    or None if no domain names are available
    """

    # search com, net, org, info, biz, us
    if tld is None:
        tld = '*2'

    params = ENOM_DEFAULT_PARAMS
    params['command'] = 'check'
    params['sld'] = sld
    params['tld'] = tld
    doc = get_enom_response(params).getroot()
    try:
        domain_count = int(doc.find('DomainCount').text)
    except AttributeError:
        domain_count = 1

    domain_list = []
    if domain_count > 1:
        for i in xrange(1, domain_count + 1):
            rrp_code = int(doc.find('RRPCode%s' % i).text)
            if rrp_code == 210:
                domain = tuple(doc.find('Domain%s' % i).text.split('.'))
                domain_list.append(domain)
    else:
        rrp_code = int(doc.find('RRPCode').text)
        if rrp_code == 210:
            domain = tuple(doc.find('DomainName').text.split('.'))
            domain_list.append(domain)

    if domain_list:
        return domain_list


def extend(sld, tld, years):
    """
    Extend (renew) the registration period for a domain name.
    """
    params = ENOM_DEFAULT_PARAMS
    params['command'] = 'extend'
    params['sld'] = sld
    params['tld'] = tld
    params['numyears'] = years

    doc = get_enom_response(params).getroot()
    return doc.find('OrderID').text

def get_domain_info(sld, tld):
    """
    Get information about a single domain name
    """
    params = ENOM_DEFAULT_PARAMS
    params['command'] = 'GetDomainInfo'
    params['sld'] = sld
    params['tld'] = tld
    doc = get_enom_response(params).getroot()
    status = doc.find('GetDomainInfo').find('status')
    services = doc.find('GetDomainInfo').find('services')
    domain_id = doc.find('GetDomainInfo').find('domainname').get('domainnameid')

    domain_info = {}
    domain_info['domain_id'] = domain_id
    # Get expiration
    exp_dt = datetime.strptime(status.find('expiration').text,
                               '%m/%d/%Y %I:%M:%S %p').replace(tzinfo=ENOM_TZ)
    domain_info['expiration'] = exp_dt

    # Get DNS servers
    dns_list = []
    for entry in services.findall('entry'):
        if entry.get('name') == 'dnsserver':
            for dns in entry.find('configuration').findall('dns'):
                dns_list.append(dns.text)

    for i in xrange(0,4):
        k = 'ns%s' % (i + 1)
        try:
            domain_info[k] = dns_list[i]
        except (IndexError):
            domain_info[k] = ''

    return domain_info


def get_renew(sld, tld):
    """
    Get the auto-renew setting for a domain name
    """
    params = ENOM_DEFAULT_PARAMS
    params['command'] = 'getrenew'
    params['sld'] = sld
    params['tld'] = tld

    doc = get_enom_response(params).getroot()
    if int(doc.find('auto-renew').text) == 1:
        return True
    return False

def name_spinner(sld, tld, max_results=10):
    """
    Return a list of tuples of sld's and tld's of variations
    of a domain name that you specify, for .com, .net, .tv, and .cc TLDs.
    """
    SPINNER_TLDS = ['com', 'net', 'tv', 'cc']

    params = ENOM_DEFAULT_PARAMS
    params['command'] = 'namespinner'
    params['sld'] = sld
    params['tld'] = tld
    params['maxresults'] = max_results

    # search settings
    params['basic'] = 'high'
    params['related'] = 'high'
    params['similar'] = 'low'
    params['topical'] = 'off'

    doc = get_enom_response(params).getroot()
    domain_list = []
    for domain in doc.find('namespin').find('domains'):
        domain_sld = domain.get('name').lower()
        if tld in SPINNER_TLDS:
            if domain.get(tld) == 'y':
                domain_list.append((domain_sld, tld))
        else:
            for domain_tld in SPINNER_TLDS:
                if domain.get(domain_tld) == 'y':
                    domain_list.append((domain_sld, domain_tld))

    if domain_list:
        return domain_list

def purchase(sld, tld, years, ns1, ns2, ns3=None, ns4=None, lock=True):

    params = ENOM_DEFAULT_PARAMS
    params['command'] = 'purchase'
    params['sld'] = sld
    params['tld'] = tld
    params['numyears'] = years

    params['ns1'] = ns1
    params['ns2'] = ns2
    if ns3 is not None: params['ns3'] = ns3
    if ns4 is not None: params['ns4'] = ns4

    params['unlockregistrar'] = 0
    if not lock: params['unlockregistrar'] = 1

    params['renewname'] = 0

    doc = get_enom_response(params).getroot()
    return doc


def remove_tld(tld):
    """
    Remove TLDs from your list of authorized TLDs that you offer to
    your resellers and retail customers.
    """
    params = ENOM_DEFAULT_PARAMS
    params['command'] = 'removetld'
    if type(tld) == type(list()):
        params['domainlist'] = ','.join(tld)
    else:
        params['tld'] = tld
    get_enom_response(params)


def get_tld_list():
    """
    Retrieve a list of the TLDs that you offer
    """
    params = ENOM_DEFAULT_PARAMS
    params['command'] = 'gettldlist'
    doc = get_enom_response(params).getroot()
    tld_list = []
    for tld in doc.find('tldlist').findall('tld'):
        t = tld.find('tld').text
        if t:
            tld_list.append(t)
    return tld_list

def set_contacts(sld, tld, contact_type, first_name=None, last_name=None,
                org_name=None, job_title=None, address_1=None,
                address_2=None, city=None, state=None, postal_code=None,
                country='US', email_address=None, phone=None, fax=None):
    """
    Updates contact information for a domain name
    """
    params = ENOM_DEFAULT_PARAMS
    params['command'] = 'contacts'
    params[contact_type + 'FirstName'] = first_name
    params[contact_type + 'LastName'] = last_name
    params[contact_type + 'OrganizationName'] = org_name
    params[contact_type + 'JobTitle'] = job_title
    params[contact_type + 'Address1'] = address_1
    params[contact_type + 'Address2'] = address_2
    params[contact_type + 'City'] = city
    params[contact_type + 'StateProvinceChoice'] = 'S'
    params[contact_type + 'StateProvince'] = state
    params[contact_type + 'PostalCode'] = postal_code
    params[contact_type + 'Country'] = country
    params[contact_type + 'EmailAddress'] = email_address
    params[contact_type + 'Phone'] = phone
    params[contact_type + 'Fax'] = fax

    doc = get_enom_response(params).getroot()

def set_renew(sld, tld, renew=False):
    pass


