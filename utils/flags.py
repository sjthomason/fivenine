set_flag = lambda x, flag: x | flag
clear_flag = lambda x, flag: x ^ flag
test_flag = lambda x, flag: True if x & flag else False

class Flag(object):

    value = 0

    def __init__(self, flag=0):
        self.value = set_flag(self.value, flag)

    def __nonzero__(self):
        if self.value:
            return True
        return False

    def __eq__(self, other):
        return test_flag(self.value, other)

    def __ne__(self, other):
        return not test_flag(self.value, other)

    def __add__(self, other):
        return Flag(set_flag(self.value, other))

    def __sub__(self, other):
        return Flag(clear_flag(self.value, other))

    def __radd__(self, other):
        return other + self.value

    def __rsub__(self, other):
        return other - self.value

    def __iadd__(self, other):
        self.value = set_flag(self.value, other)
        return self

    def __isub__(self, other):
        self.value = clear_flag(self.value, other)
        return self

    def __len__(self):
        return len(bin(self.value)[2:])

    def __getitem__(self, key):
        key = 2**key
        return test_flag(self.value, key)

    def __setitem__(self, key, value):
        key = 2**key
        if value:
            value = 1
        else:
            value = 0
        self.value = set_flag(self.value, key * value)

    def __repr__(self):
        return str(list(bin(self.value))[2:][::-1])

    def __str__(self):
        return self.__repr__()

    def __unicode__(self):
        return unicode(self.__str__())
