import calendar

from datetime import date, datetime
from django import forms

from utils import validators


UNMASKED_DIGITS_TO_SHOW = 4
mask_cc = lambda c: "%s%s" % ("*" * max(len(c) - UNMASKED_DIGITS_TO_SHOW, 0), c[-UNMASKED_DIGITS_TO_SHOW:])

CC_CHOICES = (
    ('VISA', 'Visa'),
    ('MSTR', 'MasterCard'),
    ('AMEX', 'American Express'),
)

PAYMENT_METHOD_CHOICES = CC_CHOICES + (
    ('CASH', 'Cash'),
    ('CHECK', 'Check'),
    ('CREDIT', 'Account Credit'),
)

def validate_luhn_checksum(cc_num):
    """
    checks to make sure that the card passes a luhn mod-10 checksum
    """

    cc_num = str(cc_num)

    try:
        int(cc_num)
    except ValueError:
        return False

    sum = 0
    num_digits = len(cc_num)
    oddeven = num_digits & 1

    for i in range(0, num_digits):
        digit = int(cc_num[i])

        if not (( i & 1 ) ^ oddeven ):
            digit = digit * 2
        if digit > 9:
            digit = digit - 9

        sum = sum + digit

    return ( (sum % 10) == 0 )


class BlankInput(forms.TextInput):

    def render(self, name, value, attrs=None):
        value = None
        return super(BlankInput, self).render(name, value, attrs)


class CountryField(forms.ChoiceField):
    def __init__(self, *args, **kwargs):
        from countries import COUNTRIES
        super(CountryField, self).__init__(choices=COUNTRIES, initial='US',
                                           *args, **kwargs)


class CreditCardField(forms.CharField):

    widget = BlankInput

    def clean(self, value):
        """
        Check if given CC number is valid and one of the
        card types we accept
        """

        # Remove spaces and dashes
        value = value.replace(' ', '')
        value = value.replace('-', '')

        if not validate_luhn_checksum(value):
            raise forms.ValidationError("Please enter a valid "+\
                "credit card number.")

        return super(CreditCardField, self).clean(value)


class CreditCardTypeField(forms.ChoiceField):
    def __init__(self, *args, **kwargs):
        super(CreditCardTypeField, self).__init__(choices=CC_CHOICES,
                                                  *args, **kwargs)


class CreditCardExpMonthField(forms.ChoiceField):
    def __init__(self, *args, **kwargs):
        EXP_MONTH_CHOICES = tuple([(i, '%02d - %s' % (i, calendar.month_name[i]))
                                  for i in xrange(1, 13)])
        super(CreditCardExpMonthField, self).__init__(choices=EXP_MONTH_CHOICES,
                                                      *args, **kwargs)


class CreditCardExpYearField(forms.ChoiceField):
    def __init__(self, *args, **kwargs):
        EXP_YEAR_CHOICES = [(x, x) for x in xrange(date.today().year,
                                                   date.today().year + 15)]
        super(CreditCardExpYearField, self).__init__(choices=EXP_YEAR_CHOICES,
                                                     initial=date.today().year + 3,
                                                     *args, **kwargs)


class PaymentField(forms.DecimalField):

    def clean(self, value):
        value = value.replace('$', '')
        value = value.replace(',', '')
        return super(PaymentField, self).clean(value)


class MultipleEmailField(forms.CharField):
    default_error_messages = {
        'invalid': "Enter a valid e-mail address or addresses "\
                   "separated by commas."
    }
    default_validators = [validators.validate_multiple_email]

    def clean(self, value):
        value = self.to_python(value).strip()
        return super(MultipleEmailField, self).clean(value)
