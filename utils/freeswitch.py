import simplejson as json

from datetime import datetime
from django.core.cache import cache
from django.utils.timezone import utc

from portal.models import Endpoint
from utils.cache import key_from_args
from utils.ESL import ESLconnection

show_calls_cache_key = key_from_args(__file__, 'FreeswitchServer', 'show_calls')

class FreeswitchServer(object):
    con = None

    def __init__(self, host, password, port='8021'):

        con = ESLconnection(host, port, password)
        if con.connected():
            self.con = con
        else:
            #TODO
            # raise error
            pass

    def show_calls(self):
        """
        returns the output of show calls as a list of dictionaries
        """
        call_list = []
        raw_calls = self.con.api('show', 'calls as json').getBody()

        try:
            raw_calls = json.loads(raw_calls)['rows']
        except KeyError:
            return call_list

        if raw_calls:
            for call in raw_calls:
                if call['created_epoch']:
                    call['created'] = datetime.utcfromtimestamp(
                        int(call['created_epoch'])).replace(tzinfo=utc)
                if call['b_created_epoch']:
                    call['b_created'] = datetime.utcfromtimestamp(
                        int(call['b_created_epoch'])).replace(tzinfo=utc)
                call_list.append(call)
        return call_list


def get_calls():
    """
    Returns a list of all active calls
    """
    key = key_from_args(__file__, 'get_calls')
    endpoint_list = cache.get(key)
    if endpoint_list is None:
        endpoint_list = list(Endpoint.objects.all())
        cache.set(key, endpoint_list, 600)

    fs_call_list = cache.get(show_calls_cache_key)
    if fs_call_list is None:
        fs = FreeswitchServer(host='10.59.1.95',
                              password='zQzvCtEyfDpH',
                              port='8021')
        fs_call_list = fs.show_calls()
        cache.set(show_calls_cache_key, fs_call_list, 2)

    call_list = []
    for fs_call in fs_call_list:
        for endpoint in endpoint_list:
            if fs_call['presence_data'] == endpoint.pk:
                call = {}
                call['endpoint'] = endpoint
                call['caller_name'] = fs_call['b_cid_name'] or fs_call['cid_name']
                call['caller_num'] = fs_call['b_cid_num'] or fs_call['cid_num']
                call['callee_num'] = fs_call['dest']
                call['created'] = fs_call['created']
                call_list.append(call)
    return call_list

def get_calls_for_user(user):
    """
    Returns a list of a active calls for a user
    """
    key = key_from_args(__file__, 'get_calls_for_user', user)
    endpoint_list = cache.get(key)
    if endpoint_list is None:
        endpoint_list = list(Endpoint.objects.filter(user=user))
        cache.set(key, endpoint_list, 600)

    fs_call_list = cache.get(show_calls_cache_key)
    if fs_call_list is None:
        fs = FreeswitchServer(host='10.59.1.95',
                              password='zQzvCtEyfDpH',
                              port='8021')
        fs_call_list = fs.show_calls()
        cache.set(show_calls_cache_key, fs_call_list, 2)

    call_list = []
    for fs_call in fs_call_list:
        for endpoint in endpoint_list:
            if fs_call['presence_data'] == endpoint.pk:
                call = {}
                call['endpoint'] = endpoint
                call['caller_name'] = fs_call['b_cid_name'] or fs_call['cid_name']
                call['caller_num'] = fs_call['b_cid_num'] or fs_call['cid_num']
                call['callee_num'] = fs_call['dest']
                call['created'] = fs_call['created']
                call_list.append(call)
    return call_list
