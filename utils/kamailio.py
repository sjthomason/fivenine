import xmlrpclib

from datetime import datetime
from django.core.cache import cache
from django.utils.timezone import utc

from portal.models import Endpoint
from utils.cache import key_from_args
from utils.serxmlrpc import ServerProxy


def exp_to_dt(seconds):
    """
    Returns an absolute DateTime object for a registration expiration
    given in seconds
    """
    from datetime import datetime, timedelta
    now = datetime.utcnow().replace(tzinfo=utc)
    delta = timedelta(seconds=seconds)
    return now + delta

def parse_ul_contact(data):
    """
    Parses the output of ul_show_contact()
    Returns a dictionary of the contact uri and parameters
    """

    import re

    _syntax = re.compile('^Contact:: (.*)$')
    m = _syntax.match(data)

    if not m: raise ValueError, 'Invalid Contact(' + data + ')'
    data = m.group(1)
    data = data.replace('<', '').replace('>', '').split(';')
    uri = data.pop(0)

    # Parse parameters
    splits = map(lambda n: n.partition('='), data) if data else []
    contact = dict(map(lambda k: (k[0], k[2] if k[2] else None), splits)) if splits else {}

    expires = contact.get('expires')
    if expires:
        try:
            expires = int(expires)
            contact['expires'] = expires
            contact['expires_dt'] = exp_to_dt(expires)
        except ValueError:
            pass

    contact['uri'] = uri
    return contact


class KamailioServer(object):

    def __init__(self, host, port=5060, ssl=None):
        uri = 'http://%s:%s/RPC' % (host, port)
        self.con = ServerProxy(uri, ssl)

    def address_reload(self):
        self.con.mi('address_reload')

    def core_ps(self):
        ps = self.con.core.ps()
        l = []
        for i in range(0, len(ps), 2):
            l.append((ps[i], ps[i+1]))
        return l

    def core_version(self):
        ver = self.con.core.version()
        return ver

    def core_uptime(self):
        uptime = self.con.core.uptime()
        for k in uptime.keys():
            uptime[k] = str(uptime[k]).strip()
        uptime['uptime'] = float(uptime['uptime'])
        return uptime

    def core_kill(self, sig=15):
        self.con.core.kill(sig)

    def core_shmmem(self):
        return self.con.core.shmmem()

    def core_tcp_info(self):
        return self.con.core.tcp_info()

    def dr_reload(self):
        self.con.mi('dr_reload')

    def sl_stats(self):
        return self.con.sl.stats()

    def tm_stats(self):
        return self.con.tm.stats()

    def ul_dump(self):
        return self.con.ul.dump()

    def ul_show_contact(self, aor, table='registrations', domain=None):
        if domain:
            aor = '%s@%s' % (aor, domain)

        cache_key = key_from_args(__file__, 'ul_show_contact', table, aor)
        parsed_contact_list = cache.get(cache_key)
        if parsed_contact_list is not None:
            return parsed_contact_list

        parsed_contact_list = []
        try:
            raw_contact_list = self.con.mi('ul_show_contact', table, aor)
            for i in raw_contact_list:
                parsed_contact_list.append(parse_ul_contact(i))
        except xmlrpclib.Fault:
            pass

        cache.set(cache_key, parsed_contact_list, 180)
        return parsed_contact_list

    def tls_list(self):
        return self.con.tls.list()

    def system_listmethods(self):
        return self.con.system.listMethods()
