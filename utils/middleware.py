from django.utils import timezone
from django.core.cache import cache
from utils.cache import key_from_args

class TimezoneMiddleware(object):

    def process_request(self, request):
        key = key_from_args('timezone', request.session.session_key)
        tz = cache.get(key)
        if tz is not None:
            timezone.activate(tz)
        else:
            tz = request.session.get('timezone')
            if tz:
                cache.set(key, tz, 1800)
                timezone.activate(tz)
