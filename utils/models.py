from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.template.defaultfilters import slugify
from uuid import uuid4 as uuid

from utils.forms import MultipleEmailField as MultipleEmailFormField
from utils import validators

def unique_slug(model_instance, slug_field, slug_value):
    orig_slug = slug = slugify(slug_value)
    index = 0

    queryset = model_instance.__class__._default_manager.filter(**{slug_field: slug})
    if model_instance.pk:
        queryset = queryset.exclude(pk=model_instance.pk)

    while queryset.filter(**{slug_field: slug}).count():
        index += 1
        slug = orig_slug + '-' + str(index)
    return slug


class UUIDField(models.CharField):

    __metaclass__ = models.SubfieldBase

    description = _("UUID")

    empty_strings_allowed = False
    default_error_messages = {
        'invalid': _(u"'%s' value must be a string."),
    }

    def __init__(self, *args, **kwargs):
        assert kwargs.get('primary_key', False) is True, \
               "%ss must have primary_key=True." % self.__class__.__name__
        kwargs['blank'] = True
        kwargs['editable'] = False
        kwargs['max_length'] = 32
        models.CharField.__init__(self, *args, **kwargs)

    def get_internal_type(self):
        return "CharField"

    def to_python(self, value):
        if value is None:
            return value
        try:
            return str(value)
        except (TypeError, ValueError):
            msg = self.error_messages['invalid'] % str(value)
            raise exceptions.ValidationError(msg)

    def pre_save(self, model_instance, add):
        if add:
            value = str(getattr(model_instance, self.attname))
            if not value:
                value = str(uuid().hex)
                setattr(model_instance, self.attname, value)
            return value
        else:
            return super(UUIDField, self).pre_save(model_instance, add)

    def validate(self, value, model_instance):
            pass

    def get_prep_value(self, value):
        if value is None:
            return None
        return str(value)

    def formfield(self, **kwargs):
        return None


class AutoSlugField(models.SlugField):

    description = _("Auto generated slug field")

    def __init__(self, populate_from=None, **kwargs):
        self.populate_from = populate_from
        if populate_from:
            kwargs['editable'] = False
            kwargs['blank'] = True

        super(AutoSlugField, self).__init__(**kwargs)

    def pre_save(self, model_instance, add):
        if self.populate_from:
            if self.unique:
                return unique_slug(model_instance, self.name, getattr(model_instance, self.populate_from))
            else:
                return slugify(getattr(model_instance, self.populate_from))
        else:
            return super(AutoSlugField, self).pre_save(model_instance, add)


class CountryField(models.CharField):

    description = _("Country code (two uppercase letters)")

    def __init__(self, *args, **kwargs):
        from utils.countries import COUNTRIES
        kwargs['choices'] = COUNTRIES
        kwargs['max_length'] = 2
        super(CountryField, self).__init__(*args, **kwargs)


class MultipleEmailField(models.CharField):
    default_validators = [validators.validate_multiple_email]
    description = _("E-mail address or addresses separated by commas")

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = kwargs.get('max_length', 255)
        models.CharField.__init__(self, *args, **kwargs)

    def formfield(self, **kwargs):
        # As with CharField, this will cause email validation to be performed
        # twice.
        defaults = {
            'form_class': MultipleEmailFormField,
        }
        defaults.update(kwargs)
        return super(MultipleEmailField, self).formfield(**defaults)


class UUIDModel(models.Model):
    """
    A model with a UUID as pk
    """

    id = UUIDField(verbose_name='ID', primary_key=True, auto_created=True)

    class Meta:
        abstract = True

