import pytz
from datetime import datetime


def get_offset(tz):
    now = datetime.now()
    timezone = pytz.timezone(tz)
    ofs = timezone.utcoffset(now) - timezone.dst(now)
    s = int(ofs.total_seconds())
    h = s / 3600
    s -= 3600*h
    m = s / 60
    if h < 0:
        return '%03d%02d' % (h, m)
    return '+%02d%02d' % (h, m)


ALL_TIMEZONE_CHOICES = tuple(zip(pytz.all_timezones,
                                 pytz.all_timezones))
COMMON_TIMEZONE_CHOICES = tuple(zip(pytz.common_timezones,
                                    pytz.common_timezones))
PRETTY_TIMEZONE_CHOICES = []

for time_zone in pytz.common_timezones:
    try:
        offset = get_offset(time_zone)
        PRETTY_TIMEZONE_CHOICES.append((time_zone, "(GMT%s:%s) %s" %
                                        (offset[0:3], offset[3:], time_zone)))
    except (pytz.exceptions.AmbiguousTimeError,
            pytz.exceptions.NonExistentTimeError):
        pass

PRETTY_TIMEZONE_CHOICES = tuple(sorted(PRETTY_TIMEZONE_CHOICES,
                                       key=lambda tz: int(tz[1][4:7]),
                                       reverse=True))

def get_country_tz_choices(country=None):
    if country is not None and country.upper() in pytz.country_names:
        COUNTRY_TIMEZONE_CHOICES = []

        for tz in pytz.country_timezones[country.upper()]:
            if tz in pytz.common_timezones:
                ofs = get_offset(tz)
                COUNTRY_TIMEZONE_CHOICES.append((tz, "(GMT%s:%s) %s" %
                                                (ofs[0:3], ofs[3:], tz)))

        COUNTRY_TIMEZONE_CHOICES = tuple(sorted(COUNTRY_TIMEZONE_CHOICES,
                                                key=lambda tz: int(tz[1][4:7]),
                                                reverse=True))
        return COUNTRY_TIMEZONE_CHOICES
    else:
        return PRETTY_TIMEZONE_CHOICES
