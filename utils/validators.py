from django.core.validators import EmailValidator, email_re


class MultipleEmailValidator(EmailValidator):

    def __call__(self, value):
        #value = value.replace(';', ',')
        email_list = value.split(',')
        for email in email_list:
            super(MultipleEmailValidator, self).__call__(email)


validate_multiple_email = MultipleEmailValidator(email_re,
    "Enter a valid e-mail address or addresses separated by commas.", 'invalid')
