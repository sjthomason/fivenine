from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from portal.models import UserProfile
from utils import pywhois


class ContactForm(forms.Form):
    PH = {'name': "Enter your name",
          'username': "Enter your username",
          'email': "Enter your email address",
          'subject': "Subject",
          'message': "Comments" }

    name = forms.CharField(max_length=100,
                widget=forms.TextInput(attrs={'class':'add-ph',
                                              'placeholder':PH['name']}))
    username = forms.CharField(max_length=64, required=False,
                widget=forms.TextInput(attrs={'class':'add-ph',
                                              'placeholder':PH['username']}))
    email = forms.EmailField(label="E-mail", max_length=75,
                widget=forms.TextInput(attrs={'class':'add-ph',
                                              'placeholder':PH['email']}))
    subject = forms.CharField(max_length=100,
                widget=forms.TextInput(attrs={'class':'add-ph',
                                              'placeholder':PH['subject']}))
    message = forms.CharField(
                widget=forms.Textarea(attrs={'class':'add-ph',
                                             'placeholder':PH['message']}))


class SupportForm(ContactForm):
    PH = {'subject': "Brief description of your support request",
          'message': "Include as much detail as possible" }

    subject = forms.CharField(max_length=100,
                widget=forms.TextInput(attrs={'class':'add-ph',
                                              'placeholder':PH['subject']}))

    message = forms.CharField(
                widget=forms.Textarea(attrs={'class':'add-ph',
                                             'placeholder':PH['message']}))

    def clean_username(self):
        """
        Validates that a user exists with the given username.
        """
        username = self.cleaned_data["username"]
        self.users_cache = User.objects.filter(username__iexact=username)
        if not len(self.users_cache) > 0:
            raise forms.ValidationError("That username is not on file.")
        return username


class HostingForm(forms.Form):
    PH = {'first_name': "First Name",
          'last_name': "Last Name",
          'email': "Email Address",
          'phone': "Phone Number",
          'comments': "Comments"}

    first_name = forms.CharField(max_length=30,
                 widget=forms.TextInput(attrs={'class':'add-ph',
                                               'placeholder':PH['first_name']}))
    last_name = forms.CharField(max_length=30,
                widget=forms.TextInput(attrs={'class':'add-ph',
                                              'placeholder':PH['last_name']}))
    email = forms.EmailField(label="E-mail", max_length=75,
                widget=forms.TextInput(attrs={'class':'add-ph',
                                              'placeholder':PH['email']}))
    phone = forms.CharField(max_length=16,
                widget=forms.TextInput(attrs={'class':'add-ph',
                                              'placeholder':PH['phone']}))
    comments = forms.CharField(
                widget=forms.Textarea(attrs={'class':'add-ph',
                                             'placeholder':PH['comments']}))


class QuoteForm(forms.Form):

    SIZE_CHOICES = (
        ('', 'Company Size'),
        ('1-4', 'Less than 5'),
        ('5-10', '5-10'),
        ('11-20', '11-20'),
        ('21-50', '21-50'),
        ('51-250', '51-250'),
        ('250+', '250+'),
        ('Unknown', 'Unknown'),
    )

    PH = {'first_name': "First Name",
          'last_name': "Last Name",
          'email': "Email Address",
          'phone': "Phone Number"}

    first_name = forms.CharField(max_length=30,
                 widget=forms.TextInput(attrs={'class':'add-ph',
                                               'placeholder':PH['first_name']}))
    last_name = forms.CharField(max_length=30,
                widget=forms.TextInput(attrs={'class':'add-ph',
                                              'placeholder':PH['last_name']}))
    email = forms.EmailField(label="E-mail", max_length=75,
                widget=forms.TextInput(attrs={'class':'add-ph',
                                              'placeholder':PH['email']}))
    phone = forms.CharField(max_length=16,
                widget=forms.TextInput(attrs={'class':'add-ph',
                                              'placeholder':PH['phone']}))
    company_size = forms.ChoiceField(choices=SIZE_CHOICES)


class DomainForm(forms.Form):

    domain = forms.CharField()

    def clean_domain(self):
        """
        Validates that a registered domain does not exist for the give domain
        """
        domain = self.cleaned_data["domain"]
        try:
            whois = pywhois.whois(domain)
        except pywhois.parser.PywhoisError:
            return domain
        raise forms.ValidationError("The domain %s is already registered." % domain)


class TermsOfServiceForm(forms.Form):

    tos = forms.BooleanField(widget=forms.CheckboxInput(),
                             label=u'I have read and agree to the Terms of Service',
                             error_messages={'required': "You must agree to the terms to register"})



class UserRegistrationForm(forms.Form):
    """
    A form that creates a user, with no privileges, from the given username and password.
    """
    error_messages = {
        'duplicate_username': _("A user with that username already exists."),
        'password_mismatch': _("The two password fields didn't match."),
    }

    first_name = forms.RegexField(label="First Name", max_length=30, regex=r'^[a-zA-Z]+$')
    last_name = forms.RegexField(label="Last Name", max_length=30, regex=r'^[a-zA-Z]+$')

    username = forms.RegexField(label=_("Username"), max_length=30,
        regex=r'^[\w.@+-]+$',
        help_text = _("Required. 30 characters or fewer. Letters, digits and "
                      "@/./+/-/_ only."),
        error_messages = {
            'invalid': _("This value may contain only letters, numbers and "
                         "@/./+/-/_ characters.")})
    password1 = forms.CharField(label=_("Password"),
        widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password confirmation"),
        widget=forms.PasswordInput,
        help_text = _("Enter the same password as above, for verification."))

    email = forms.EmailField(label="E-mail", max_length=75)

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1", "")
        password2 = self.cleaned_data["password2"]
        if password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'])
        return password2

    def clean_email(self):
        """
        Validates that a user does not exist with the given e-mail address.
        """
        email = self.cleaned_data["email"]
        self.users_cache = User.objects.filter(
                                email__iexact=email
                            )
        if len(self.users_cache) > 0:
            raise forms.ValidationError("That e-mail address is already on file.")
        return email


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        exclude = ["acct_type"]

