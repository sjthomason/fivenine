import datetime
import hashlib
import random
import re

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db import transaction
from django.template.loader import render_to_string
from django.template.defaultfilters import truncatewords
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now as datetime_now


SHA1_RE = re.compile('^[a-f0-9]{40}$')


class RegistrationManager(models.Manager):
    """
    Provides shortcuts for account creation
    and activation (including generation and emailing of activation
    keys), and for cleaning out expired inactive accounts.
    """

    def activate_user(self, activation_key):
        """
        Validate an activation key and activate the corresponding
        ``User`` if valid.

        If the key is valid and has not expired, return the ``User``
        after activating.

        If the key is not valid or has expired, return ``None``.

        If the key is valid but the ``User`` is already active,
        return ``None``.

        To prevent reactivation of an account which has been
        deactivated by site administrators, the activation key is
        reset to the string constant ``RegistrationProfile.ACTIVATED``
        after successful activation.

        """
        # Make sure the key we're trying conforms to the pattern of a
        # SHA1 hash; if it doesn't, no point trying to look it up in
        # the database.
        if SHA1_RE.search(activation_key):
            try:
                registration = self.get(activation_key=activation_key)
            except self.model.DoesNotExist:
                return
            if not registration.activation_key_expired():
                user = registration.user
                user.is_active = True
                user.save()
                registration.activation_key = self.model.ACTIVATED
                registration.save()
                return user
        return

    def create_inactive_user(self, username, email, password,
                             first_name, last_name, site=None,
                             send_email=True):
        """
        Create a new, inactive ``User``, generate a
        ``Registration`` and email its activation key to the
        ``User``, returning the new ``User``.

        By default, an activation email will be sent to the new
        user. To disable this, pass ``send_email=False``.

        """
        new_user = User.objects.create_user(username, email, password)
        new_user.is_active = False
        new_user.first_name = first_name
        new_user.last_name = last_name
        new_user.save()

        registration = self.create_registration(new_user)

        if send_email:
            registration.send_activation_email(site)

        return new_user
    create_inactive_user = transaction.commit_on_success(create_inactive_user)

    def create_registration(self, user):
        """
        Create a ``Registration`` for a given
        ``User``, and return the ``Registration``.

        The activation key for the ``Registration`` will be a
        SHA1 hash, generated from a combination of the ``User``'s
        username and a random salt.

        """
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        username = user.username
        if isinstance(username, unicode):
            username = username.encode('utf-8')
        activation_key = hashlib.sha1(salt+username).hexdigest()
        return self.create(user=user,
                           activation_key=activation_key)

    def delete_expired_users(self):
        """
        Remove expired instances of ``Registration`` and their
        associated ``User``s.
        """

        for registration in self.all():
            try:
                if registration.activation_key_expired():
                    user = registration.user
                    if not user.is_active:
                        user.delete()
                        registration.delete()
            except User.DoesNotExist:
                registration.delete()


class Registration(models.Model):
    """
    Stores an activation key for use during
    user account registration.
    """

    ACTIVATED = u"ALREADY_ACTIVATED"

    user = models.ForeignKey(User, unique=True, verbose_name=_('user'))
    activation_key = models.CharField(_('activation key'), max_length=40)

    objects = RegistrationManager()

    class Meta:
        verbose_name = "registration"
        verbose_name_plural = "registrations"

    def __unicode__(self):
        return u"Registration information for %s" % self.user

    def activation_key_expired(self):
        """
        Determine whether this ``RegistrationProfile``'s activation
        key has expired, returning a boolean -- ``True`` if the key
        has expired.

        Key expiration is determined by a two-step process:

        1. If the user has already activated, the key will have been
           reset to the string constant ``ACTIVATED``. Re-activating
           is not permitted, and so this method returns ``True`` in
           this case.

        2. Otherwise, the date the user signed up is incremented by
           the number of days specified in the setting
           ``ACCOUNT_ACTIVATION_DAYS`` (which should be the number of
           days after signup during which a user is allowed to
           activate their account); if the result is less than or
           equal to the current date, the key has expired and this
           method returns ``True``.

        """
        expiration_date = datetime.timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS)
        return self.activation_key == self.ACTIVATED or \
               (self.user.date_joined + expiration_date <= datetime_now())
    activation_key_expired.boolean = True

    def send_activation_email(self, site=None):
        """
        Send an activation email to the user associated with this
        ``Registration``.
        """

        FROM_ADDR = 'no-reply@5ninesolutions.com'
        FROM_NAME = '5Nine Solutions'
        FROM_EMAIL = '%s <%s>' % (FROM_NAME, FROM_ADDR)

        subject_template_name='registration/activation_email_subject.txt'
        email_template_name='registration/activation_email.txt'
        from_email=FROM_EMAIL
        use_https = False

        if site is not None:
            site_name = site.name
            domain = site.domain

        user = self.user

        c = {
            'email': user.email,
            'domain': domain,
            'site_name': site_name,
            'user': user,
            'protocol': use_https and 'https' or 'http',
            'activation_key': self.activation_key,
            'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS
        }
        subject = render_to_string(subject_template_name, c)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        email = render_to_string(email_template_name, c)

        user.email_user(subject, email, FROM_EMAIL)


class Testimonial(models.Model):
    added = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    body = models.TextField()
    customer_name = models.CharField(max_length=128)
    customer_title = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return '%s - %s' % (truncatewords(self.body, 6), self.customer_name)
