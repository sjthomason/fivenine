from django.conf.urls.defaults import patterns, url
from django.views.generic import TemplateView

from forms import TermsOfServiceForm, UserProfileForm, UserRegistrationForm
from views import RegistrationActivateView, SignupWizardView

form_list = [TermsOfServiceForm, UserRegistrationForm, UserProfileForm]

urlpatterns = patterns('',
    url(r'^$', SignupWizardView.as_view(form_list=form_list)),
    url(r'^activate/complete/$',
        TemplateView.as_view(template_name='registration/registration_activation_complete.html')),
    url(r'^activate-key/(?P<activation_key>\w+)/$',
        RegistrationActivateView.as_view()),
    url(r'^complete/$',
        TemplateView.as_view(template_name='registration/registration_complete.html')),
    url(r'^not-allowed/$', TemplateView.as_view(template_name='about.html'))

)
