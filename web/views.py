from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.formtools.wizard.views import SessionWizardView
from django.contrib.sites.models import get_current_site
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic import DetailView, ListView, FormView, TemplateView


from forms import (ContactForm, DomainForm, HostingForm, QuoteForm,
                   SupportForm, TermsOfServiceForm,
                   UserRegistrationForm, UserProfileForm)
from billing.models import CpanelPlan, PbxPlan
from kb.models import KbArticle
from models import Registration, Testimonial

class ContactFormView(FormView):

    form_class = ContactForm
    success_url = "/"
    recipients = ['info@5ninesolutions.com']

    def get_initial(self, *args, **kwargs):
        """
        Returns the initial data to use for forms on this view.
        """
        initial = super(ContactFormView, self).get_initial(*args, **kwargs)

        if self.request.user.is_authenticated():
            initial['name'] = self.request.user.get_full_name()
            initial['username'] = self.request.user.username
            initial['email'] = self.request.user.email

        return initial

    def get_recipients(self):
        """
        Returns the recipients of the email for this form
        """
        return self.recipients

    def form_valid(self, form):
        name = form.cleaned_data['name']
        username = form.cleaned_data['username']
        email = form.cleaned_data['email']
        subject = form.cleaned_data['subject']
        message = form.cleaned_data['message']

        recipients = self.get_recipients()

        if name:
            email = '%s <%s>' % (name, email)

        from django.core.mail import send_mail
        send_mail(subject, message, email, recipients)
        return HttpResponseRedirect(self.get_success_url())


class DomainFormView(FormView):

    template_name = "web/domain_form.html"
    form_class = DomainForm
    success_url = "domains/register/"


class RegistrationActivateView(DetailView):

    template_name = "registration/registration_activation_complete.html"
    context_object_name = 'new_user'

    def get_object(self):
        activation_key = self.kwargs.get('activation_key')

        if activation_key is not None:
            obj = Registration.objects.activate_user(activation_key)
            return obj


class ServiceView(FormView):

    recipients = []

    def get_recipients(self):
        """
        Returns the recipients of the email for this form
        """
        return self.recipients

    def get_context_data(self, *args, **kwargs):
        context = super(ServiceView, self).get_context_data(*args, **kwargs)
        context['plan_list'] = list(CpanelPlan.objects.all())
        return context

    def form_valid(self, form):
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        email = form.cleaned_data['email']
        phone = form.cleaned_data['phone']
        comments = form.cleaned_data['comments']

        name = first_name + ' ' + last_name

        subject = 'Service Contact Request'
        message = []
        message.append('Service Contact Request')
        message.append('\n')
        message.append('Name: %s' % name)
        message.append('Email: %s' % email)
        message.append('Phone: %s' % phone)
        message.append('------------------------')
        message.append(comments)

        message = '\n'.join(message)
        recipients = self.get_recipients()

        if name:
            email = '%s <%s>' % (name, email)

        from django.core.mail import send_mail
        send_mail(subject, message, email, recipients)
        msg = "Thank you for contacting us. "\
              "One of our sales associates will be in touch shortly."
        messages.success(self.request, msg, fail_silently=True)
        return super(ServiceView, self).form_valid(form)


class PbxServiceView(FormView):

    recipients = []

    def get_recipients(self):
        """
        Returns the recipients of the email for this form
        """
        return self.recipients

    def get_context_data(self, *args, **kwargs):
        context = super(PbxServiceView, self).get_context_data(*args, **kwargs)
        context['plan_list'] = list(PbxPlan.objects.all())
        return context

    def form_valid(self, form):
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        email = form.cleaned_data['email']
        phone = form.cleaned_data['phone']
        company_size = form.cleaned_data['company_size']

        name = first_name + ' ' + last_name

        subject = 'Pbx Quote Request'
        message = []
        message.append('Pbx Quote Request')
        message.append('\n')
        message.append('Name: %s' % name)
        message.append('Email: %s' % email)
        message.append('Phone: %s' % phone)
        message.append('Company Size: %s' % company_size)

        message = '\n'.join(message)
        recipients = self.get_recipients()

        if name:
            email = '%s <%s>' % (name, email)

        from django.core.mail import send_mail
        send_mail(subject, message, email, recipients)
        msg = "Thank you for submitting a quote request. "\
              "One of our sales associates will be in touch shortly."
        messages.success(self.request, msg, fail_silently=True)
        return super(PbxServiceView, self).form_valid(form)


class SupportFormView(ContactFormView):

    template_name = "web/support_form.html"
    form_class = SupportForm
    success_url = "/"
    recipients = ['support@5ninesolutions.com']


class SignupWizardView(SessionWizardView):

    success_url = "/signup/complete/"
    template_name = "web/signup_wizard_form.html"

    def get_success_url(self):
        return self.success_url

    def done(self, form_list, **kwargs):
        user_form = form_list[1]
        profile_form = form_list[2]

        username = user_form.cleaned_data['username']
        first_name = user_form.cleaned_data['first_name']
        last_name = user_form.cleaned_data['last_name']
        email = user_form.cleaned_data['email']
        password = user_form.cleaned_data['password1']

        site = get_current_site(self.request)

        new_user = Registration.objects.create_inactive_user(
            username, email, password, first_name, last_name, site)

        profile = profile_form.save(commit=False)
        profile.user = new_user
        profile.save()

        return HttpResponseRedirect(self.get_success_url())


class SitemapView(ListView):

    def render_to_response(self, context, **response_kwargs):
        response_kwargs.update({'content_type': 'application/xml'})
        return super(SitemapView, self).render_to_response(context, **response_kwargs)

class TestimonialView(DetailView):

    model = Testimonial
    context_object_name = 'testimonial'

    def get_object(self):
        queryset = self.get_queryset()
        queryset = queryset.order_by('?')

        try:
            obj = queryset[0]
        except IndexError:
            obj = None
        return obj

##### view functions #####
about_view = TemplateView.as_view(template_name='web/about.html')
contact_view = ContactFormView.as_view(template_name='web/contact_form.html')
contact_done_view = TemplateView.as_view(template_name='web/contact_done.html')
index_view = TestimonialView.as_view(template_name='web/index.html')
network_view = TemplateView.as_view(template_name='web/network.html')

hosting_service_view = ServiceView.as_view(
                        template_name='web/services_web.html',
                        form_class=HostingForm,
                        success_url=reverse_lazy('web.views.hosting_service_view'),
                        recipients = ['info@5ninesolutions.com'])
pbx_service_view = PbxServiceView.as_view(
                        template_name='web/services_pbx.html',
                        form_class=QuoteForm,
                        success_url=reverse_lazy('web.views.pbx_service_view'),
                        recipients=['info@5ninesolutions.com'])
services_view = ServiceView.as_view(
                        template_name='web/services.html',
                        form_class=HostingForm,
                        success_url=reverse_lazy('web.views.services_view'),
                        recipients = ['info@5ninesolutions.com'])

services_trunking_view = TemplateView.as_view(template_name='web/services_trunking.html')

sitemap_view = SitemapView.as_view(template_name='web/sitemap.xml',
                                   model=KbArticle)
