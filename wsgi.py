import os
import sys

install_root = os.path.abspath(os.path.dirname(__file__))
if install_root not in sys.path:
    sys.path.append(install_root)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
